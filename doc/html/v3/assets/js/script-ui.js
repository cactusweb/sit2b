$(document).ready(function(){

    //NEW TASK
    //temp: open popup
    $("button.create_task").click(function(){
        $("div.overlay").fadeIn();
        $("div.popup.new_task").fadeIn();
    });
    //

    //toggle: Описание | Список
    $("div.new_task a.toggle").click(function(){
        $("div.new_task a.toggle").removeClass("active");
        $(this).addClass("active");
        var dataClass = $(this).attr("data-class");
        $(this).parent().parent().find(".toggle_data").removeClass("active");
        $(this).parent().parent().find(".toggle_data."+dataClass).addClass("active");
    });

    //accordion
    $("div.new_task div.part div.part_title").click(function(){
        $(this).toggleClass("active");
        var options = {
        };
        $(this).parent().find("div.part_content").toggle( "blind", options, 500 );
    });

});