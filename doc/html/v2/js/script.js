$(document).ready(function(){
    
    ColumnResize();
    
    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
        _renderItem: function( ul, item ) {
            ul.addClass("list_sort");
            var li = $( "<li>", { text: item.label } );

            if ( item.disabled ) {
                li.addClass( "ui-state-disabled" );
            }

            $( "<span>", {
                style: item.element.attr( "data-style" ),
                "class": "ui-icon " + item.element.attr( "data-class" )
            })
                .appendTo( li );

            return li.appendTo( ul );
        }
    });

    //LIST SORT
    $("#list_sort")
        .iconselectmenu({
            select: function( event, ui ) {
                $(".list_sort_wrapper .ui-selectmenu-text").find("span.dir").remove();
                var dir = document.createElement('span');
                dir.className = "dir ";
                dir.className += ui.item.element.attr( "data-class" );
                $(".list_sort_wrapper .ui-selectmenu-text").prepend(dir);
            },
            create: function( event, ui ) {
                var dir = document.createElement('span');
                dir.className = "dir ";
                dir.className += $("#list_sort option").first().attr( "data-class" );
                $(".list_sort_wrapper .ui-selectmenu-text").prepend(dir);
                var buttonDel = document.createElement('a');
                buttonDel.className = "sort_delete";
                $(".list_sort_wrapper .ui-selectmenu-text").after(buttonDel);
            }
        })
        .iconselectmenu( "menuWidget" )
        .addClass( "ui-menu-icons customicons" );

    //temp
    $(".list_sort_wrapper a.sort_delete").click(function(){
        $(".list_sort_wrapper").remove();
    });
    $(".filter_by_user a.sort_delete").click(function(){
        $(".filter_by_user").remove();
    });
    //LIST SORT END

    $("form select:not(.sorted)").selectmenu();

    $("select.sorted")
        .iconselectmenu({
            select: function( event, ui ) {
                var id = this.id;
                $("#"+id+"-button .ui-selectmenu-text").find("span.dir").remove();
                var dir = document.createElement('span');
                dir.className = "dir ";
                dir.className += ui.item.element.attr( "data-class" );
                $("#"+id+"-button .ui-selectmenu-text").prepend(dir);
            },
            create: function( event, ui ) {
                var id = this.id;
                var dir = document.createElement('span');
                dir.className = "dir ";
                dir.className += $("#"+id+" option").first().attr( "data-class" );
                $("#"+id+"-button .ui-selectmenu-text").prepend(dir);
                $("#"+id+"-button").addClass("sorted");
            }
        })
        .iconselectmenu( "menuWidget" )
        .addClass( "ui-menu-icons customicons" );

    //COLORS
    $("ul.colors li").click(function(){
        $(this).parent().find("li").removeClass("selected");
        var color = $(this).attr("data-color");
        $(this).parent().find("input.color").val(color);
        $(this).addClass("selected");
    });

    //CHECKBOX
    $.widget( "app.checkbox", {
        _create: function() {
            // Call the default widget constructor first.            
            this._super();
            // Hide the HTML checkbox, then insert our button.
            this.element.addClass( "ui-helper-hidden-accessible" );
            this.button = $( "<button/>" ).insertAfter( this.element );
            // Configure the button by adding our widget class,
            // setting some default text, default icons, and such.
            // The create event handler removes the title attribute,
            // because we don't need it.
            this.button.addClass( "ui-checkbox" )
                       .text( "checkbox" )
                       .button({
                           text: false,
                           icons: { 
                               primary: "ui-icon-blank"
                           },
                           create: function( e, ui ) {
                               $( this ).removeAttr( "title" ); 
                           }
                       });
            // Listen for click events on the button we just inserted and
            // toggle the checked state of our hidden checkbox.
            this._on( this.button, {
                click: function( e ) {
                    this.element.prop( "checked", !this.element.is( ":checked" ) );
                    this.refresh();
                }
            });
            // Update the checked state of the button, depending on the
            // initial checked state of the checkbox.
            this.refresh();
        },
        _destroy: function() {
            // Standard widget cleanup.
            this._super();
            // Display the HTML checkbox and remove the button.
            this.element.removeClass( "ui-helper-hidden-accessible" );
            this.button.button( "destroy" ).remove();

        },
        refresh: function() {
            // Set the button icon based on the state of the checkbox.
            this.button.button( "option", "icons", {
                primary: this.element.is( ":checked" ) ?
                "ui-icon-check" : "ui-icon-blank"
            });
        }
    });
    // Create three checkbox instances.
    $( "input[type='checkbox']" ).checkbox();
    //CHECKBOX END

    $("a.addlist").click(function(){
        $("div.popup.addlist").fadeIn();
    });
    $("a.lists_settings").click(function(){
        $("div.popup.lists_settings").fadeIn();
    });
    $("a.list_menu").click(function(){
        $(this).parent().find("div.popup.list_settings").fadeIn();
    });
    $("div.popup div.popup_header .close").click(function(){
        $(this).parent().parent().fadeOut();
        $("div.overlay").fadeOut();
    });
    $("div.overlay").click(function(){
        $("div.popup").fadeOut();
        $("div.overlay").fadeOut();
    });

    //NEW TASK
    $("a.create_task").click(function(){
        $("div.overlay").fadeIn();
        $("div.popup.new_task").fadeIn();
    });
    $("div.new_task a.toggle").click(function(){
        $("div.new_task a.toggle").removeClass("active");
        $(this).addClass("active");
        var dataClass = $(this).attr("data-class");
        $(this).parent().parent().find(".toggle_data").removeClass("active");
        $(this).parent().parent().find(".toggle_data."+dataClass).addClass("active");
    });
    $("div.new_task .list_priority").buttonset();
    $("div.new_task div.part div.part_title").click(function(){
        $(this).toggleClass("active");
        var options = {
        };
        $(this).parent().find("div.part_content").toggle( "blind", options, 500 );
    });
    
    //NOTIFICATIONS
    $("header a.notification").click(function(){
        $("div.overlay").fadeIn();
        $("div.popup.notifications").fadeIn();
        $(this).addClass("active"); 
        return false;
    });
    
    //CALENDAR
    $(".calendar_top ul.colors li").click(function(){
        $(this).toggleClass("active");
    });
    $("div#calendar_show_by").buttonset();
    //
    
//    $(".lists").niceScroll({cursorcolor:"#b3b3b3"});
    
//    $(".resizable1").resizable(
//    {
//        autoHide: true,
//        handles: 'e',
//        resize: function(e, ui) 
//        {
//            var parent = ui.element.parent();
//            var remainingSpace = parent.width() - ui.element.outerWidth(),
//                divTwo = ui.element.next(),
//                divTwoWidth = (remainingSpace - (divTwo.outerWidth() - divTwo.width()))/parent.width()*100+"%";
//                divTwo.width(divTwoWidth);
//        },
//        stop: function(e, ui) 
//        {
//            var parent = ui.element.parent();
//            ui.element.css(
//            {
//                width: ui.element.width()/parent.width()*100+"%",
//            });
//        }
//    });
 
});

function ColumnResize(){
    var winH = $(document).height();
    var columhH = winH - 100;
    $(".column div.lists").height(columhH);
}