<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>sit2B</title>
        <script type="text/javascript" src="./js/jquery-1.11.0.js"></script>
<!--        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>-->
        <link rel="stylesheet" href="./css/jquery-ui.css" />
        <script src="./js/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="./css/reset.css" />
        <link rel="stylesheet" href="./css/style.css?v=1" />
        <script type="text/javascript" src="./js/script.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="overlay"></div>
        <div class="popup dialog trashbox" style="top:10px; left: 10px;">
            <div class="popup_header">
                <div class="title">Корзина</div>
                <a href="#" class="close"></a>
            </div>
            <form action="#" >
                <div class="popup_content">
                    <div class="row">
                        <input type="text" value="" placeholder="Поиск..." />
                    </div>
                    <div class="tasks color_bce5fa">
                        <div class="task" >
                            <a href="#" class="button cancel">Восстановить</a>
                            <a href="#" class="mode_toggle"></a>
                            <div class="task_title">Продумать варианты скрытия панели Sit2b.</div>
                            <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                            <ul class="task_notifications">
                                <li>
                                    <span class="ico priority_a"></span>
                                </li>
                                <li>
                                    <span class="ico repeat"></span>
                                </li>
                                <li>
                                    <span class="ico reminder"></span>
                                </li>
                                <li>
                                    <span class="ico place"></span>
                                </li>
                                <li>
                                    <span class="ico list"></span>
                                </li>
                                <li>
                                    <span class="ico executor"></span>
                                </li>
                                <li>
                                    <span class="ico files active"></span>
                                </li>
                                <li>
                                    <span class="ico comments active"></span>
                                </li>
                            </ul>
                        </div>
                        <div class="task" >
                            <a href="#" class="button cancel">Восстановить</a>
                            <a href="#" class="mode_toggle"></a>
                            <div class="task_title">Заехать на СТО</div>
                            <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                            <ul class="task_notifications">
                                <li>
                                    <span class="ico priority_b"></span>
                                </li>
                                <li>
                                    <span class="ico repeat"></span>
                                </li>
                                <li>
                                    <span class="ico reminder"></span>
                                </li>
                                <li>
                                    <span class="ico place"></span>
                                </li>
                                <li>
                                    <span class="ico list"></span>
                                </li>
                                <li>
                                    <span class="ico executor"></span>
                                </li>
                                <li>
                                    <span class="ico files"></span>
                                </li>
                                <li>
                                    <span class="ico comments"></span>
                                </li>
                            </ul>
                        </div>
                        <div class="task nobg" >
                            <a href="#" class="button cancel">Восстановить</a>
                            <a href="#" class="mode_toggle"></a>
                            <div class="task_title">Тема задачи, которая очень длинная и переносится на вторую, третью и, возможно даже четвертую строку и следующую строку</div>
                            <div class="task_date time"><span>1 час</span></div>
                            <ul class="task_notifications">
                                <li>
                                    <span class="ico priority_c"></span>
                                </li>
                            </ul>
                        </div>
                        <div class="task done" >
                            <a href="#" class="button cancel">Восстановить</a>
                            <a href="#" class="mode_toggle"></a>
                            <div class="task_title">Заехать на СТО</div>
                            <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                            <ul class="task_notifications">
                                <li>
                                    <span class="ico priority_b"></span>
                                </li>
                                <li>
                                    <span class="ico repeat"></span>
                                </li>
                                <li>
                                    <span class="ico reminder"></span>
                                </li>
                                <li>
                                    <span class="ico place"></span>
                                </li>
                                <li>
                                    <span class="ico list"></span>
                                </li>
                                <li>
                                    <span class="ico executor"></span>
                                </li>
                                <li>
                                    <span class="ico files"></span>
                                </li>
                                <li>
                                    <span class="ico comments"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="popup_footer">
                    <a href="#" class="button cancel">Отмена</a>
                    <input type="submit" value="Готово" />
                </div>
            </form>
        </div>
        <!--popup 2-->
        <div class="popup dialog" style="top:10px; left: 480px;">
            <div class="popup_header">
                <div class="title">Безымянное модальное окно</div>
                <a href="#" class="close"></a>
            </div>
            <form action="#" >
                <div class="popup_content">
                    <div class="subtitle">Хотите взять с полки пирожок?</div>
                    <div>
                        Несмотря на сложности, процесс стратегического планирования подсознательно специфицирует нишевый проект. Опрос без оглядки на авторитеты многопланово концентрирует повседневный отраслевой стандарт. Позиционирование на рынке, конечно, слабо искажает анализ рыночных цен.
                    </div>
                </div>
                <div class="popup_footer">
                    <a href="#" class="button cancel">Нет</a>
                    <input type="submit" value="Да" />
                </div>
            </form>
        </div>
        <div class="popup dialog" style="top:310px; left: 480px;">
            <div class="popup_header">
                <div class="title">Переименовать список</div>
                <a href="#" class="close"></a>
            </div>
            <form action="#" >
                <div class="popup_content">
                    <input type="text" value="Список «А»" />
                </div>
                <div class="popup_footer">
                    <a href="#" class="button cancel">Отменить</a>
                    <input type="submit" value="Готово" />
                </div>
            </form>
        </div>
        <div class="popup dialog" style="top:10px; left: 950px;">
            <div class="popup_header">
                <div class="title">Настройки доступа к списку «Список А»</div>
                <a href="#" class="close"></a>
            </div>
            <form action="#" >
                <div class="popup_content">
                    <div class="row10">
                        <div class="list_user_title">Владелец</div>
                        <div class="list_user">
                            <div class="ava">
                                <img src="./images/ava/male.png" />
                            </div>
                            <div class="name">Александр Иванов</div>
                            <div class="email">alex.ivan@gmail.com</div>
                        </div>
                    </div>
                    <div class="row10">
                        <div class="list_user_title">Общий доступ:</div>
                        <div class="list_user">
                            <a href="#" class="delete"></a>
                            <div class="ava">
                                <img src="./images/ava/female.png" />
                            </div>
                            <div class="name">Наталия Натальева</div>
                            <div class="email">natalia@gmail.com</div>
                        </div>
                        <div class="list_user">
                            <a href="#" class="delete"></a>
                            <div class="ava">
                                <img src="./images/ava/male.png" />
                            </div>
                            <div class="name">Роман Хабибулин</div>
                            <div class="email">romka@gmail.com</div>
                        </div>
                        <div class="list_user">
                            <a href="#" class="delete"></a>
                            <div class="ava">
                                <img src="./images/ava/male.png" />
                            </div>
                            <div class="name">Сергей Карпатов</div>
                            <div class="email">sergey.karp@gmail.com</div>
                        </div>
                    </div>
                    <div class="row10">
                        <label>Добавить пользователя:</label>
                        <input type="text" value="" placeholder="Введите имя или адрес электронной почты" 
                               onfocus="$('.list_user_add_wrapper').css('display','block');" 
                               />
                    </div>
                    <div class="list_user_add_wrapper">
                        <a href="" class="button">Добавить</a>
                        <div class="checkbox_toggle">
                            <input type="checkbox" checked="1"/>
                            <label for="check" class="checkbox">Оповестить по электронной почте</label>
                        </div>
                    </div>
                </div>
                <div class="popup_footer">
                    <a href="#" class="button cancel">Отменить</a>
                    <input type="submit" value="Готово" />
                </div>
            </form>
        </div>
    </body>
</html>
