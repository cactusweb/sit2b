<div class="lists">
    <div class="lists_header">
        <ul class="lists_nav">
            <li>
                <a href="#" class="prev"></a>
            </li>
            <li>
                <a href="#" class="next"></a>
            </li>
        </ul>
        <div class="title">Списки задач (7)</div>
        <ul class="lists_controls">
            <li class="menu" >
                <a href="#" class="menu lists_settings"></a>
            </li>
            <li>
                <a href="#" class="search"></a>
            </li>
            <li>
                <a href="#" class="add addlist"></a>
            </li>
        </ul>
        <div class="popup lists_settings">
            <div class="popup_header">
                <span class="tail"></span>
                Настройки списков
                <a href="#" class="close"></a>
            </div>
            <form action="#">
                <div class="popup_body">
                    <div class="row checkbox_toggle">
                        <input type="checkbox" checked="1"/>
                        <label for="check" class="checkbox">Показывать выполненные задачи</label>
                    </div>
                    <div class="popup_footer">
                        <div class="ico trashbox"></div>
                        <div>В корзине задач: 198</div>
                        <div><a href="#">Посмотреть удаленные</a></div>
                    </div>
                </div>
            </form>  
        </div>
        <div class="popup addlist">
            <div class="popup_header">
                <span class="tail"></span>
                Создание нового списка
                <a href="#" class="close"></a>
            </div>
            <form action="#">
                <div class="popup_body">
                        <div class="clear">
                            <div class="popup_addlist_col_left">
                                <div class="row">
                                    <label>Название нового списка</label>
                                    <input type="text" name="name"  class="" placeholder="Новый список 1" />
                                </div>
                                <div class="row">
                                    <label>Сортировать</label>
                                    <select name="sort" class="sorted">
                                        <option value="1" data-class="desc" >По дате добавления</option>
                                        <option value="2" data-class="asc" >По дате добавления</option>
                                        <option value="3" data-class="desc" >По названию задач</option>
                                        <option value="4" data-class="asc" >По названию задач</option>
                                    </select> 
                                </div>
                                <div class="row">
                                    <label>Продолжительность задачи по­ умолчанию:</label>
                                    <select name="duration" class="">
                                        <option value="1">1 час</option>
                                        <option value="1">1 день</option>
                                        <option value="1">1 неделя</option>
                                        <option value="1">1 месяц</option>
                                    </select> 
                                </div>
                            </div>
                            <div class="popup_addlist_col_right">
                                <div class="row">
                                    <label>Цвет списка</label>
                                    <ul class="colors">
                                        <input type="hidden" value="" name="color" class="color" />

                                        <li class="color_fdbebe" data-color="color_fdbebe" ></li>
                                        <li class="color_ffccb3 selected" data-color="color_ffccb3" ></li>
                                        <li class="color_ffdd99" data-color="color_ffdd99" ></li>
                                        <li class="color_ffffa6" data-color="color_ffffa6" ></li>
                                        <li class="color_e6ffb3" data-color="color_e6ffb3" ></li>
                                        <li class="color_ccffb3" data-color="color_ccffb3" ></li>
                                        <li class="color_bcfad1" data-color="color_bcfad1" ></li>
                                        <li class="color_bcf6fa" data-color="color_bcf6fa" ></li>

                                        <li class="color_bce5fa" data-color="color_bce5fa" ></li>
                                        <li class="color_c3d1fa" data-color="color_c3d1fa" ></li>
                                        <li class="color_fac3f1" data-color="color_fac3f1" ></li>
                                        <li class="color_e8c3fa" data-color="color_e8c3fa" ></li>
                                        <li class="color_d1c3fa" data-color="color_d1c3fa" ></li>
                                        <li class="color_e6cfcf" data-color="color_e6cfcf" ></li>
                                        <li class="color_e6decf" data-color="color_e6decf" ></li>
                                        <li class="color_cfe6d7" data-color="color_cfe6d7" ></li>

                                        <li class="color_dee6cf" data-color="color_dee6cf" ></li>
                                        <li class="color_cfe4e6" data-color="color_cfe4e6" ></li>
                                        <li class="color_cfd5e6" data-color="color_cfd5e6" ></li>
                                        <li class="color_e7dce6" data-color="color_e7dce6" ></li>
                                        <li class="color_decfe6" data-color="color_decfe6" ></li>
                                        <li class="color_f2f2f2" data-color="color_f2f2f2" ></li>
                                        <li class="color_e6e6e6" data-color="color_e6e6e6" ></li>
                                        <li class="color_cccccc" data-color="color_cccccc" ></li>
                                    </ul>
                                </div>
                                <div class="row checkbox_toggle">
                                    <input type="checkbox" checked="1"/>
                                    <label for="check" class="checkbox">Показывать выполненные задачи</label>
                                </div>
                                <div class="row checkbox_toggle">
                                    <input type="checkbox" checked="1"/>
                                    <label for="check" class="checkbox">Показывать задачи, которые запланированы в календаре</label>
                                </div>
                                <div class="row checkbox_toggle">
                                    <input type="checkbox" checked="1"/>
                                    <label for="check" class="checkbox">Показывать задачи, которые не запланированы в календаре</label>
                                </div>
                                <div class="row checkbox_toggle">
                                    <input type="checkbox"/>
                                    <label for="check" class="checkbox">Отмечать прошедшие задачи,  как выполненные</label>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="popup_footer">
                    <input type="submit" value="Создать новый список" />
                </div>
            </form>  
        </div>
    </div>
    <div class="lists_wrapper" style="width: 840px;">
        <!--LIST #1-->
        <div class="list color_cfe6d7">
            <div class="list_header">
                <div class="title"><span>Входящие</span></div>
                <div class="tasks">Задачи: 5 (<span class="done">1</span>)</div>
                <a href="#" class="list_menu"></a>
                <div class="popup list_settings">
                    <div class="popup_header">
                        <span class="tail"></span>
                        Настройки списка <a href="#">(Вернуть настройки по умолчанию)</a>
                        <a href="#" class="close"></a>
                    </div>
                    <form action="#">
                        <div class="popup_body">
                            <div class="clear">
                                <div class="popup_addlist_col_left">
                                    <div class="row">
                                        <label>Сортировать</label>
                                        <select name="sort" class="sorted">
                                            <option value="1" data-class="desc" >По дате добавления</option>
                                            <option value="2" data-class="asc" >По дате добавления</option>
                                            <option value="3" data-class="desc" >По названию задач</option>
                                            <option value="4" data-class="asc" >По названию задач</option>
                                        </select> 
                                    </div>
                                    <div class="row">
                                        <label>Фильтровать по назначеному исполнителю</label>
                                        <select name="filter">
                                            <option value="1">Все исполнители</option>
                                            <option value="2">Владимир Петров</option>
                                            <option value="3">Владимир Петров2</option>
                                            <option value="4">Владимир Петров3</option>
                                        </select> 
                                    </div>
                                    <div class="row">
                                        <label>Продолжительность задачи по умолчанию:</label>
                                        <select name="duration" class="">
                                            <option value="1">1 час</option>
                                            <option value="1">1 день</option>
                                            <option value="1">1 неделя</option>
                                            <option value="1">1 месяц</option>
                                        </select> 
                                    </div>
                                    <div class="subtitle"><img src="./images/ico/shared.png" /> Общий доступ к списку</div>
                                    <div class="shares">
                                        <div>Доступно: 4 человека</div>
                                        <div class="users">(Зинаида Кораблева, Наталия Натальева, Роман Хабибулин, Сергей Карпатов)</div>
                                        <div><a href="#">Изменить настройки доступа</a></div>
                                    </div>
                                </div>
                                <div class="popup_addlist_col_right">
                                    <div class="row">
                                        <label>Цвет списка</label>
                                        <ul class="colors">
                                            <input type="hidden" value="" name="color" class="color" />

                                            <li class="color_fdbebe" data-color="color_fdbebe" ></li>
                                            <li class="color_ffccb3 selected" data-color="color_ffccb3" ></li>
                                            <li class="color_ffdd99" data-color="color_ffdd99" ></li>
                                            <li class="color_ffffa6" data-color="color_ffffa6" ></li>
                                            <li class="color_e6ffb3" data-color="color_e6ffb3" ></li>
                                            <li class="color_ccffb3" data-color="color_ccffb3" ></li>
                                            <li class="color_bcfad1" data-color="color_bcfad1" ></li>
                                            <li class="color_bcf6fa" data-color="color_bcf6fa" ></li>

                                            <li class="color_bce5fa" data-color="color_bce5fa" ></li>
                                            <li class="color_c3d1fa" data-color="color_c3d1fa" ></li>
                                            <li class="color_fac3f1" data-color="color_fac3f1" ></li>
                                            <li class="color_e8c3fa" data-color="color_e8c3fa" ></li>
                                            <li class="color_d1c3fa" data-color="color_d1c3fa" ></li>
                                            <li class="color_e6cfcf" data-color="color_e6cfcf" ></li>
                                            <li class="color_e6decf" data-color="color_e6decf" ></li>
                                            <li class="color_cfe6d7" data-color="color_cfe6d7" ></li>

                                            <li class="color_dee6cf" data-color="color_dee6cf" ></li>
                                            <li class="color_cfe4e6" data-color="color_cfe4e6" ></li>
                                            <li class="color_cfd5e6" data-color="color_cfd5e6" ></li>
                                            <li class="color_e7dce6" data-color="color_e7dce6" ></li>
                                            <li class="color_decfe6" data-color="color_decfe6" ></li>
                                            <li class="color_f2f2f2" data-color="color_f2f2f2" ></li>
                                            <li class="color_e6e6e6" data-color="color_e6e6e6" ></li>
                                            <li class="color_cccccc" data-color="color_cccccc" ></li>
                                        </ul>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox" checked="1"/>
                                        <label for="check" class="checkbox">Показывать выполненные задачи</label>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox" checked="1"/>
                                        <label for="check" class="checkbox">Показывать задачи, которые запланированы в календаре</label>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox" checked="1"/>
                                        <label for="check" class="checkbox">Показывать задачи, которые не запланированы в календаре</label>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox"/>
                                        <label for="check" class="checkbox">Отмечать прошедшие задачи,  как выполненные</label>
                                    </div>
                                    <div  class="list_actions" >
                                        <div class="subtitle">Действия со списком</div>
                                        <ul>
                                            <li><a href="#">Переименовать</a></li>
                                            <li><a href="#">Объединить</a></li>
                                            <li><a href="#">Удалить</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>  
                </div>
            </div>
            <div class="tasks">
                <div class="task create">
                    <input type="text" value="" placeholder="Создать задачу" class="create_task" onfocus="$(this).addClass('active'); $(this).attr('placeholder', '');" onblur="$(this).removeClass('active'); $(this).attr('placeholder', 'Создать задачу');" />
                </div>
                <div class="task" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Продумать варианты скрытия панели Sit2b.</div>
                    <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_a"></span>
                        </li>
                        <li>
                            <span class="ico repeat"></span>
                        </li>
                        <li>
                            <span class="ico reminder"></span>
                        </li>
                        <li>
                            <span class="ico place"></span>
                        </li>
                        <li>
                            <span class="ico list"></span>
                        </li>
                        <li>
                            <span class="ico executor"></span>
                        </li>
                        <li>
                            <span class="ico files active"></span>
                        </li>
                        <li>
                            <span class="ico comments active"></span>
                        </li>
                    </ul>
                </div>
                <div class="task" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Заехать на СТО</div>
                    <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_b"></span>
                        </li>
                        <li>
                            <span class="ico repeat"></span>
                        </li>
                        <li>
                            <span class="ico reminder"></span>
                        </li>
                        <li>
                            <span class="ico place"></span>
                        </li>
                        <li>
                            <span class="ico list"></span>
                        </li>
                        <li>
                            <span class="ico executor"></span>
                        </li>
                        <li>
                            <span class="ico files"></span>
                        </li>
                        <li>
                            <span class="ico comments"></span>
                        </li>
                    </ul>
                </div>
                <div class="task nobg" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Тема задачи, которая очень длинная и переносится на вторую, третью и, возможно даже четвертую строку и следующую строку</div>
                    <div class="task_date time"><span>1 час</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_c"></span>
                        </li>
                    </ul>
                </div>
                <div class="task past" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Сделать генеральную уборку в доме</div>
                    <div class="task_date"><span>11:00-11:30, вчера</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_d"></span>
                        </li>
                        <li>
                            <span class="ico repeat_out active"></span>
                        </li>
                        <li>
                            <span class="ico reminder"></span>
                        </li>
                        <li>
                            <span class="ico place"></span>
                        </li>
                    </ul>
                </div>
                <div class="task done" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Позвонить бабушке и поздравить с Днем Рожденья</div>
                    <div class="task_date"><span>11:00-11:30, сегодня</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_a"></span>
                        </li>
                        <li>
                            <span class="ico reminder"></span>
                        </li>
                        <li>
                            <span class="ico place"></span>
                        </li>
                        <li>
                            <span class="ico list"></span>
                        </li>
                        <li>
                            <span class="ico executor"></span>
                        </li>
                        <li>
                            <span class="ico files"></span>
                        </li>
                        <li>
                            <span class="ico comments"></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--LIST #2-->
        <div class="list shared color_bce5fa">
            <div class="list_header">
                <div class="title"><span>Список «A»</span></div>
                <div class="tasks">Задачи: 5 (<span class="done">1</span>)</div>
                <div class="list_filter">
                    <div href="#" class="list_sort_wrapper">
                        <select id="list_sort">
                            <option value="1" data-class="desc" >Сортировка1</option>
                            <option value="2" data-class="asc" >Сортировка1</option>
                            <option value="3" data-class="desc" >Сортировка2</option>
                            <option value="4" data-class="asc" >Сортировка2</option>
                            <option value="5" data-class="desc" >Сортировка3</option>
                            <option value="6" data-class="asc" >Сортировка3</option>
                        </select>
                    </div>
                    <div class="filter_by_user">Зинаида Кораблева <a href="#" class="sort_delete"></a></div>
                </div>
                <a href="#" class="list_menu"></a>
                <div class="popup list_settings">
                    <div class="popup_header">
                        <span class="tail"></span>
                        Настройки списка <a href="#">(Вернуть настройки по умолчанию)</a>
                        <a href="#" class="close"></a>
                    </div>
                    <form action="#">
                        <div class="popup_body">
                            <div class="clear">
                                <div class="popup_addlist_col_left">
                                    <div class="row">
                                        <label>Сортировать</label>
                                        <select name="sort" class="sorted">
                                            <option value="1" data-class="desc" >По дате добавления</option>
                                            <option value="2" data-class="asc" >По дате добавления</option>
                                            <option value="3" data-class="desc" >По названию задач</option>
                                            <option value="4" data-class="asc" >По названию задач</option>
                                        </select> 
                                    </div>
                                    <div class="row">
                                        <label>Фильтровать по назначеному исполнителю</label>
                                        <select name="filter">
                                            <option value="1">Все исполнители</option>
                                            <option value="2">Владимир Петров</option>
                                            <option value="3">Владимир Петров2</option>
                                            <option value="4">Владимир Петров3</option>
                                        </select> 
                                    </div>
                                    <div class="row">
                                        <label>Продолжительность задачи по умолчанию:</label>
                                        <select name="duration" class="">
                                            <option value="1">1 час</option>
                                            <option value="1">1 день</option>
                                            <option value="1">1 неделя</option>
                                            <option value="1">1 месяц</option>
                                        </select> 
                                    </div>
                                    <div class="subtitle"><img src="./images/ico/shared.png" /> Общий доступ к списку</div>
                                    <div class="shares">
                                        <div>Доступно: 4 человека</div>
                                        <div class="users">(Зинаида Кораблева, Наталия Натальева, Роман Хабибулин, Сергей Карпатов)</div>
                                        <div><a href="#">Изменить настройки доступа</a></div>
                                    </div>
                                </div>
                                <div class="popup_addlist_col_right">
                                    <div class="row">
                                        <label>Цвет списка</label>
                                        <ul class="colors">
                                            <input type="hidden" value="" name="color" class="color" />

                                            <li class="color_fdbebe" data-color="color_fdbebe" ></li>
                                            <li class="color_ffccb3 selected" data-color="color_ffccb3" ></li>
                                            <li class="color_ffdd99" data-color="color_ffdd99" ></li>
                                            <li class="color_ffffa6" data-color="color_ffffa6" ></li>
                                            <li class="color_e6ffb3" data-color="color_e6ffb3" ></li>
                                            <li class="color_ccffb3" data-color="color_ccffb3" ></li>
                                            <li class="color_bcfad1" data-color="color_bcfad1" ></li>
                                            <li class="color_bcf6fa" data-color="color_bcf6fa" ></li>

                                            <li class="color_bce5fa" data-color="color_bce5fa" ></li>
                                            <li class="color_c3d1fa" data-color="color_c3d1fa" ></li>
                                            <li class="color_fac3f1" data-color="color_fac3f1" ></li>
                                            <li class="color_e8c3fa" data-color="color_e8c3fa" ></li>
                                            <li class="color_d1c3fa" data-color="color_d1c3fa" ></li>
                                            <li class="color_e6cfcf" data-color="color_e6cfcf" ></li>
                                            <li class="color_e6decf" data-color="color_e6decf" ></li>
                                            <li class="color_cfe6d7" data-color="color_cfe6d7" ></li>

                                            <li class="color_dee6cf" data-color="color_dee6cf" ></li>
                                            <li class="color_cfe4e6" data-color="color_cfe4e6" ></li>
                                            <li class="color_cfd5e6" data-color="color_cfd5e6" ></li>
                                            <li class="color_e7dce6" data-color="color_e7dce6" ></li>
                                            <li class="color_decfe6" data-color="color_decfe6" ></li>
                                            <li class="color_f2f2f2" data-color="color_f2f2f2" ></li>
                                            <li class="color_e6e6e6" data-color="color_e6e6e6" ></li>
                                            <li class="color_cccccc" data-color="color_cccccc" ></li>
                                        </ul>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox" checked="1"/>
                                        <label for="check" class="checkbox">Показывать выполненные задачи</label>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox" checked="1"/>
                                        <label for="check" class="checkbox">Показывать задачи, которые запланированы в календаре</label>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox" checked="1"/>
                                        <label for="check" class="checkbox">Показывать задачи, которые не запланированы в календаре</label>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox"/>
                                        <label for="check" class="checkbox">Отмечать прошедшие задачи,  как выполненные</label>
                                    </div>
                                    <div  class="list_actions" >
                                        <div class="subtitle">Действия со списком</div>
                                        <ul>
                                            <li><a href="#">Переименовать</a></li>
                                            <li><a href="#">Объединить</a></li>
                                            <li><a href="#">Удалить</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>  
                </div>
            </div>
            <div class="tasks">
                <div class="task create">
                    <input type="text" value="" placeholder="Создать задачу" class="create_task" onfocus="$(this).addClass('active'); $(this).attr('placeholder', '');" onblur="$(this).removeClass('active'); $(this).attr('placeholder', 'Создать задачу');" />
                </div>
                <div class="task" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Внести измеенеия в макеты Sit2B с совещания</div>
                    <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_a"></span>
                        </li>
                        <li>
                            <span class="ico repeat"></span>
                        </li>
                        <li>
                            <span class="ico reminder"></span>
                        </li>
                        <li>
                            <span class="ico place"></span>
                        </li>
                        <li>
                            <span class="ico list"></span>
                        </li>
                        <li>
                            <span class="ico executor"></span>
                        </li>
                        <li>
                            <span class="ico files active"></span>
                        </li>
                        <li>
                            <span class="ico comments active"></span>
                        </li>
                    </ul>
                </div>
                <div class="task nobg" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Закончить составление отчета по рабочим делам</div>
                    <div class="task_date time"><span>1 час</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_d"></span>
                        </li>
                    </ul>
                </div>
                <div class="task" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Заехать на СТО</div>
                    <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_b"></span>
                        </li>
                        <li>
                            <span class="ico reminder active"></span>
                        </li>
                    </ul>
                </div>
                <div class="task past" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Сделать уборку в доме</div>
                    <div class="task_date"><span>11:00-11:30, вчера</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_d"></span>
                        </li>
                        <li>
                            <span class="ico repeat_out active"></span>
                        </li>
                        <li>
                            <span class="ico reminder"></span>
                        </li>
                        <li>
                            <span class="ico place"></span>
                        </li>
                    </ul>
                </div>
                <div class="task done" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Заскочить за продуктами</div>
                    <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_b"></span>
                        </li>
                        <li>
                            <span class="ico reminder"></span>
                        </li>
                        <li>
                            <span class="ico repeat"></span>
                        </li>
                        <li>
                            <span class="ico place"></span>
                        </li>
                        <li>
                            <span class="ico list"></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--LIST #3-->
        <div class="list color_ffffa6">
            <div class="list_header">
                <div class="title"><span>Список «B»</span></div>
                <div class="tasks">Задачи: 3</div>
                <a href="#" class="list_menu"></a>
                <div class="popup list_settings">
                    <div class="popup_header">
                        <span class="tail"></span>
                        Настройки списка <a href="#">(Вернуть настройки по умолчанию)</a>
                        <a href="#" class="close"></a>
                    </div>
                    <form action="#">
                        <div class="popup_body">
                            <div class="clear">
                                <div class="popup_addlist_col_left">
                                    <div class="row">
                                        <label>Сортировать</label>
                                        <select name="sort" class="sorted">
                                            <option value="1" data-class="desc" >По дате добавления</option>
                                            <option value="2" data-class="asc" >По дате добавления</option>
                                            <option value="3" data-class="desc" >По названию задач</option>
                                            <option value="4" data-class="asc" >По названию задач</option>
                                        </select> 
                                    </div>
                                    <div class="row">
                                        <label>Фильтровать по назначеному исполнителю</label>
                                        <select name="filter">
                                            <option value="1">Все исполнители</option>
                                            <option value="2">Владимир Петров</option>
                                            <option value="3">Владимир Петров2</option>
                                            <option value="4">Владимир Петров3</option>
                                        </select> 
                                    </div>
                                    <div class="row">
                                        <label>Продолжительность задачи по умолчанию:</label>
                                        <select name="duration" class="">
                                            <option value="1">1 час</option>
                                            <option value="1">1 день</option>
                                            <option value="1">1 неделя</option>
                                            <option value="1">1 месяц</option>
                                        </select> 
                                    </div>
                                    <div class="subtitle"><img src="./images/ico/shared.png" /> Общий доступ к списку</div>
                                    <div class="shares">
                                        <div>Доступно: 4 человека</div>
                                        <div class="users">(Зинаида Кораблева, Наталия Натальева, Роман Хабибулин, Сергей Карпатов)</div>
                                        <div><a href="#">Изменить настройки доступа</a></div>
                                    </div>
                                </div>
                                <div class="popup_addlist_col_right">
                                    <div class="row">
                                        <label>Цвет списка</label>
                                        <ul class="colors">
                                            <input type="hidden" value="" name="color" class="color" />

                                            <li class="color_fdbebe" data-color="color_fdbebe" ></li>
                                            <li class="color_ffccb3 selected" data-color="color_ffccb3" ></li>
                                            <li class="color_ffdd99" data-color="color_ffdd99" ></li>
                                            <li class="color_ffffa6" data-color="color_ffffa6" ></li>
                                            <li class="color_e6ffb3" data-color="color_e6ffb3" ></li>
                                            <li class="color_ccffb3" data-color="color_ccffb3" ></li>
                                            <li class="color_bcfad1" data-color="color_bcfad1" ></li>
                                            <li class="color_bcf6fa" data-color="color_bcf6fa" ></li>

                                            <li class="color_bce5fa" data-color="color_bce5fa" ></li>
                                            <li class="color_c3d1fa" data-color="color_c3d1fa" ></li>
                                            <li class="color_fac3f1" data-color="color_fac3f1" ></li>
                                            <li class="color_e8c3fa" data-color="color_e8c3fa" ></li>
                                            <li class="color_d1c3fa" data-color="color_d1c3fa" ></li>
                                            <li class="color_e6cfcf" data-color="color_e6cfcf" ></li>
                                            <li class="color_e6decf" data-color="color_e6decf" ></li>
                                            <li class="color_cfe6d7" data-color="color_cfe6d7" ></li>

                                            <li class="color_dee6cf" data-color="color_dee6cf" ></li>
                                            <li class="color_cfe4e6" data-color="color_cfe4e6" ></li>
                                            <li class="color_cfd5e6" data-color="color_cfd5e6" ></li>
                                            <li class="color_e7dce6" data-color="color_e7dce6" ></li>
                                            <li class="color_decfe6" data-color="color_decfe6" ></li>
                                            <li class="color_f2f2f2" data-color="color_f2f2f2" ></li>
                                            <li class="color_e6e6e6" data-color="color_e6e6e6" ></li>
                                            <li class="color_cccccc" data-color="color_cccccc" ></li>
                                        </ul>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox" checked="1"/>
                                        <label for="check" class="checkbox">Показывать выполненные задачи</label>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox" checked="1"/>
                                        <label for="check" class="checkbox">Показывать задачи, которые запланированы в календаре</label>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox" checked="1"/>
                                        <label for="check" class="checkbox">Показывать задачи, которые не запланированы в календаре</label>
                                    </div>
                                    <div class="row checkbox_toggle">
                                        <input type="checkbox"/>
                                        <label for="check" class="checkbox">Отмечать прошедшие задачи,  как выполненные</label>
                                    </div>
                                    <div  class="list_actions" >
                                        <div class="subtitle">Действия со списком</div>
                                        <ul>
                                            <li><a href="#">Переименовать</a></li>
                                            <li><a href="#">Объединить</a></li>
                                            <li><a href="#">Удалить</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>  
                </div>
            </div>
            <div class="tasks">
                <div class="task create">
                    <input type="text" value="" placeholder="Создать задачу" class="create_task" onfocus="$(this).addClass('active'); $(this).attr('placeholder', '');" onblur="$(this).removeClass('active'); $(this).attr('placeholder', 'Создать задачу');" />
                </div>
                <div class="task" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Продумать варианты скрытия панели Sit2b.</div>
                    <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_a"></span>
                        </li>
                        <li>
                            <span class="ico repeat"></span>
                        </li>
                        <li>
                            <span class="ico reminder"></span>
                        </li>
                        <li>
                            <span class="ico place"></span>
                        </li>
                        <li>
                            <span class="ico list"></span>
                        </li>
                        <li>
                            <span class="ico executor"></span>
                        </li>
                        <li>
                            <span class="ico files active"></span>
                        </li>
                        <li>
                            <span class="ico comments active"></span>
                        </li>
                    </ul>
                </div>
                <div class="task" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Заехать на СТО</div>
                    <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_b"></span>
                        </li>
                        <li>
                            <span class="ico repeat"></span>
                        </li>
                        <li>
                            <span class="ico reminder"></span>
                        </li>
                        <li>
                            <span class="ico place"></span>
                        </li>
                        <li>
                            <span class="ico list"></span>
                        </li>
                        <li>
                            <span class="ico executor"></span>
                        </li>
                        <li>
                            <span class="ico files"></span>
                        </li>
                        <li>
                            <span class="ico comments"></span>
                        </li>
                    </ul>
                </div>
                <div class="task nobg" >
                    <a href="#" class="mode_toggle"></a>
                    <div class="task_title">Тема задачи, которая очень длинная и переносится на вторую, третью и, возможно даже четвертую строку и следующую строку</div>
                    <div class="task_date time"><span>1 час</span></div>
                    <ul class="task_notifications">
                        <li>
                            <span class="ico priority_c"></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>