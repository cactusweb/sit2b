<div class="calendar_wrapper">
    <div class="calendar_top">
        <div class="calendar_lists">
            <div class="wrapper">
                <a href="#" class="block_menu"></a>
                <div class="title">Мои списки задач</div>
                <ul class="colors">
                    <li class="color_fdbebe">Список «А»</li>
                    <li class="color_ffccb3">Список «B»</li>
                    <li class="color_ffffa6 active">Рабочее</li>
                    <li class="color_e6ffb3">Личное</li>
                    <li class="color_ccffb3">Список «С»</li>
                    <li class="color_bcfad1">Список «C»</li>
                    <li class="color_bce5fa">Список «E»</li>
                    <li class="color_c3d1fa active">Задачи по дизайну</li>
                    <li class="color_fac3f1">Личные встречи</li>
                    <li class="color_cfe4e6">Дни Рождения</li>
                </ul>
            </div>
        </div>
        <div class="calendar_datepicker_wrapper">
            <div class="calendar_datepicker">
                <img src="./images/datepicker.jpg" />
            </div>
        </div>
    </div>
    <div class="calendar_body">
        <div class="calendar_header">
            <ul class="calendar_nav">
                <li>
                    <a href="#" class="prev"></a>
                </li>
                <li>
                    <a href="#" class="next"></a>
                </li>
            </ul>
            <div class="title">9 – 12 сент 2014</div>
            <form class="calendar_show_by">
                <div id="calendar_show_by">
                    <input type="radio" id="calendar_radio1" name="radio"><label for="calendar_radio1">День</label>
                    <input type="radio" id="calendar_radio2" name="radio" checked="checked"><label for="calendar_radio2">4 дня</label>
                    <input type="radio" id="calendar_radio3" name="radio"><label for="calendar_radio3">Неделя</label>
                </div>
            </form>
        </div>
        <div class="calendar">
            <table class="calendar">
                <tr>
                    <th></th>
                    <th class="today">Вт, 9/9</th>
                    <th>Ср, 10/9</th>
                    <th>Чт, 11/9</th>
                    <th>Пт, 12/9</th>
                </tr>
                <tr>
                    <td>
                        <div class="">День</div>
                    </td>
                    <td class="today"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <div class="hour">00:00</div>
                        <div class="hour">01:00</div>
                        <div class="hour">02:00</div>
                        <div class="hour">03:00</div>
                        <div class="hour">04:00</div>
                        <div class="hour">05:00</div>
                        <div class="hour">06:00</div>
                        <div class="hour">07:00</div>
                        <div class="hour">08:00</div>
                        <div class="hour">09:00</div>
                        <div class="hour">10:00</div>
                        <div class="hour">11:00</div>
                        <div class="hour">12:00</div>
                        <div class="hour">13:00</div>
                        <div class="hour"><span class="current_time" style="top: 50%;"></span>14:00</div>
                        <div class="hour">15:00</div>
                        <div class="hour">16:00</div>
                        <div class="hour">17:00</div>
                        <div class="hour">18:00</div>
                        <div class="hour">19:00</div>
                        <div class="hour">20:00</div>
                        <div class="hour">21:00</div>
                        <div class="hour">22:00</div>
                        <div class="hour">23:00</div>
                    </td>
                    <td class="today" >
                        <div class="grid tasks">
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour">
                                <div class="task_wrapper color_cfe6d7" style="height: 106px;">
                                    <div class="task done">
                                        <a href="#" class="mode_toggle"></a>
                                        <div class="task_title">Продумать варианты скрытия панели Sit2b.</div>
                                        <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="hour"></div>
                            <div class="hour">
                                <div class="task_wrapper color_cfe6d7 overtask">
                                    <div class="task done">
                                        <a href="#" class="mode_toggle"></a>
                                        <div class="task_title">Позвонить бубу</div>
                                    </div>
                                </div>
                            </div>
                            <div class="hour"></div>
                            <div class="hour">
                                <div class="task_wrapper color_bce5fa" style="height: 24px;">
                                    <div class="task done">
                                        <a href="#" class="mode_toggle"></a>
                                        <div class="task_title">Заскочить за продуктами</div>
                                        <div class="task_date"><span>13:00-14:00, сегодня</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="hour">
                                <span class="current_time" style="top: 60%;"></span>
                                <div class="task_wrapper color_bce5fa" style="height: 24px;">
                                    <div class="task">
                                        <a href="#" class="mode_toggle"></a>
                                        <div class="task_title">Заехать на СТО</div>
                                        <div class="task_date"><span>14:00-15:00, сегодня</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="hour">
                                <div class="task_wrapper color_ffffa6" style="height: 106px;">
                                    <div class="task">
                                        <a href="#" class="mode_toggle"></a>
                                        <div class="task_title">Добавить кнопки действия</div>
                                        <div class="task_date"><span>15:00-17:00, сегодня</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                        </div>
                    </td>
                    <td>
                        <div class="grid tasks">
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                        </div>
                    </td>
                    <td>
                        <div class="grid tasks">
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour">
                                <div class="task_wrapper color_fdbebe" style="height: 24px;">
                                    <div class="task">
                                        <a href="#" class="mode_toggle"></a>
                                        <div class="task_title">Заехать на СТО</div>
                                        <div class="task_date"><span>08:00-09:00, четверг</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                        </div>
                    </td>
                    <td>
                        <div class="grid tasks">
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                            <div class="hour"></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>