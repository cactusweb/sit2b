<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>sit2B</title>
        <script type="text/javascript" src="./js/jquery-1.11.0.js"></script>
<!--        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>-->
        <link rel="stylesheet" href="./css/jquery-ui.css" />
        <script src="./js/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="./css/reset.css" />
        <link rel="stylesheet" href="./css/style.css?v=1" />
        <script type="text/javascript" src="./js/script.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="overlay"></div>
        <div class="popup notifications">
            <div class="popup_header">
                <div class="title">Уведомления (3 новых)</div>
                <a href="#" class="menu"></a>
                <a href="#" class="close"></a>
            </div>
            <div class="popup_content">
                <div class="notification active">
                    <div class="clear">
                        <div class="notification_ico"><img src="./images/ico/task_part/reminder.png"><span></span></div>
                        <div class="notification_info">
                            <div class="notification_date">09 сентября 2014, 11:20</div>
                            <div class="notification_text">Напоминание: Совещание по органайзеру. - вт, 16 сент 2014 09:30 - 10:30</div>
                            <div><a href="#" class="read_more">Подробнее</a></div>
                        </div>
                    </div>
                </div>
                <div class="notification active comment">
                    <div class="clear">
                        <div class="notification_ico"><img src="./images/ico/task_part/comments.png"><span></span></div>
                        <div class="notification_info">
                            <div class="notification_date">09 сентября 2014, 11:20</div>
                            <div class="notification_comment">Комментарий от Джессики:</div>
                            <div class="notification_text">Посмотри договор с компанией Сит-Девелоперс</div>
                            <div><a href="#" class="read_more">Подробнее</a></div>
                        </div>
                    </div>
                </div>
                <div class="notification active">
                    <div class="clear">
                        <div class="notification_ico"><img src="./images/ico/task_part/delete.png"><span></span></div>
                        <div class="notification_info">
                            <div class="notification_date">09 сентября 2014, 11:20</div>
                            <div class="notification_text">Вы удалили задачу «Просмотреть новости, узнать тренды в списках прокомментированых новостей»</div>
                            <div><a href="#" class="read_more">Подробнее</a></div>
                        </div>
                    </div>
                </div>
                <div class="notification">
                    <div class="clear">
                        <div class="notification_ico"><img src="./images/ico/task_part/calendar.png"><span></span></div>
                        <div class="notification_info">
                            <div class="notification_date">09 сентября 2014, 11:20</div>
                            <div class="notification_text">Вы удалили задачу «Просмотреть новости, узнать тренды в списках прокомментированых новостей»</div>
                            <div><a href="#" class="read_more">Подробнее</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup new_task">
            <div class="popup_header">
                <a href="#" class="save button">Сохранить</a>
                <a href="#" class="delete"></a>
                <div class="title">Создание задачи</div>
                <a href="#" class="close">Закрыть</a>
            </div>
            <form action="#">
                <div class="body">
                    <div class="row">
                        <label>Тема <div class="author">Автор: <span>Я (Nikolay Dobin)</span></div></label>
                        <input type="text" class="task_title" placeholder="Задача без названия" />
                    </div>
                    <div class="row">
                        <label><a href="#" class="toggle active" data-class="list_description">Описание</a> | <a href="#" class="toggle" data-class="list_list">Список</a></label>
                        <div class="list_description toggle_data active">
                            <input type="text" class="" placeholder="Описание..." />
                        </div>
                        <div class="list_list toggle_data">
                            <ul>
                                <li>
                                    <div>Молоко</div>
                                    <a href="#" class="delete"></a>
                                </li>
                                <li>
                                    <div>Молоко</div>
                                    <a href="#" class="delete"></a>
                                </li>
                                <li>
                                    <input type="text" placeholder="Новый пункт" value="" />
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <label>Список задач</label>
                        <select name="list" class="select_list">
                            <option value="1">Список «А»</option>
                            <option value="2">Список «B»</option>
                            <option value="3">Список «C»</option>
                        </select>
                        <a href="#" class="new_list"></a>
                    </div>
                    <div class="row clear">
                        <label class="list_priority">Приоритет:</label>    
                        <div class="list_priority radio-buttons">
                            <input type="radio" id="radio1" name="radio"><label for="radio1"><img src="./images/ico/priority/a.png" /></label>
                            <input type="radio" id="radio2" name="radio" checked="checked"><label for="radio2"><img src="./images/ico/priority/b.png" /></label>
                            <input type="radio" id="radio3" name="radio"><label for="radio3"><img src="./images/ico/priority/c.png" /></label>
                            <input type="radio" id="radio4" name="radio"><label for="radio4"><img src="./images/ico/priority/d.png" /></label>
                        </div>
                    </div>
                    <div class="part edited">
                        <div class="part_title">
                            <div class="part_ico"><img src="./images/ico/task_part/calendar.png" /><span></span></div>
                            <div class="part_title_text">
                                <div>Дата выполнения и продолжительность</div>
                                <div class="edited_note">09:30 09.09.2014 — 10:30 09.09.2014, 1 час <a href="#" class="delete"></a></div>
                            </div>
                            <span></span>
                        </div>
                        <div class="part_content">
                            <div class="row clear task_date_range">
                                <div class="part_col_left">
                                    <label>Дата и время начала</label>
                                    <div class="clear">
                                        <input type="text" value="09.09.2014" name="date" class="date datepicker" /> 
                                        <input type="text" value="09:30" name="time" class="time" />
                                    </div>
                                </div>
                                <div class="part_col_right">
                                    <label>Дата и время окончания</label>
                                    <div class="clear">
                                        <input type="text" value="09:30" name="time" class="time" />
                                        <input type="text" value="09.09.2014" name="date" class="date datepicker" /> 
                                    </div>
                                </div>
                            </div>
                            <div class="row clear task_duration">
                                <div class="part_col_left">
                                    <label>Продолжительность:</label>
                                    <div class="clear">
                                        <input type="text" value="1 час" name="date"/> 
                                        <input type="text" value="0 минут" name="time" />
                                    </div>
                                </div>
                                <div class="part_col_right">
                                    <label></label>
                                    <div class="clear">
                                        <input type="checkbox" />
                                        <label for="check" class="checkbox">В течении дня</label>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <label>Часовой пояс: (GMT+02:00) Киев <span>| <a href="#">Изменить</a></span></label>
                            </div>
                        </div>
                    </div>
                    <div class="part">
                        <div class="part_title">
                            <div class="part_ico"><img src="./images/ico/task_part/repeat.png" /><span></span></div>
                            <div class="part_title_text">
                                <div>Повторять задачу</div>
                            </div>
                            <span></span>
                        </div>
                        <div class="part_content">
                            <div class="row clear">
                                <div class="part_col_left">
                                    <label>Повторяется:</label>
                                    <div>
                                        <select>
                                            <option>Каждый день</option>
                                            <option selected>Каждую неделю</option>
                                            <option>Каждый месяц</option>
                                            <option>Каждый год</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="part_col_right">
                                    <label>Повторять с интервалом:</label>
                                    <div>
                                        <select>
                                            <option>1 неделю</option>
                                            <option>2 недели</option>
                                            <option>3 недели</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label>Дни повторения:</label>
                                <div class="clear day_repeat">
                                        <input type="checkbox" />
                                        <label for="check" class="checkbox">Пн</label>
                                        <input type="checkbox" />
                                        <label for="check" class="checkbox">Вт</label>
                                        <input type="checkbox" checked="1"/>
                                        <label for="check" class="checkbox">Ср</label>
                                        <input type="checkbox" checked="1"/>
                                        <label for="check" class="checkbox">Чт</label>
                                        <input type="checkbox" />
                                        <label for="check" class="checkbox">Пт</label>
                                        <input type="checkbox" />
                                        <label for="check" class="checkbox">Сб</label>
                                        <input type="checkbox" />
                                        <label for="check" class="checkbox">Вс</label>
                                </div>
                            </div>
                            <div class="row clear">
                                <div class="part_col_left">
                                    <label>Дата начала:</label>
                                    <div>
                                        <input type="text" value="09.09.2014" name="date" class="datepicker" /> 
                                    </div>
                                </div>
                                <div class="part_col_right"></div>
                            </div>
                            <div class="clear">
                                <div class="part_col_left">
                                    <label>Окончание:</label>
                                    <div>
                                        <select>
                                            <option>Никогда</option>
                                            <option>После повторов</option>
                                            <option>До даты</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="part_col_right"></div>
                            </div>
                        </div>
                    </div>
                    <div class="part edited">
                        <div class="part_title">
                            <div class="part_ico"><img src="./images/ico/task_part/reminder.png" /><span></span></div>
                            <div class="part_title_text">
                                <div>Напомнить</div>
                                <div class="edited_note">Внутри системы, за 30 минут <a href="#" class="delete"></a></div>
                            </div>
                            <span></span>
                        </div>
                        <div class="part_content">
                            <div class="row clear">
                                <div class="part_col_left">
                                    <div>
                                        <select>
                                            <option>Внутри системы</option>
                                            <option>На E-mail</option>
                                            <option>SMS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="part_col_right">
                                    <div class="clear">
                                        <div class="reminder_value">
                                            <input type="text" value="30" class="reminder_value" />
                                        </div>
                                        <div class="reminder_unit">
                                            <select>
                                                <option>секунд</option>
                                                <option selected>минут</option>
                                                <option>часов</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <a href="#" class="add">Добавить напоминание</a>
                            </div>
                            <div class="row">
                                <label>Исключения из правила повторения:</label>
                                <div class="tasks color_cfe6d7">
                                    <div class="task">
                                        <a href="#" class="task_delete"></a>
                                        <a href="#" class="mode_toggle"></a>
                                        <div class="task_title">Заехать на СТО</div>
                                        <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                                        <ul class="task_notifications">
                                            <li>
                                                <span class="ico priority_b"></span>
                                            </li>
                                            <li>
                                                <span class="ico repeat"></span>
                                            </li>
                                            <li>
                                                <span class="ico reminder"></span>
                                            </li>
                                            <li>
                                                <span class="ico place"></span>
                                            </li>
                                            <li>
                                                <span class="ico list"></span>
                                            </li>
                                            <li>
                                                <span class="ico executor"></span>
                                            </li>
                                            <li>
                                                <span class="ico files"></span>
                                            </li>
                                            <li>
                                                <span class="ico comments"></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="task">
                                        <a href="#" class="task_delete"></a>
                                        <a href="#" class="mode_toggle"></a>
                                        <div class="task_title">Продумать варианты скрытия панели Sit2b.</div>
                                        <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                                        <ul class="task_notifications">
                                            <li>
                                                <span class="ico priority_a"></span>
                                            </li>
                                            <li>
                                                <span class="ico repeat"></span>
                                            </li>
                                            <li>
                                                <span class="ico reminder"></span>
                                            </li>
                                            <li>
                                                <span class="ico place"></span>
                                            </li>
                                            <li>
                                                <span class="ico list"></span>
                                            </li>
                                            <li>
                                                <span class="ico executor"></span>
                                            </li>
                                            <li>
                                                <span class="ico files active"></span>
                                            </li>
                                            <li>
                                                <span class="ico comments active"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="checkbox_toggle clear mb10">
                                <input type="checkbox" checked="1"/>
                                <label for="check" class="checkbox">Показывать выполненные</label>
                            </div>
                            <div class="row">
                                <div class="tasks color_cfe6d7">
                                    <div class="task done">
                                        <a href="#" class="task_delete"></a>
                                        <a href="#" class="mode_toggle"></a>
                                        <div class="task_title">Заехать на СТО</div>
                                        <div class="task_date"><span>09:00-12:00, сегодня</span></div>
                                        <ul class="task_notifications">
                                            <li>
                                                <span class="ico priority_b"></span>
                                            </li>
                                            <li>
                                                <span class="ico repeat"></span>
                                            </li>
                                            <li>
                                                <span class="ico reminder"></span>
                                            </li>
                                            <li>
                                                <span class="ico place"></span>
                                            </li>
                                            <li>
                                                <span class="ico list"></span>
                                            </li>
                                            <li>
                                                <span class="ico executor"></span>
                                            </li>
                                            <li>
                                                <span class="ico files"></span>
                                            </li>
                                            <li>
                                                <span class="ico comments"></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="part edited">
                        <div class="part_title">
                            <div class="part_ico"><img src="./images/ico/task_part/place.png" /><span></span></div>
                            <div class="part_title_text">
                                <div>Местоположение</div>
                                <div class="edited_note">г.Чернигов, Черниговская обл., Украина <a href="#" class="delete"></a></div>
                            </div>
                            <span></span>
                        </div>
                        <div class="part_content">
                            <div>
                                <input type="text" value="г.Чернигов, Черниговская обл., Украина" />
                            </div>
                        </div>
                    </div>
                    <div class="part edited">
                        <div class="part_title">
                            <div class="part_ico"><img src="./images/ico/task_part/executor.png" /><span></span></div>
                            <div class="part_title_text">
                                <div>Исполнитель</div>
                                <div class="edited_note">Зинаида Короблева <a href="#" class="delete"></a></div>
                            </div>
                            <span></span>
                        </div>
                        <div class="part_content">
                            <div>
                                <select name="executor" class="task_executor" >
                                    <option>Зинаида Кораблева (zinaida@gmail.com)</option>
                                    <option>Зинаида Кораблева (zinaida@gmail.com)</option>
                                    <option>Зинаида Кораблева (zinaida@gmail.com)</option>
                                    <option>Зинаида Кораблева (zinaida@gmail.com)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="part edited">
                        <div class="part_title">
                            <div class="part_ico"><img src="./images/ico/task_part/files.png" /><span></span></div>
                            <div class="part_title_text">
                                <div>Файлы</div>
                                <div class="edited_note">3 файла <a href="#" class="delete"></a></div>
                            </div>
                            <span></span>
                        </div>
                        <div class="part_content">
                            <div>
                                <ul class="uploaded_files">
                                    <li>
                                        <div>
                                            <span>Picture.jpeg</span>
                                            <a href="#" class="delete"></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <span>Условия договора.doc</span>
                                            <a href="#" class="delete"></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <span>Фотка.jpeg</span>
                                            <a href="#" class="delete"></a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="upload_area">
                                    Перетащите файлы сюда или <a href="#">загрузите их</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="part">
                        <div class="part_title">
                            <div class="part_ico"><img src="./images/ico/task_part/comments.png" /><span></span></div>
                            <div class="part_title_text">
                                <div>Комментарии</div>
                            </div>
                            <span></span>
                        </div>
                        <div class="part_content">
                            <div class="task_comments">
                                <div class="task_comment">
                                    <div class="task_comment_ava">
                                        <img src="./images/ava/male.png" />
                                    </div>
                                    <div class="task_comment_text">
                                        <div class="task_comment_author">Александр Иванов<span>, Сегодня в 12:12</span></div>
                                        <div class="task_comment_content">Посмотри договор с компанией Сит-Девелоперс</div>
                                    </div>
                                </div>
                                <div class="task_comment">
                                    <div class="task_comment_ava">
                                        <img src="./images/ava/male.png" />
                                    </div>
                                    <div class="task_comment_text">
                                        <div class="task_comment_author">Роман Хабибулин<span>, 02 сентября 2014 в 12:12</span></div>
                                        <div class="task_comment_content">
                                            Голос персонажа, как справедливо считает
                                            И.Гальперин, начинает строфоид. Кульминация
                                            вызывает диалектический характер. Ударение
                                            аннигилирует экзистенциальный реформаторский
                                            пафос, потому что в стихах и в прозе
                                        </div>
                                    </div>
                                </div>
                                <div class="task_comment">
                                    <div class="task_comment_ava">
                                        <img src="./images/ava/male.png" />
                                    </div>
                                    <div class="task_comment_text">
                                        <div class="task_comment_author">Александр Иванов<span>, Сегодня в 12:12</span></div>
                                        <div class="task_comment_content">Посмотри договор с компанией Сит-Девелоперс</div>
                                    </div>
                                </div>
                                <div class="task_comment">
                                    <div class="task_comment_ava">
                                        <img src="./images/ava/male.png" />
                                    </div>
                                    <div class="task_comment_text">
                                        <div class="task_comment_author">Роман Хабибулин<span>, 02 сентября 2014 в 12:12</span></div>
                                        <div class="task_comment_content">
                                            Голос персонажа, как справедливо считает
                                            И.Гальперин, начинает строфоид. Кульминация
                                            вызывает диалектический характер. Ударение
                                            аннигилирует экзистенциальный реформаторский
                                            пафос, потому что в стихах и в прозе
                                        </div>
                                    </div>
                                </div>
                                <div class="task_comment">
                                    <div class="task_comment_ava">
                                        <img src="./images/ava/male.png" />
                                    </div>
                                    <div class="task_comment_text">
                                        <textarea placeholder="Ваш комментарий..." ></textarea>
                                    </div>
                                </div>
                                <div class="task_comment_submit_wrapper">
                                    <input type="submit" value="Отправить" />
                                    <a href="#" class="clear">Очистить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="part">
                        <div class="part_title">
                            <div class="part_ico"><img src="./images/ico/task_part/log.png" /><span></span></div>
                            <div class="part_title_text">
                                <div>Лог изменений</div>
                            </div>
                            <span></span>
                        </div>
                        <div class="part_content">
                            <div class="task_log">
                                <div class="log_date"><span>04 сентября 2014</span></div>
                                <div class="log_info_wrapper">
                                    <div class="log_time">08:42</div> 
                                    <div class="log_info"><span>Александр Иванов</span> изменил тему задачи.</div>
                                </div>
                                <div class="log_info_wrapper">
                                    <div class="log_time">08:42</div> 
                                    <div class="log_info"><span>Александр Иванов</span> добавил комментарий..</div>
                                </div>
                            </div>
                            <div class="task_log">
                                <div class="log_date"><span>04 сентября 2014</span></div>
                                <div class="log_info_wrapper">
                                    <div class="log_time">08:42</div> 
                                    <div class="log_info"><span>Александр Иванов</span> добавил файлы <span>Picture.jpeg,
Условия договора.doc, Фотка.jpeg</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="popup_footer">
                    <a href="#" class="button cancel">Отмена</a>
                    <input type="submit" value="Готово" />
                </div>
            </form>
        </div>
        <header>
            <a href="./" class="logo">
                <img src="./images/logo.png" />
            </a>
            <ul class="nav">
                <li class="active ico" >
                    <a href="#" class="calendar"></a>
                </li>
                <li class="active ico" >
                    <a href="#" class="list"></a>
                </li>
            </ul>
            <ul class="nav controls">
                <li>
                    <a href="#" class="button add create_task"><span class="ico"></span> Создать задачу</a>
                </li>
                <li class="ico" >
                    <a href="#" class="chat">
                        <span>8</span>
                    </a>
                </li>
                <li class="ico" >
                    <a href="#" class="notification">
                        <span>38</span>
                    </a>
                </li>
                <li class="ico" >
                    <a href="#" class="settings"></a>
                </li>
                <li class="ico" >
                    <a href="#" class="logout"></a>
                </li>
            </ul>
        </header>
        <div class="side_left">
            <div class="user_info">
                <div class="name">Александр Иванов</div>
                <div class="avatar">
                    <img src="./images/ava/male.png" />
                </div>
            </div>
            <ul class="nav">
                <li class="active">
                    <a href="#">Франклинайзер</a>
                </li>
                <li>
                    <a href="#">CRM</a>
                </li>
                <li>
                    <a href="#">Органайзер</a>
                </li>
            </ul>
        </div>
        <div class="side_right">
            <div class="body">
                <div class="two_colums">
                    <div class="column"><?php include './includes/calendar.php'; ?></div>
                    <div class="column column_separator"></div>
                    <div class="column"><?php include './includes/lists.php'; ?></div>
                </div>
            </div>
        </div>
        <div class="footer"></div>
    </body>
</html>
