<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>sit2B</title>
        <script type="text/javascript" src="./js/jquery-1.11.0.js"></script>
<!--        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>-->
        <link rel="stylesheet" href="./css/jquery-ui.css" />
        <script src="./js/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="./css/reset.css" />
        <link rel="stylesheet" href="./css/style.css?v=1" />
        <script type="text/javascript" src="./js/script.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="overlay"></div>
        <div class="popup auth">
            <div class="popup_header">
                <a href="#" class="close"></a>
                <a href="./" class="logo"><img src="./images/logo_dark.png"></a>
                <div class="subtitle">Франклинайзер</div>
                <div class="title">Вход на сайт</div>
            </div>
            <div class="popup_content">
                <form action="#">
                    <div class="row">
                        <label>Email</label>
                        <input type="text" value="" placeholder="" />
                    </div>
                    <div class="row10">
                        <label>Пароль</label>
                        <input type="password" value="" placeholder="" />
                    </div>
                    <div class="row10">
                        <a href="#" class="pwd_restore">Забыли пароль?</a>
                    </div>
                    <div class="row10">
                        <input type="submit" value="Войти" />
                    </div>
                    <div class="row10 tac">Нет аккаунта? <a href="#">Зарегистрируйтесь</a></div>
                </form>
            </div>
            <div class="popup_footer">
                <div>Вход через:</div>
                <div>
                    <a href="#" class="social google"></a>
                </div>
                <div class="clear">
                    <a href="#" class="social fb"></a>
                    <a href="#" class="social tw"></a>
                    <a href="#" class="social vk"></a>
                </div>
            </div>
        </div>
        <div class="popup auth" style="left: 300px;" >
            <div class="popup_header">
                <a href="#" class="close"></a>
                <a href="./" class="logo"><img src="./images/logo_dark.png"></a>
                <div class="subtitle">Франклинайзер</div>
                <div class="title">Регистрация</div>
            </div>
            <div class="popup_content">
                <form action="#">
                    <div class="row">
                        <label>Email</label>
                        <input type="text" value="" placeholder="" />
                    </div>
                    <div class="row10">
                        <label>Пароль</label>
                        <input type="password" value="" placeholder="" />
                    </div>
                    <div class="row10">
                        От 6 до 32 символов
                    </div>
                    <div class="row10">
                        <input type="submit" value="Зарегистрироваться" />
                    </div>
                    <div class="row10 tac">
                        <div>Уже есть аккаунт?</div>
                        <div>Воспользуйтесь <a href="#">формой входа</a></div>
                    </div>
                </form>
            </div>
            <div class="popup_footer">
                <div>Регистрация через:</div>
                <div>
                    <a href="#" class="social google"></a>
                </div>
                <div class="clear">
                    <a href="#" class="social fb"></a>
                    <a href="#" class="social tw"></a>
                    <a href="#" class="social vk"></a>
                </div>
            </div>
        </div>
        <div class="popup auth" style="left: 600px;" >
            <div class="popup_header">
                <a href="#" class="close"></a>
                <a href="./" class="logo"><img src="./images/logo_dark.png"></a>
                <div class="subtitle">Франклинайзер</div>
                <div class="title">Восстановление пароль</div>
            </div>
            <div class="popup_content">
                <form action="#">
                    <div class="hint">
                        Введите ваш email, мы отправим на него инструкции по восстановлению пароля.
                    </div>
                    <div class="row">
                        <label>Email</label>
                        <input type="text" value="" placeholder="" />
                    </div>
                    <div class="row10">
                        <input type="submit" value="Отправить" />
                    </div>
                    <div class="row10 tac">
                        <a href="#">Я вспомнил пароль</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="popup auth" style="left: 900px;">
            <div class="popup_header">
                <a href="#" class="close"></a>
                <a href="./" class="logo"><img src="./images/logo_dark.png"></a>
                <div class="subtitle">Франклинайзер</div>
                <div class="title">Вход на сайт</div>
            </div>
            <div class="popup_content">
                <form action="#">
                    <div class="row">
                        <label>Email</label>
                        <input type="text" value="" placeholder="" class="error" />
                        <div class="error"><span>Поле должно быть заполнено</span></div>
                    </div>
                    <div class="row10">
                        <label>Пароль</label>
                        <input type="password" value="" placeholder="" />
                    </div>
                    <div class="row10">
                        <a href="#" class="pwd_restore">Забыли пароль?</a>
                    </div>
                    <div class="row10">
                        <input type="submit" value="Войти" />
                    </div>
                    <div class="row10 tac">Нет аккаунта? <a href="#">Зарегистрируйтесь</a></div>
                </form>
            </div>
            <div class="popup_footer">
                <div>Вход через:</div>
                <div>
                    <a href="#" class="social google"></a>
                </div>
                <div class="clear">
                    <a href="#" class="social fb"></a>
                    <a href="#" class="social tw"></a>
                    <a href="#" class="social vk"></a>
                </div>
            </div>
        </div>
    </body>
</html>
