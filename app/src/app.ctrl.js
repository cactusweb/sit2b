/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 02.03.15
 */

angular.module('FRZ')
  .controller('AppController', ['$scope', '$sessionStorage', 'AUTH_EVENTS', 'USER_ROLES', 'AuthService', 'LocalData', 'UserProfile', '$timeout', '$interval', 'toastr', '$injector',
    function ($scope, $sessionStorage, AUTH_EVENTS, USER_ROLES, AuthService, LocalData, UserProfile, $timeout, $interval, toastr, $injector) {
      var LogModel = $injector.get('LogModel');
      var TaskModel = $injector.get('TaskModel');
      var self = this;

      $scope.currentUser = null;
      $scope.userRoles = USER_ROLES;
      $scope.isAuthorized = AuthService.isAuthorized;

      $scope.isLoginPage = false; // @TODO remove later

      $scope.setCurrentUser = function (user) {
        $scope.user = user;
      };

      this.startPolling = function () {
        var Polling = $injector.get('Polling');

        $interval(function () {
          Polling.makeRequest();
        }, 30000); // @TODO Make delay value a variable
      };

      $scope.$on(AUTH_EVENTS.loginSuccess, function (event, user) {
        toastr.success('Добро пожаловать!', 'Вход выполнен');

        LocalData.initLocalData();
        UserProfile.putUserToSession(user);
        UserProfile.getUserPreferences();
        $scope.$root.user = UserProfile.getUserFromSession();

        if ($sessionStorage.originRedirect !== undefined && $sessionStorage.originRedirect !== null) {
          var originRedirect = angular.copy($sessionStorage.originRedirect);
          delete $sessionStorage.originRedirect;
          $scope.$state.go(originRedirect.toState, originRedirect.toParams);
        } else {
          self.startPolling();

          $scope.$state.go('dashboard.start');
        }
      });

      $scope.$on(AUTH_EVENTS.notAuthenticated, function (event) {
        toastr.warning('Требуется аутентификация!');
        console.log(event.name);
      });

      $scope.$on(AUTH_EVENTS.notAuthorized, function (event) {
        //console.log(event.name);
      });

      $scope.$on('$stateChangeError', function (event) {
        console.log(event);
      });

      $scope.$on(AUTH_EVENTS.signupSuccess, function (event) {
        toastr.success('Добро пожаловать!', 'Регистрация успешна');
        self.startPolling();
        console.log(event.name);
      });

      $scope.$on(AUTH_EVENTS.signupFailed, function (event) {
        toastr.error('Ошибка регистрации');
        console.log(event.name);
      });

      $scope.$on(AUTH_EVENTS.sessionRecovered, function (event) {
        toastr.error('Сессия восстановлена');
        self.startPolling();
        console.log(event.name);
      });

    }]);
