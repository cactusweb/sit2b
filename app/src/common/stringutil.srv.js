/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 23.04.15
 */

angular.module('FRZ')
  .service('StringUtils',
  ['$injector', '$q',
    function ($injector, $q) {

      this.trimLeft = function (str, charlist) {
        if (charlist === undefined)
          charlist = "\s";

        return str.replace(new RegExp("^[" + charlist + "]+"), "");
      };

      this.trimRight = function (str, charlist) {
        if (charlist === undefined)
          charlist = "\s";

        return str.replace(new RegExp("[" + charlist + "]+$"), "");
      };

      this.trim = function (str, charlist) {
        return str.trimLeft(charlist).trimRight(charlist);
      };

    }]);


