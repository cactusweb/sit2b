/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 28.11.14
 */

angular.module('FRZ.profile')
  .factory('UserModel', ['$resource', 'appConfig', function ($resource, appConfig) {
    return $resource(appConfig.apiUrl + '/user/:id', { // default parameters
      id: '@id',
      email: null
    }, { // actions
      'newPassword': {
        method: 'POST',
        responseType: 'JSON',
        url: appConfig.apiUrl + '/user/:id/new-password',
        params: {}
      },
      'update': {
        method: 'PUT'
      },
      'findByEmail': {
        method: 'GET',
        params: {email: ':email'}
      }
    });
  }]);