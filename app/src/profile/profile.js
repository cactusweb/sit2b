/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 24.03.15
 */

angular.module('FRZ.profile', [
  'ui.router',
  'ui.bootstrap',
  'ngSanitize',
  'ngResource'
])
  .config([
    '$stateProvider', '$urlRouterProvider', 'USER_ROLES',
    function ($stateProvider, $urlRouterProvider, USER_ROLES) {
      "use strict";

      $stateProvider
        .state('dashboard.start.profile', {
          data: {
            authorizedRoles: [USER_ROLES.user] // will inherited by child states
          },
          url: '/profile',
          onEnter: ['$stateParams', '$state', '$modal', '$injector', '$localStorage', 'LocalData',
            function ($stateParams, $state, $modal, $injector, $localStorage, LocalData) {
              var $scope = $injector.get('$rootScope'),
                $q = $injector.get('$q'),
                modal;

              modal = $modal.open({
                backdrop: 'static',
                resolve: {},
                templateUrl: "src/profile/partials/profile.html",
                controller: 'ProfileCtrl'
              });

              modal.result.then(function (reason) {
                $scope.$state.go('^');
              }, function (reason) {
                if (reason == 'not-found'){
                  $scope.$state.go('not-found', {location: false});
                } else {
                  $scope.$state.go('^');
                }
              });

              $scope.modals = [];
              $scope.modals.push(modal);
            }
          ],
          onExit: ['$injector', function ($injector) {
            var $scope = $injector.get('$rootScope'),
              modal = $scope.modals.pop();
            modal.dismiss('cancel');
          }]
        })
    }]);
