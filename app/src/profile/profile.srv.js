/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 06.02.15
 */

angular.module('FRZ.profile')
  .service('UserProfile', [
    '$sessionStorage', '$localStorage', '$injector',
    function ($sessionStorage, $localStorage, $injector) {

      var self = this;

      'use strict';

      /**
       *
       * @param user
       */
      this.putUserToSession = function (user) {
        if (user.preferences) {
          user.preferences = JSON.parse(user.preferences);
          console.log(user.preferences);
        } else {
          user.preferences = {};
        }

        $sessionStorage.user = user;
      };

      /**
       *
       * @returns {*|$sessionStorage.user}
       */
      this.getUserFromSession = function () {
        return $sessionStorage.user;
      };

      /**
       * Get all user preferences.
       * @returns {dumpExecutors.preferences|*|widget.preferences}
       */
      this.getUserPreferences = function () {
        if (!$sessionStorage.user.preferences || !$sessionStorage.user.preferences.lists) {
          $sessionStorage.user.preferences = {};
          $sessionStorage.user.preferences.showLists = true;
          $sessionStorage.user.preferences.showCalendar = true;
          $sessionStorage.user.preferences.lists = {};
          angular.forEach($localStorage.sit2b.listsAndTasks, function(list){
            $sessionStorage.user.preferences.lists[list.id] = self.getDefaultProperties(list.id);
          });
        }

        return $sessionStorage.user.preferences;
      };

      /**
       * Get property of certain list.
       * @param list_id
       */
      this.getListProperties = function (list_id) {
        var prefs = this.getUserPreferences();
        var UserModel = $injector.get('UserModel');

        if (!prefs.lists[list_id]) {
          prefs.lists[list_id] = this.getDefaultProperties(list_id);
        }
        return prefs.lists[list_id];
      };

      /**
       * Default set of properties.
       * @returns {{id: *, title: *, own_title: *, order: string, def_duration: string, color: string, show_done: boolean, show_panned: boolean, show_not_planned: boolean, auto_done: boolean}}
       */
      this.getDefaultProperties = function (list_id) {
        var title, own_title;

        if (list_id) {// && $localStorage.sit2b.listsAndTasks[list_id]) {
          title = $localStorage.sit2b.listsAndTasks[list_id].title;
        } else {
          title = 'Новый список'; // + (Object.keys($localStorage.sit2b.listsAndTasks).length);
        }

        own_title = title;
        return {
          id: list_id,
          title: title,
          own_title: own_title,
          order: 'created',
          def_duration: 30,
          filter_executor: 0,
          color: 'bcfad1',
          show_done: 1,
          show_planned: 0,
          show_not_planned: 1,
          auto_done: 0
        };
      };

      /**
       * Reset any user's changes fro list.
       * @param list_id
       * @returns {*}
       */
      this.revertToDefaultProperties = function (list_id) {
        var prefs = this.getUserPreferences();

        prefs.lists[list_id] = this.getDefaultProperties(list_id);
        this.saveToServer();

        return this;
      };

      /**
       * Used after creating new list.
       * @param list_id
       * @param properties
       */
      this.setListProperties = function (list_id, properties) {
        var prefs = this.getUserPreferences();

        prefs.lists[list_id] = properties;
        this.saveToServer();

        return this;
      };

      /**
       *
       * @param list_id
       */
      this.deleteListProperties = function (list_id) {
        var prefs = this.getUserPreferences();
        delete prefs.lists[list_id];
        this.saveToServer();

        return this;
      };

      /**
       * Save user info to server' DB.
       */
      this.saveToServer = function () {
        var UserModel = $injector.get('UserModel');
        UserModel.update({id: $sessionStorage.user.id}, $sessionStorage.user,
          function (user) {
            // success
          }, function (result) {
            console.log('Пользователь НЕ записан ', result.data);
            alert(result.data.status + ': ' + result.data.message);
          });
      };

    }]);