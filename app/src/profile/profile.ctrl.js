/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 24.03.15
 */

angular.module('FRZ.profile')
  .controller('ProfileCtrl', ['$scope', '$injector', '$upload', 'appConfig', 'UserProfile', function ($scope, $injector, $upload, appConfig, UserProfile) {

    var appConfig = $injector.get('appConfig');

    $scope.form = {};
    $scope.form.tab = 'main';
    $scope.form.user = angular.copy($scope.$root.user);
    $scope.apiHost = appConfig.apiHost;

    //$scope.$watch(function(){
    //  return $scope.$root.user;
    //}, function(n){
    //  $scope.form.user = angular.copy(n);
    //});

    //value = DB.info_type.id
    //label = DB.info_type.name
    $scope.form.infoTypes = [
      {
        'value':1,
        'label':'Телефон',
        'position':0
      },
      {
        'value':2,
        'label':'E-mail',
        'position':0
      }
    ];

    $scope.form.contacts = [
      {
        value: 'phone',
        label: 'Телефон',
        position: 0
      },
      {
        value: 'email',
        label: 'Email',
        position: 0
      }
    ];

    //TODO get inputs type email/text
    $scope.form.getContactsInputType = function(type_id){
      //if(parseInt(type_id) === 2){
      //  return 'email';
      //}
      return 'text';
    };

    //TODO validate email/text
    $scope.form.validateContactsInput = function(event){
      console.log($(event.target).attr('type'));
    };

    $scope.form.updateUser = function () {
      var UserModel = $injector.get('UserModel'),
        LocalData = $injector.get('LocalData');

      UserModel.update({
        id: $scope.$root.user.id
      }, $scope.form.user, function (newUser) {
        newUser.access_token = UserProfile.getUserFromSession().access_token;
        UserProfile.putUserToSession(newUser);
        $scope.$root.user = newUser;
        $scope.$close(true);
      }, function (result) {
        alert(result.data.status + ': ' + result.data.message);
      });

      return false;

    };

    $scope.$watch('form.photo', function () {
      if($scope.form.photo){
        var file = $scope.form.photo;
        $scope.upload = $upload.upload({
          url: appConfig.apiUrl + '/user/'+$scope.$root.user.id+'/photo',
          method: 'POST',
          headers: {'Content-Type': file.type != '' ? file.type : 'application/octet-stream'}, // only for html5
          withCredentials: false,
          file: file
        }).progress(function (evt) {
          $scope.form.filePreloader = true;
        }).success(function (data, status, headers, config) {
          $scope.form.user.photo = data;
          $scope.$root.user.photo = data;
          console.log(data);
        }).error(function (data) {
          $scope.form.filePreloader = false;
          alert(data.status + ': ' + data.message);
        });

      }
    });

    $scope.phone = 'phone';

    $scope.form.addContact = function (){
      $scope.form.user.userInfos.push({
        'info_type_id': $scope.form.infoTypes[1].value,
        'value': ''
      });
    };

    $scope.form.deleteContact = function (key){
      $scope.form.user.userInfos.splice(key, 1);
    };

    /**
     * Close modal.
     */
    $scope.form.close = function () {
      $scope.$close('closed');
    };

    $scope.form.changePass = function () {
      var parent = $('div.popup.profile');
      var ChangePassModal = $injector.get('ChangePassModal');
      var result = ChangePassModal.open({parent: parent}); // возвращает промис

      result.then(function (data){
        console.log('ok');
      }, function (reason){
        console.log('cancel');
      });

    };

  }])

  .service('ChangePassModal', ['$injector', function ($injector) {
    /**
     *
     * @param question
     * @param parent
     * @returns {*}
     */
    this.open = function (options) {
      if (!options) {
        options = {
          parent: null
        }
      }

      var $modal = $injector.get('$modal'),
        modal = $modal.open({
          size: 'sm',
          backdrop: 'static',
          templateUrl: "src/profile/partials/changepass.html",
          controller: ['$scope', function ($scope) {

            $scope.oldPwd = null;
            $scope.newPwd = null;
            $scope.newPwd2 = null;

            $scope.ok = function (form) {
              $scope.error_repwd = false;

              if($scope.newPwd != $scope.newPwd2){
                form.$invalid = true;
                $scope.error_repwd = true;
                return false;
              }

              if(form.$invalid){
                return false;
              }

              var data = {
                oldPwd: $scope.oldPwd,
                newPwd: $scope.newPwd
              };

              var UserModel = $injector.get('UserModel');

              UserModel.newPassword({
                id: $scope.$root.user.id
              }, data, function (result) {
                //update localData

                //console.log(result);
                //return false;

                $scope.$close(true);
              }, function (result){
                $scope.error = result.data;
                //alert(result.data.status + ': ' + result.data.message);
                return false;
              });

            };

            $scope.cancel = function () {
              $scope.$dismiss(false);
            };
          }]
        });

      modal.opened.then(function () {
        var $timeout = $injector.get('$timeout');

        if (options.parent) {
          // adjust position
          $timeout(function () {
            var parent = options.parent,
              myself = $('div.popup.changepass'),
              parentPos = parent.position(),
              offset = {
                top: 0,
                left: parentPos.left + ((parent.outerWidth() - myself.outerWidth()) / 2)
              };

            myself.offset({top: offset.top, left: offset.left});
          });
        }
      });

      return modal.result;
    };
  }]);
