/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 20.11.14
 */
"use strict";

angular.module('FRZ.auth', [
  'ui.router',
  'ngResource'
])

  .constant('AUTH_EVENTS', {
    loginFree: 'auth-login-free',
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    sessionRecovered: 'auth-session-recovered',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized',
    signupSuccess: 'auth-signup-success',
    signupFailed: 'auth-signup-failed'
  })

  .constant('USER_ROLES', {
    all: '*',
    admin: 'admin',
    user: 'user',
    guest: 'guest'
  })

  .config(['$stateProvider', '$urlRouterProvider', 'USER_ROLES', 'AUTH_EVENTS',
    function ($stateProvider, $urlRouterProvider, USER_ROLES, AUTH_EVENTS) {

      $stateProvider
        .state('auth', {
          data: {
            authorizedRoles: [USER_ROLES.all] // will inherited by child states
          },
          url: '/auth',
          abstract: true,
          template: '<ui-view/>'
        })

        .state('auth.login', {
          url: '/login',
          templateUrl: 'src/auth/partials/login.html'
        })

        .state('auth.logout', {
          data: {
            authorizedRoles: [USER_ROLES.user, USER_ROLES.admin] // only for logged in users
          },
          url: '/logout',
          controller: ['$scope', '$rootScope', '$sessionStorage', 'AuthService', function ($scope, $rootScope, $sessionStorage, AuthService) {
            AuthService.logout();

            delete $scope.$root.user;
            delete $sessionStorage.user;

            $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
            $scope.$state.go('home');
          }]
        })

        .state('auth.signup', {
          url: '/signup',
          templateUrl: 'src/auth/partials/signup.html'
        })

        .state('auth.recovery', {
          url: '/recovery?email&code',
          templateUrl: 'src/auth/partials/recovery.html'
        })

      /**
       * The Google's OAuth code comes here via .htaccess redirection
       * from sit2b.com?type=google&code=google_code
       */
        .state('auth.social', {
          url: '/social/:type?code',
          controller: 'SocialCtrl'
        });
    }
  ]);