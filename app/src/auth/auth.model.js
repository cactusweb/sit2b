/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 27.11.14
 */

angular.module('FRZ.auth')
  .factory('AuthModel', ['$resource', 'appConfig', function ($resource, appConfig) {
    return $resource(appConfig.apiUrl + '/:ctrl/:act', { // default parameters
      ctrl: 'auth',
      act: '@act'
    }, { // actions
      'signup': {
        method: 'POST',
        responseType: 'JSON',
        params: {
          act: 'signup'
        }
      },
      'login': {
        method: 'POST',
        responseType: 'JSON',
        params: {
          act: 'login'
        }
      },
      'checkEmail': {
        method: 'POST',
        responseType: 'JSON',
        params: {
          act: 'email-check'
        }
      },
      'recovery': {
        method: 'POST',
        responseType: 'JSON',
        params: {
          act: 'recovery'
        }
      },
      'sessionRecovery': {
        method: 'POST',
        responseType: 'JSON',
        params: {
          act: 'session-recovery'
        }
      },
      'social': {
        method: 'POST',
        responseType: 'JSON',
        params: {
          act: 'social'
        }
      }
    });
  }]);