/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 21.11.14
 */

angular.module('FRZ.auth')
  .controller('LoginCtrl', [
    '$scope', '$rootScope', '$sessionStorage', 'AuthService', 'AUTH_EVENTS', 'UserProfile',
    function ($scope, $rootScope, $sessionStorage, AuthService, AUTH_EVENTS, UserProfile) {
      $scope.credentials = {
        email: '',
        password: ''
      };
      $scope.isLoginPage = true;
      $scope.error = null;

      /**
       * Do login with credentials.
       */
      $scope.login = function (credentials) {
        AuthService.login(credentials).then(function (user) {
          $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, user);
          $scope.error = null;
        }, function (response) {
          $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
          $scope.error = response.data;
        });
      };
    }
  ]);