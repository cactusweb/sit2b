/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 01.12.14
 */

angular.module('FRZ.auth')
  .controller('RecoveryCtrl', [
    '$scope', '$sessionStorage', 'AuthModel', 'UserModel',
    function ($scope, $sessionStorage, AuthModel, UserModel) {
      $scope.error = null;
      $scope.emailSent = false;
      $scope.credentials = {
        email: $scope.$stateParams.email,
        code: $scope.$stateParams.code
      };

      /**
       * Step 1 - user send email for recovery code.
       * @param {string} email
       */
      $scope.sendEmail = function (email) {
        AuthModel.recovery({}, {
            email: email
          },
          // success
          function (data) {
            $scope.error = null;
            $scope.emailSent = true;
          },
          // error
          function (e) {
            $scope.emailSent = false;
            $scope.error = e.data;
          });
      };

      /**
       * Step 2 - user send email and code together.
       * @param {object} guest {email, code}
       */
      $scope.sendCode = function (guest) {
        AuthModel.recovery({}, guest,
          // success
          function (auth) {
            $scope.error = null;
            $sessionStorage.user = auth;

            UserModel.newPassword({
                id: auth.id
              }, {
                password: 'Qwe1@3'
              },
              function (user) {
                console.info('@TODO Profive a new password input here.');
                console.dir(user);

                UserModel.get({
                    id: auth.id
                  },
                  function (user) {
                    $sessionStorage.user = user;
                  },
                  function (e) {
                    console.log('UserModel Error');
                    console.dir(e);
                  }
                );

              },
              function (e) {
                console.log('UserModel Error');
                console.dir(e);
              });
          },
          // error
          function (e) {
            console.log('AuthModel Error');
            console.dir(e);
            $scope.error = e.data;
          });
      };

      $scope.submit = function () {
        if ($scope.credentials.email && !$scope.credentials.code) {
          $scope.sendEmail($scope.credentials.email);
        } else if ($scope.credentials.email && $scope.credentials.code) {
          $scope.sendCode($scope.credentials);
        }
      }

    }
  ]);