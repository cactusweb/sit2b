/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 02.03.15
 */

angular.module('FRZ.auth')
  .factory('AuthService', ['$q', '$injector', 'AuthModel', 'SessionService', 'USER_ROLES',
    function ($q, $injector, AuthModel, SessionService, USER_ROLES) {
      var authService = {};

      /**
       *
       * @param credentials
       * @returns {*}`
       */
      authService.signup = function (credentials) {
        var deferred = $q.defer();

        AuthModel.signup(credentials,
          function (user) {
            SessionService.create(user.access_token, user.id, user.group);
            deferred.resolve(user);
          },
          function (data) {
            deferred.reject(data);
          });

        return deferred.promise;
      };

      /**
       *
       * @param credentials
       * @returns {*}
       */
      authService.login = function (credentials) {
        var deferred = $q.defer();

        AuthModel.login(credentials,
          function (user) {
            SessionService.create(user.access_token, user.id, user.group);
            deferred.resolve(user);
          },
          function (data) {
            deferred.reject(data);
          });

        return deferred.promise;
      };

      /**
       * Login or signup via Google credentials
       * @param code
       * @returns {*}
       */
      authService.loginViaGoogle = function (code) {
        var deferred = $q.defer();

        AuthModel.social({
            type: 'google',
            code: code
          },
          function (user) {
            SessionService.create(user.access_token, user.id, user.group);
            deferred.resolve(user);
          },
          function (data) {
            deferred.reject(data);
          });

        return deferred.promise;
      };

      /**
       * Session restored based on the old access token.
       *
       * @param key
       * @returns {*}
       */
      authService.sessionRecovery = function () {
        var deferred = $q.defer();

        AuthModel.sessionRecovery({},
          function (res) {
            SessionService.update(res.access_token);
            deferred.resolve(res);
          },
          function (data) {
            deferred.reject(data);
          });

        return deferred.promise;
      };

      /**
       *
       * @returns {boolean}
       */
      authService.isAuthenticated = function () {
        return !!SessionService.userId;
      };

      /**
       *
       * @param authorizedRoles
       * @returns {boolean}
       */
      authService.isAuthorized = function (authorizedRoles) {
        if (!angular.isArray(authorizedRoles)) {
          authorizedRoles = [authorizedRoles];
        }

        var allUsersAllowed = authorizedRoles.indexOf(USER_ROLES.all) !== -1;

        //debugger;
        if (allUsersAllowed) {
          return true;
        } else {
          return authService.isAuthenticated() && authorizedRoles.indexOf(SessionService.userRole) !== -1;
        }
      };

      /**
       * Logout
       */
      authService.logout = function () {
        SessionService.destroy();
      };

      /**
       *
       * @param authorizedRoles
       * @returns {boolean}
       */
      authService.allUsersAllowed = function (authorizedRoles) {
        return authorizedRoles.indexOf(USER_ROLES.all) !== -1;
      };

      return authService;
    }]);