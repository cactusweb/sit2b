/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 01.12.14
 */

angular.module('FRZ.auth')
  .controller('SocialCtrl', [
    '$scope', '$rootScope', 'appConfig', '$sessionStorage', '$localStorage', '$location', 'AuthService', 'AUTH_EVENTS', 'UserProfile',
    function ($scope, $rootScope, appConfig, $sessionStorage, $localStorage, $location, AuthService, AUTH_EVENTS, UserProfile) {
      /**
       * On-click function for button "Google"
       * Redirect to Google OAuth 2 service.
       */
      $scope.useGoogleApi = function () {
        var url = 'https://accounts.google.com/o/oauth2/auth',
          redirect_uri = appConfig.frontUrl + '/?type=google',
          response_type = 'code',
          client_id = appConfig.googleOAuthClientId,  //'744059574569-6q7vobmamrc70qku16e8g79c32gmtppg.apps.googleusercontent.com',
          scope = 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile';

        url += '?redirect_uri=' + redirect_uri;
        url += '&response_type=' + response_type;
        url += '&client_id=' + client_id;
        url += '&scope=' + scope;

        window.location.href = url;
      };

      /**
       * Login or signup via social networks credentials
       */
      if ($scope.$stateParams.type == 'google' && $scope.$stateParams.code) {
        var code = $scope.$stateParams.code.replace('_s_', '/').replace('_d_', '.');

        AuthService.loginViaGoogle(code).then(function (user) {
          $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, user);
          $scope.error = null;
        }, function () {
          $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        });
      } // else if facebook ...
    }
  ]);