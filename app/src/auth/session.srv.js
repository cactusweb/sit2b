/**
 * FRZ.auth Module
 *
 * Description
 */
angular.module('FRZ.auth')
  .service('SessionService', [
    '$injector',
    function ($injector) {
      "use strict";
      var $rootScope = $injector.get('$rootScope'),
        $sessionStorage = $injector.get('$sessionStorage');

      /**
       *
       * @param sessionId
       * @param userId
       * @param userRole
       */
      this.create = function (sessionId, userId, userRole) {
        this.id = sessionId;
        this.userId = userId;
        this.userRole = userRole;
        //console.log('Session ID: ' + this.id);
      };

      /**
       *
       */
      this.destroy = function () {
        this.id = null;
        this.userId = null;
        this.userRole = null;
      };

      this.update = function (sessionId) {
        this.id = sessionId;
        //console.log('Session ID: ' + this.id);

        $sessionStorage.user.access_token = sessionId;
        $rootScope.user = $sessionStorage.user;
      };

      return this;
    }
  ]);