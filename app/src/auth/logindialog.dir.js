/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 03.03.15
 */

angular.module('FRZ.auth')
  .directive('loginDialog', function (AUTH_EVENTS) {
    return {
      restrict: 'A',
      template: '<div ng-if="visible" ng-include="\'src/auth/partials/login.html\'">',
      link: function (scope) {
        var showDialog = function () {
          scope.visible = true;
        };

        var hideDialog = function () {
          scope.visible = false;
        };

        scope.visible = false;

        scope.$on(AUTH_EVENTS.notAuthenticated, showDialog);
        scope.$on(AUTH_EVENTS.sessionTimeout, showDialog);

        scope.$on(AUTH_EVENTS.loginFree, hideDialog)
        scope.$on(AUTH_EVENTS.loginSuccess, hideDialog)
      }
    };
  })