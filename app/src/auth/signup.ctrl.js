/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 24.11.14
 */

angular.module('FRZ.auth')
  .controller('SignUpCtrl', [
    '$scope', '$rootScope', '$sessionStorage', '$localStorage', 'AuthModel', 'AuthService', 'AUTH_EVENTS', 'UserProfile', 'LocalData',
    function ($scope, $rootScope, $sessionStorage, $localStorage, AuthModel, AuthService, AUTH_EVENTS, UserProfile, LocalData) {
      $scope.credentials = {
        email: '',
        password: ''
      };
      $scope.error = null;

      /**
       *
       * @param credentials
       */
      $scope.signup = function (credentials) {
        AuthService.signup(credentials).then(function (user) {
          $rootScope.$broadcast(AUTH_EVENTS.signupSuccess);
          $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, user);
          $scope.error = null;
          $scope.$state.go('dashboard.start');
        }, function (data) {
          $rootScope.$broadcast(AUTH_EVENTS.signupFailed);
        });
      };

      /**
       * Check email on server
       * @param email NgModelController
       */
      $scope.checkEmail = function (email) {
        AuthModel.checkEmail({}, {
            email: email.$modelValue
          },
          //success
          function (data) {
            email.$setValidity('CheckOnServer', true);
            $scope.error = null;
          },
          function (e) {
            email.$setValidity('CheckOnServer', false);
            $scope.error = e.data;
          });
      };
    }
  ]);