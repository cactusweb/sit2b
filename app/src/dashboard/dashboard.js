/**
 * FRZ.dashboard Module
 *
 * Description
 */

angular.module('FRZ.dashboard', [
  'FRZ.profile',
  'ui.router',
  'ui.bootstrap',
  'ngSanitize',
  'angularFileUpload',
  'ui.calendar',
  'ui.date'
])
  .config([
    '$stateProvider', '$urlRouterProvider', 'USER_ROLES',
    function ($stateProvider, $urlRouterProvider, USER_ROLES) {
      "use strict";

      $stateProvider
        .state('dashboard', {
          data: {
            authorizedRoles: [USER_ROLES.user] // will inherited by child states
          },
          url: '/dashboard',
          resolve: {
            LocalData: 'LocalData',
            lists: function (LocalData) {
              return LocalData.fetchAllData();
            },
            /**
             * This is a temporary solution (kostyl).
             * Force updationg of list properties.
             * @param lists
             * @param LocalData
             * @returns {*}
             */
            updateListProps: function (lists, LocalData) {
              return LocalData.getAllListsProperties();
            }
          },
          abstract: true,
          templateUrl: 'src/dashboard/partials/dashboard.html',
          controller: ['$scope', 'UserProfile', function ($scope, UserProfile) {
            var preferences = UserProfile.getUserPreferences();
            $scope.showCalendar = preferences.showCalendar;
            $scope.showLists = preferences.showLists;

            $scope.toggleCalendarLists = function(){
              $scope.showCalendar = !$scope.showCalendar;
              $scope.showLists = !$scope.showLists;
            };

          }],
          controllerAs: 'dashboard'
        })

      /**
       * Main dashboard page
       */
        .state('dashboard.start', {
          url: '',
          views: {
            'topnav': {
              templateUrl: 'src/dashboard/partials/topnav.html',
              controller: 'TopNavCtrl'
            },
            'sidebar': {
              templateUrl: 'src/dashboard/partials/sidebar.html',
              controller: ['$scope', '$injector', function ($scope, $injector) {
                var appConfig = $injector.get('appConfig');
                $scope.apiHost = appConfig.apiHost;
                var ls = $injector.get('$localStorage');
                console.log(ls.sit2b);
              }]
            },
            'task-form': {},
            'lists': {
              templateUrl: 'src/dashboard/partials/lists.html',
              controller: 'TaskListsCtrl'
              //template: 'Calendar and Task List here'
            },
            'calendar': {
              templateUrl: 'src/dashboard/partials/calendar.html',
              controller: 'CalendarCtrl'
            }
          }
        })

      /**
       * Task creation/edit dialog
       */
        .state('dashboard.start.task', {
          url: '/task/:id',
          onEnter: ['$stateParams', '$state', '$modal', '$injector', '$localStorage', 'LocalData',
            function ($stateParams, $state, $modal, $injector, $localStorage, LocalData) {
              var $scope = $injector.get('$rootScope'),
                $q = $injector.get('$q'),
                modal;

                modal = $modal.open({
                  backdrop: 'static',
                  keyboard: false,
                  templateUrl: "src/dashboard/partials/taskform.html",
                  resolve: {
                    task: function () {
                      if ($stateParams.id === 'new') {
                        var newTaskModel = $injector.get('newTaskModel');
                        var newTask = newTaskModel.create($scope.$root.user.id, false);
                        newTask.executor = $scope.$root.user;
                        newTask.tlist_id = $localStorage.sit2b.inboxListId;
                        return newTask;
                      } else {
                        var task = LocalData.findTaskById($stateParams.id);
                        if (task === undefined) return $q.reject('not-found');
                        return angular.copy(task);
                      }
                    }
                  },
                  controller: 'TaskFormCtrl'
                });

              modal.result.then(function (reason) {
                $scope.$state.go('^');
              }, function (reason) {
                if (reason == 'not-found'){
                  $scope.$state.go('not-found', {location: false});
                } else {
                  $scope.$state.go('^');
                }
              });
            }
          ]
        })

      /**
       * Task creation/edit dialog repeated task
       */
        .state('dashboard.start.task-inst', {
          url: '/task-inst/:id?incr&period',
          onEnter: ['$stateParams', '$state', '$modal', '$injector', '$localStorage', 'LocalData',
            function ($stateParams, $state, $modal, $injector, $localStorage, LocalData) {

              var $scope = $injector.get('$rootScope'),
                $q = $injector.get('$q'),
                modal,
                incr = $stateParams.incr,
                period = $stateParams.period;

              if (incr === undefined || period === undefined) return $q.reject('not-found');

                modal = $modal.open({
                  backdrop: 'static',
                  keyboard: false,
                  templateUrl: "src/dashboard/partials/taskform.html",
                  resolve: {
                    task: function () {
                        var task = LocalData.findTaskById($stateParams.id);
                        if (task === undefined) return $q.reject('not-found');

                      console.log(task.start_date);
                      console.log(moment(task.start_date).add(incr, period));

                        var taskInst = angular.copy(task);
                        taskInst.parent_id = task.id;
                        taskInst.incr = incr;
                        taskInst.period = period;
                        taskInst.id = null;
                        taskInst.start_date =  moment(task.start_date).add(incr, period).toDate();
                        taskInst.end_date =  moment(task.end_date).add(incr, period).toDate();
                        taskInst.repeatRules = null;
                        taskInst.comments = [];
                        taskInst.files = [];
                        taskInst.logs = [];
                        taskInst.reminders = [];

                        return taskInst;
                    }
                  },
                  controller: 'TaskFormCtrl'
                });

              modal.result.then(function (reason) {
                $scope.$state.go('^');
              }, function (reason) {
                if (reason == 'not-found'){
                  $scope.$state.go('not-found', {location: false});
                } else {
                  $scope.$state.go('^');
                }
              });
            }
          ]
        })

      /**
       * List property dialog
       */
        .state('dashboard.start.list-property', {
          url: '/list/:id',
          onEnter: ['$stateParams', '$state', '$modal', 'UserProfile', '$injector',
            function ($stateParams, $state, $modal, UserProfile, $injector) {
              var $scope = $injector.get('$rootScope'),
                $q = $injector.get('$q'),
                $localStorage = $injector.get('$localStorage'),
                LocalData = $injector.get('LocalData'),
                modal;

              modal = $modal.open({
                backdrop: 'static',
                templateUrl: "src/dashboard/partials/listform.html",
                resolve: {
                  listProps: function () {
                    if ($stateParams.id === 'new') {
                      return UserProfile.getDefaultProperties();
                    } else {
                      if (!LocalData.doesListExists($stateParams.id)) return $q.reject('not-found');
                      return UserProfile.getListProperties($stateParams.id);
                    }
                  },
                  executors: function () {
                    if ($stateParams.id === 'new') {
                      return LocalData.getListExecutors($localStorage.sit2b.inboxListId);
                    } else {
                      if (!LocalData.doesListExists($stateParams.id)) return $q.reject('not-found');
                      return LocalData.getListExecutors($stateParams.id);
                    }
                  },
                  members: function () {
                    if ($stateParams.id === 'new') {
                      return [];
                    } else {
                      if (!LocalData.doesListExists($stateParams.id)) return $q.reject('not-found');
                      if (!$localStorage.sit2b.listsAndTasks[$stateParams.id].private) {
                        return LocalData.getListMembers($stateParams.id);
                      } else {
                        return [];
                      }
                    }
                  }
                },
                controller: 'ListFormCtrl',
                windowClass: 'listSettingsPopup'
              });

              modal.opened.then(function () {
                var $timeout = $injector.get('$timeout');
                $timeout(alignPopup);
                $(window).resize(alignPopup);
                function alignPopup(){
                  var place = $('a#list-' + $stateParams.id).offset(),
                    right = $(window).width() - place.left;
                  console.log('place:', place);
                  console.log('right:', right);
                  if ($stateParams.id === 'new') right -= 6;
                  $('div.popup.list_settings').css({top: place.top-20, right: right-39, position: 'absolute'});
                  if (place.left < $('div.popup.list_settings').width()) $('div.popup.list_settings').addClass('go-right');
                }

              });

              modal.result.then(function (reason) {
                $scope.$state.go('^');
              }, function (reason) {
                if (reason == 'not-found'){
                  $scope.$state.go('not-found', {location: false});
                } else {
                  $scope.$state.go('^');
                }
              });
            }
          ]
        })

      /**
       * List sharing dialog.
       */
        .state('dashboard.start.list-property.share', {
          url: '/share',
          onEnter: ['$stateParams', '$state', '$modal', 'UserProfile', '$injector',
            function ($stateParams, $state, $modal, UserProfile, $injector) {
              var $scope = $injector.get('$rootScope'),
                LocalData = $injector.get('LocalData'),
                $q = $injector.get('$q'),
                modal;

              modal = $modal.open({
                backdrop: 'static',
                templateUrl: "src/dashboard/partials/list_share.html",
                resolve: {
                  listProps: function () {
                    if (!LocalData.doesListExists($stateParams.id)) return $q.reject('not-found');
                    return UserProfile.getListProperties($stateParams.id);
                  }
                },
                controller: 'ListShareCtrl'
              });

              modal.opened.then(function () {
                var $timeout = $injector.get('$timeout');
                $timeout(function () {
                  var place = $('div.popup.list_settings').offset(),
                    pW = $('div.popup.list_settings').width(),
                    mW = $('div.popup.list_share').width(),
                    left = place.left + ((pW - mW) / 2);
                  $('div.popup.list_share').offset({top: place.top - 190, left: left});
                });
              });

              modal.result.then(function (reason) {
                $scope.$state.go('^');
              }, function (reason) {
                if (reason == 'not-found'){
                  $scope.$state.go('not-found', {location: false});
                } else {
                  $scope.$state.go('^');
                }
              });
            }
          ]
        })

      /**
       * Log popup window
       */
        .state('dashboard.start.log', {
          url: '/notifications',
          onEnter: ['$stateParams', '$state', '$modal', '$injector', '$localStorage',
            function ($stateParams, $state, $modal, $injector, $localStorage) {
              var $scope = $injector.get('$rootScope'),
                modal = $modal.open({
                  backdrop: 'static',
                  templateUrl: "src/dashboard/partials/log.html",
                  resolve: {
                    logs: function () {
                      return $localStorage.sit2b.logs;
                    }
                  },
                  controller: 'LogCtrl'
                });

              modal.result.then(function () {
                $scope.$state.go('^');
              }, function () {
                $scope.$state.go('^');
              });
            }
          ]
        })

      /**
       * Comments popup window
       */
        .state('dashboard.start.comments', {
          url: '/comments',
          onEnter: ['$stateParams', '$state', '$modal', '$injector', '$localStorage',
            function ($stateParams, $state, $modal, $injector, $localStorage) {
              var $scope = $injector.get('$rootScope'),
                modal = $modal.open({
                  backdrop: 'static',
                  templateUrl: "src/dashboard/partials/comments.html",
                  resolve: {
                    comments: function () {
                      return $localStorage.sit2b.comments;
                    }
                  },
                  controller: 'CommentCtrl'
                });

              modal.result.then(function () {
                $scope.$state.go('dashboard.start');
              }, function () {
                $scope.$state.go('dashboard.start');
              });
            }
          ]
        })

      /**
       * Trash popup window
       */
        .state('dashboard.start.trash', {
          url: '/trash',
          onEnter: ['$stateParams', '$state', '$modal', '$injector', '$localStorage',
            function ($stateParams, $state, $modal, $injector, $localStorage) {
              var $scope = $injector.get('$rootScope'),
                modal = $modal.open({
                  backdrop: 'static',
                  templateUrl: "src/dashboard/partials/trash.html",
                  resolve: {
                    tasks: function () {
                      return $localStorage.sit2b.trash;
                    }
                  },
                  controller: 'TrashCtrl'
                });

              modal.result.then(function () {
                $scope.$state.go('^');
              }, function () {
                $scope.$state.go('^');
              });
            }
          ]
        });
    }
  ])

  .service('ConfirmDialog', ['$injector', function ($injector) {
    /**
     *
     * @param question
     * @param parent
     * @returns {*}
     */
    this.open = function (question, parent) {
      var $modal = $injector.get('$modal'),
        modal = $modal.open({
          size: 'sm',
          backdrop: 'static',
          templateUrl: "src/dashboard/partials/confirm.html",
          resolve: {
            question: function () {
              return question;
            }
          },
          controller: ['$scope', 'question', function ($scope, question) {
            $scope.question = question;

            $scope.ok = function () {
              $scope.$close(true);
            };

            $scope.cancel = function () {
              $scope.$dismiss(false);
            };
          }]
        });

      modal.opened.then(function () {
        var $timeout = $injector.get('$timeout');

        $timeout(function () {
          var myself = $('#confirm'),
            parentPos = parent.position(),
            offset = {
              top: 0,
              left: parentPos.left + ((parent.outerWidth() - myself.outerWidth()) / 2)
            };

          myself.offset({top: offset.top, left: offset.left});
        });
      });

      return modal.result;
    };

  }]);