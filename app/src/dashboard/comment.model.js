angular.module('FRZ.dashboard')
  .factory('CommentModel', ['$resource', 'appConfig', function ($resource, appConfig) {
        return $resource(appConfig.apiUrl + '/comment/:id',
          {id: '@id'}, {
            'update': {method: 'PUT'},
            'get':{isArray: true}
          }
        );
  }]);