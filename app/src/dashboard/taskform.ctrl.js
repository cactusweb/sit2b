/**
 * frzDashboard
 *
 * Description
 */
angular.module('FRZ.dashboard')
  .controller('TaskFormCtrl',
  ['$scope', 'task', 'appConfig', '$injector', '$modal', '$upload', '$http', 'UserModel', 'UserProfile', 'CommentModel', 'TaskModel', 'ListModel', '$interval', '$localStorage', 'LocalData', 'toastr',
    function ($scope, task, appConfig, $injector, $modal, $upload, $http, UserModel, UserProfile, CommentModel, TaskModel, ListModel, $interval, $localStorage, LocalData, toastr) {
      var $timeout = $injector.get('$timeout');
      console.log('TASK ', task);
      $scope.task = task;
      $scope.apiHost = appConfig.apiHost;
      $scope.taskList = $localStorage.sit2b.listsAndTasks[task.tlist_id];
      console.log($scope.taskList);

      $scope.originTask = angular.copy($scope.task); // для сравнений

      $scope.dateOptions = {
        changeYear: true,
        changeMonth: true,
        yearRange: '1900:-0'
      };

      $scope.form = {};
      $scope.form.start_date = new Date(); // for timepicker
      $scope.form.end_date = moment().add($scope.task.duration, 'minutes').toDate(); // for timepicker
      $scope.form.disabled = false;
      $scope.form.comment = ''; // text of the new comment.
      $scope.form.reminderTypes = [
        {
          value: 'system',
          label: 'Внутри системы',
          position: 0
        },
        {
          value: 'email',
          label: 'На E-mail',
          position: 1
        },
        {
          value: 'sms',
          label: 'SMS',
          position: 2
        }
      ];
      $scope.form.reminderTimeUnits = [
        {
          value: 'i',
          label: 'минут',
          position: 0
        },
        {
          value: 'h',
          label: 'часов',
          position: 1
        },
        {
          value: 'd',
          label: 'дней',
          position: 2
        },
        {
          value: 'w',
          label: 'недель',
          position: 3
        },
        {
          value: 'm',
          label: 'месяцев',
          position: 4
        }
      ];

      /**
       *
       */
      $scope.form.addReminder = function () {
        if (!$scope.task.start_date) {
          toastr.warning('Сначала нужно задать дату исполнения задачи!', 'Будьте  внимательны!');
          return;
        }

        var newReminder = {
          "id": null,
          "task_id": $scope.task.id,
          "type": $scope.form.reminderTypes[0].value,
          "before": 10,
          "time_unit": $scope.form.reminderTimeUnits[0].value,
          "send_date": ""
        };
        $scope.task.reminders.push(newReminder);
      };

      $scope.form.removeLastReminder = function () {
        $scope.task.reminders.pop();
      };

      $scope.form.getLastReminderText = function () {
        if (angular.isArray($scope.task.reminders) && $scope.task.reminders.length > 0) {
          var reminders = $scope.task.reminders.slice(0);
          var lastReminder = reminders.pop();

          var type = $.grep($scope.form.reminderTypes, function (n, i) {
            if (lastReminder.type === n.value) {
              return true;
            }
          });
          var time_unit = $.grep($scope.form.reminderTimeUnits, function (n, i) {
            if (lastReminder.time_unit === n.value) {
              return true;
            }
          });

          return type[0].label + ', за ' + lastReminder.before + ' ' + time_unit[0].label + '';
        }
      };

      //Files upload.
      $scope.form.files = [];
      $scope.form.filePreloader = false;

      $scope.form.fileDelete = function (e) {
        var index = parseInt($(e.target).data('key'));
        $scope.task.files.splice(index, 1);
      };

      $scope.form.fileCount = function () {
        var fileCount = $scope.task.files.length;
        if (fileCount) {
          return 'файлов: ' + fileCount;
        }
      };

      $scope.$watch('form.files', function () {
        var fileCounter = 0;
        for (var i = 0; i < $scope.form.files.length; i++) {
          var file = $scope.form.files[i];
          $scope.upload = $upload.upload({
            url: appConfig.apiUrl + '/file',
            method: 'POST',
            headers: {'Content-Type': file.type != '' ? file.type : 'application/octet-stream'}, // only for html5
            withCredentials: false,
            data: {taskId: $scope.task.id},
            file: file
          }).progress(function (evt) {
            $scope.form.disabled = true;
            $scope.form.filePreloader = true;
          }).success(function (data, status, headers, config) {
            fileCounter++;
            if (fileCounter === i) {
              $scope.form.disabled = false;
            }
            $scope.form.filePreloader = false;
            $scope.task.files.push(data);
            console.log($scope.task.files);
          }).error(function (data) {
            fileCounter++;
            $scope.form.disabled = false;
            $scope.form.filePreloader = false;
            alert(data.status + ': ' + data.message);
          });
        }

      });

      $scope.$watch('task.files', function () {
        if (!angular.equals($scope.task.files, $scope.originTask.files)) {
          $scope.form.changes.uploads = true;
        }
      }, true);

      $scope.form.lists = [];
      angular.forEach(UserProfile.getUserPreferences().lists, function (list) {
        $scope.form.lists.push(
          {
            value: list.id,
            label: list.own_title != '' ? list.own_title : list.title,
            position: 0
          }
        );
      });

      $scope.form.showCreateList = false;
      $scope.form.createList = function () {
        ListModel.save({
          title: $scope.form.newListName
        }, function (result) {
          $scope.form.lists.push(
            {
              value: result.id,
              label: result.title,
              position: 0
            }
          );

          LocalData.putLocalList(result);
          UserProfile.setListProperties(result.id, UserProfile.getDefaultProperties(result.id));

          $scope.task.tlist_id = result.id;
          $scope.form.showCreateList = false;

        }, function (result) {
          alert(result.data.status + ': ' + result.data.message);
        });
      };

      $scope.form.showDescriptionAsList = angular.isArray(task.description) ? true : false;

      /**
       * Flags of changes of panels
       */
      $scope.form.changes = {
        title: false,
        description: false,
        list: false,
        priority: false,
        duration: false,
        repeater: false,
        reminder: false,
        location: false,
        executor: false,
        uploads: false,
        comments: false,
        log: false
      };

      $scope.form.durationChanged = function(){
        $timeout(function(){
          $scope.task.end_date = moment($scope.task.start_date).add($scope.task.duration, 'minutes').toDate();
        });
      };

      /**
      * Recalculate the task duration if time was changed.
      */
      $scope.$watchGroup(['task.start_date', 'task.end_date'], function (n, o) {
        var diff, start = 0, end = 1;

        if (!(o[start] instanceof Date) && n[start] instanceof Date){
          var nowDate = moment();
          $scope.task.start_date = moment(n[start]).add({hours: nowDate.hours(), minutes: nowDate.minutes()}).toDate();
          return;
        }

        if (!(o[end] instanceof Date) && n[end] instanceof Date){
          var nowDate = moment();
          $scope.task.end_date = moment(n[end]).add({hours: nowDate.hours(), minutes: nowDate.minutes()+$scope.task.duration}).toDate();
          return;
        }

        if (!(n[start] instanceof Date) || !(n[end] instanceof Date)) return;
        if (n[end].getTime() < n[start].getTime()) n[end] = n[start];

        if (n[start] !== o[start] || n[end] !== o[end]) {
          diff = (n[end].getTime() - n[start].getTime()) / 60000; // ms
          $scope.task.duration = diff;
          $scope.form.changes.duration = true;
        }
      });

      /**
       * Clear 'send_date' of existing reminders if dates are changed.
       */
      $scope.$watch('task.start_date', function () {
        $timeout(function () {
          if (angular.isArray($scope.task.reminder) && $scope.task.reminder.length) {
            for (var i = 0; i < $scope.task.reminder.length; i++) {
              $scope.task.reminder[i].send_date = null;
            }
          }
        });
      }, true);

      /**
       * Clear any dates from the task.
       * @param event
       */
      $scope.form.clearDates = function (event) {
        event.stopPropagation();
        $scope.task.start_date = null;
        $scope.task.end_date = null;
        $scope.form.changes.duration = true;
        $scope.form.clearRepeates();
        $scope.task.reminders = [];
      };

      /**
       * Dates summary for the block title.
       * @returns {string}
       */
      $scope.form.showDateSummary = function () {
        var summary = '';
        var duration = {};
        duration.hours = Math.floor($scope.task.duration / 60),
          duration.minutes = $scope.task.duration % 60;

        if ($scope.task.start_date instanceof Date && $scope.task.end_date instanceof Date) {
          summary += moment($scope.task.start_date).format('HH:mm DD.MM.YYYY') + ' — ';
          summary += moment($scope.task.end_date).format('HH:mm DD.MM.YYYY') + ', ';
          summary += duration.hours + ' час ' + duration.minutes + ' мин.';
        }

        return summary;
      };

      /**
       * Change timezone
       */
      $scope.form.changeTimezone = function () {
        console.log('Change timezone');
      };

      // Datapickers status
      $scope.form.openedDatePicker = {
        start: false,
        end: false
      };

      /**
       * Toggle data picker for 'start date'
       * @param $event
       */
      $scope.form.toggleStartDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.form.openedDatePicker.start = !$scope.form.openedDatePicker.start;
      };

      /**
       * Toggle data picker for 'end date'
       * @param $event
       */
      $scope.form.toggleEndDate = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.form.openedDatePicker.end = !$scope.form.openedDatePicker.end;
      };


      /**
       * Repeat rules
       */
      $scope.form.repeat = {};
      if (angular.isObject($scope.task.repeatRules)) {
        //$scope.task.repeatRules.repeat_days = JSON.parse($scope.task.repeatRules.repeat_days);
        //console.log('repeatRules');
        //console.log($scope.task.repeatRules);
        $scope.form.repeat.rule = $scope.task.repeatRules;
        $scope.form.repeat.rule.start_date = new Date($scope.form.repeat.rule.start_date);
        console.log($scope.form.repeat.rule.start_date);
      } else {
        $scope.form.repeat.rule = {
          period: 'once',
          interval: null,
          start_date: null,
          end_date: null,
          limit: null,
          repeat_days: [0, 0, 0, 0, 0, 0, 0]
        };
        var d = new Date();
        $scope.form.repeat.rule.repeat_days[d.getDay()] = 1;
        //console.log($scope.form.repeat.rule);
      }

      $scope.form.repeat.showCompleted = 0;

      $scope.form.repeat.interval = 1;
      $scope.form.repeat.intervals = null;

      $scope.form.repeat.periods = [
        {value: 'once', label: 'Не повторять', position: 0},
        {value: 'day', label: 'Каждый день', position: 1},
        {value: 'week', label: 'Каждую неделю', position: 2},
        {value: 'month', label: 'Каждый месяц', position: 3},
        {value: 'year', label: 'Каждый год', position: 4}
      ];

      $scope.form.repeat.d_intervals = [
        {value: 1, label: '1 день', position: 0},
        {value: 2, label: '2 дня', position: 1},
        {value: 3, label: '3 дня', position: 2},
        {value: 4, label: '4 дня', position: 3},
        {value: 5, label: '5 дней', position: 4},
        {value: 6, label: '6 дней', position: 5}
      ];

      $scope.form.repeat.w_intervals = [
        {value: 1, label: '1 неделя', position: 0},
        {value: 2, label: '2 недели', position: 1},
        {value: 3, label: '3 недели', position: 2}
      ];

      $scope.form.repeat.m_intervals = [
        {value: 1, label: '1 месяц', position: 0},
        {value: 2, label: '2 месяца', position: 1},
        {value: 3, label: '3 месяца', position: 2},
        {value: 4, label: '4 месяца', position: 3},
        {value: 5, label: '5 месяцев', position: 4},
        {value: 6, label: '6 месяцев', position: 5},
        {value: 7, label: '7 месяцев', position: 6},
        {value: 8, label: '8 месяцев', position: 7},
        {value: 9, label: '9 месяцев', position: 8},
        {value: 10, label: '10 месяцев', position: 9},
        {value: 11, label: '11 месяцев', position: 10}
      ];

      $scope.form.repeat.y_intervals = [
        {value: 1, label: '1 год', position: 0},
        {value: 2, label: '2 года', position: 1},
        {value: 3, label: '3 года', position: 2}
      ];

      $scope.form.repeat.ending = 'never';
      $scope.form.repeat.endings = [
        {value: 'never', label: 'Никогда', position: 0},
        {value: 'afterRepeats', label: 'После повторов', position: 1},
        {value: 'untilDate', label: 'До даты', position: 2},
      ];

      $scope.$watch('form.repeat.rule.ending', function (newVal, oldVal) {
        switch (newVal) {
          case 'never':
            $scope.form.repeat.end_date = null;
            $scope.form.repeat.limit = null;
            break;
          case 'afterRepeats':
            $scope.form.repeat.end_date = null;
            break;
          case 'untilDate':
            $scope.form.repeat.limit = null;
            $scope.form.repeat.end_date = new Date();
            break;
        }
      });

      $scope.$watch('form.repeat.rule.period', function (newVal, oldVal) {
        if (newVal != 'once' && !$scope.task.start_date) {
          toastr.warning('Сначала нужно задать дату исполнения задачи!', 'Будьте  внимательны!');
          $scope.form.repeat.rule.period = 'once';
          return;
        }

        if (!$scope.form.repeat.rule.start_date) {
          $scope.form.repeat.rule.start_date = new Date($scope.task.start_date);
          $scope.form.repeat.rule.start_date = new Date();
        }

        switch (newVal) {
          case 'day':
            $scope.form.repeat.intervals = $scope.form.repeat.d_intervals;
            break;
          case 'week':
            $scope.form.repeat.intervals = $scope.form.repeat.w_intervals;
            break;
          case 'month':
            $scope.form.repeat.intervals = $scope.form.repeat.m_intervals;
            break;
          case 'year':
            $scope.form.repeat.intervals = $scope.form.repeat.y_intervals;
            break;
          default: // once
            $scope.form.repeat.intervals = [];
            $scope.form.repeat.interval = null;
            $scope.form.repeat.rule.start_date = null;
        }
      });

      /**
       * Rerpeater block title.
       * @returns {string}
       */
      $scope.form.showRepeatSummary = function () {
        var getQtySuffics = function (qty) {
          var last, s;
          /*
           qty = qty + '';
           last = qty.substr(qty.length - 2);
           console.log(qty, last);
           switch (last) {
           case '2':
           case '3':
           case '4':

           s = ' раза';
           break;
           default:
           s = ' раз';
           }
           */
          s = ' раз(а)'
          return s;
        };

      /**
       * Rerpeater block title.
       * @returns {string}
       */
      $scope.form.clearRepeates = function (ev) {
        if (ev) ev.stopPropagation();
        $scope.form.repeat.rule = {
          period: 'once',
          interval: null,
          start_date: '',
          end_date: '',
          limit: null,
          repeat_days: [0, 0, 0, 0, 0, 0, 0]
        };
        $scope.form.changes.repeater = true;
      };

        var rule = $scope.form.repeat.rule,
          d_names = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
          days = [],
          summary = '';

        if (typeof rule.start_date == 'string') {
          rule.start_date = new Date();
        }

        switch (rule.period) {
          case 'day':
            if (rule.interval > 1) {
              summary += 'каждый ' + rule.interval + '-й день';
            } else {
              summary += 'каждый день';
            }
            break;
          case 'week':
            if (rule.interval > 1) {
              summary += 'каждую ' + rule.interval + '-ю неделю';
            } else {
              summary += 'каждую неделю';
            }

            for (var i = 0; i < rule.repeat_days.length; i++) {
              rule.repeat_days[i] && days.push(d_names[i]);
            }
            summary += ' в ' + days.join(', ');
            break;
          case 'month':
            if (rule.interval > 1) {
              summary += 'каждый ' + rule.interval + '-й месяц';
            } else {
              summary += 'каждый месяц';
            }
            summary += ' ' + rule.start_date.getDate() + '-го числа';
            break;
          case 'year':
            if (rule.interval > 1) {
              summary += 'каждый ' + rule.interval + '-й год';
            } else {
              summary += 'каждый год';
            }
            summary += ' ' + moment($scope.form.repeat.rule.start_date).format('D MMMM');
            break;
        }

        if ($scope.form.repeat.ending == 'afterRepeats' && $scope.form.repeat.rule.limit) {
          summary += ', ' + $scope.form.repeat.rule.limit + getQtySuffics($scope.form.repeat.rule.limit);
        } else if ($scope.form.repeat.ending == 'untilDate' && $scope.form.repeat.rule.end_date) {
          summary += ', до ' + $scope.form.repeat.rule.end_date.toLocaleDateString();
        }

        return summary;
      }; // end of $scope.form.showRepeatSummary()

      /**
       * Reminders
       */
      $scope.$watch('task.reminders', function () {
        if (!angular.equals($scope.task.reminders, $scope.originTask.reminders)) {
          $scope.form.changes.reminder = true;
        }
      }, true);

      /**
       * Executor was changed.
       */
      $scope.$watch('task.executor_id', function (newVal, oldVal) {
        if ($scope.task.executor_id != $scope.originTask.executor_id) {
          if (newVal) {
            $scope.task.executor = UserModel.get({id: newVal});
          } else {
            $scope.task.executor = null;
          }

          $scope.form.changes.executor = true;
        }
      });

      /**
       * Executors list
       * @type {Array}
       */
      $scope.form.executors = [];
      $scope.getListExecutors = function (tlist_id) {
        if (tlist_id) {
          ListModel.executors({id: tlist_id}, function (executors) {
            angular.forEach(executors, function (val, key) {
              $scope.form.executors.push({
                value: Number(val.id),
                label: val.firstname + ' ' + val.lastname + ' (' + val.email + ')',
                position: 0
              });
            });
          });
        }
      };

      $scope.getListExecutors(task.tlist_id);

      /**
       * Change list id
       */
      $scope.$watch('task.tlist_id', function (newVal, oldVal) {
        if (newVal != oldVal) {
          var listProps = UserProfile.getListProperties(newVal);
          $scope.form.changes.list = true;

          $timeout(function () {
            if (!$scope.task.id) {
              $scope.task.duration = parseInt(listProps.def_duration);
            }
            console.log('LIST ID changed, duration set to ', $scope.task.duration);
          });

          $scope.getListExecutors(newVal);
        }
      }, true);

      /**
       * label for block 'Executor'
       * @returns {string}
       */
      $scope.form.getExecutorName = function () {
        var name = '';
        if ($scope.task.executor !== null) {
          name = $scope.task.executor.firstname + ' '
          + $scope.task.executor.lastname + ' (' + $scope.task.executor.email + ')';
        }
        return name;
      };

      $scope.form.deleteExecutor = function () {
        if (task.executor_id == $scope.$root.user.id) {
          console.log('нельзя удалить себя как исполнителя');
        } else {
          task.executor_id = $scope.$root.user.id;
        }
      };

      //task comment submit
      $scope.commentSubmit = function () {
        var comment = new CommentModel();
        comment.data = {
          taskId: $scope.task.id,
          text: $scope.form.comment
        };
        CommentModel.save(comment, function (result) {
          //success

          //add new comments to local task
          LocalData.addComment($scope.task, result.data);

          $scope.form.comment = '';
          console.log(result.data);
          $scope.task.comments.push(result.data);
        }, function (result) {
          //error
          alert(result.data.status + ': ' + result.data.message);
          console.log('error');
          console.log(result.data);
        });
      };

      // task changing
      $scope.$watch('task.title', function (n, o) {
        if (n !== o) {
          $scope.form.changes.title = true;
        }
      });

      $scope.$watch('task.description', function (n, o) {
        if (n !== o) {
          $scope.form.changes.description = true;
        }
      }, true);

      $scope.$watch('task.priority', function (n, o) {
        if (n !== o) {
          $scope.form.changes.priority = true;
        }
      });

      $scope.submit = function () {
        $scope.$close(true);
      };

      /**
       * Check for changes.
       * @returns {boolean}
       */
      $scope.hasChanges = function () {
        var hasChanges =  false;

        for(var prop in $scope.form.changes) {
          if ($scope.form.changes[prop]) {
            hasChanges = true;
            break;
          }
        }

        return hasChanges;
      };

      /**
       * Button "Close" and "Cancel" with confirmation.
       */
      $scope.cancel = function (needConfirm) {
        if (needConfirm === undefined) needConfirm = true;

        if (needConfirm && $scope.hasChanges()) {
          var confirm = $injector.get('ConfirmDialog'),
            parent = $('div.popup.new_task'),
            sure = confirm.open('Сохранить внесенные вами изменения?', parent);

            sure.then(function (res) {
              console.log('Тут сохраняем задачу');
              $scope.form.save();
              $scope.$dismiss('cancel');
            }, function (res) {
              $scope.$dismiss('cancel');
            })
        } else {
          $scope.$dismiss('cancel');
        }
      };

      /**
       * Delete current task while editing.
       */
      $scope.deleteTask = function (task, dontclose) {
        var parent = $('div.popup.new_task');
        var ConfirmDialog = $injector.get('ConfirmDialog');
        var sure = ConfirmDialog.open('Вы действительно хотите удалить задачу?', parent);

        sure.then(function (res) {
          console.log('Delete action');
          LocalData.removeTaskFromLocalList(task);
          TaskModel.delete({id: task.id}, task,
            function (result) {
              toastr.info('"' + task.title + '"', 'Задача удалена');
              if(task.parent_id){
                delete $localStorage.sit2b.listsAndTasks[task.tlist_id].tasks[task.parent_id].repeatRules.exception[task.repeat_exception_code];
                delete $scope.task.childs[task.id];
                delete $localStorage.sit2b.listsAndTasks[task.tlist_id].tasks[task.parent_id].childs[task.id];
              }

              if(dontclose != true){
                $scope.$close(true);
              }

            }, function (task) {
              console.log('Задача НЕ удалена ', task.data);
              toastr.error('Ошибка удаления задачи');
              alert(result.data.status + ': ' + result.data.message);
            });
        }, function (res) {
          console.log('Canceled');
        })
      };

      // task author
      if ($scope.task.user_id != $scope.$root.user.id) {
        $scope.form.username = '';
        // взять с сервера
        UserModel.get({id: $scope.task.user_id},
          function (user) {
            $scope.form.username = user.firstname + ' ' + user.lastname;
          },
          function (user) {
            console.log('ERROR: ', user);
          }
        );
      } else {
        $scope.form.username = 'Я (' + $scope.$root.user.firstname + ' ' + $scope.$root.user.lastname + ')';
      }

      /**
       * Write down tha task changes
       *  - send to server
       *  - update local data
       */
      $scope.form.save = function () {
        if ((angular.isString($scope.task.title) && $scope.task.title != "")) {
          if ($scope.form.repeat.rule.period == 'once') {
            $scope.task.repeatRules = null;
          } else {
            $scope.task.repeatRules = $scope.form.repeat.rule;
          }

          var listInputVal = $('.frzTaskDescriptionListAdd').val();
          if(listInputVal != undefined && $.trim(listInputVal) != ''){
            $scope.task.description.push({
              subject: listInputVal,
              done: false
            });
          }

          if ($scope.task.id) { // update task
            TaskModel.update({id: $scope.task.id}, $scope.task,
              function (task) {
                LocalData.prepareTask(task)
                  .removeTaskFromLocalList(task)
                  .putTaskToLocalList(task);
                console.log('Задача записана ', task);
              }, function (result) {
                console.log('Задача НЕ записана ', result.data);
                alert(result.data.status + ': ' + result.data.message);
              });
          } else if(!$scope.task.parent_id){ // create new task
            TaskModel.save($scope.task,
              function (task) {
                LocalData
                  .prepareTask(task)
                  .putTaskToLocalList(task);
                console.log('Задача записана ', task, $localStorage.sit2b.listsAndTasks);
              }, function (result) {
                console.log('Задача НЕ записана ', result.data);
                alert(result.data.status + ': ' + result.data.message);
              });
          }else{ // create new task from repeatRule
            if ($scope.hasChanges()) {
              console.log('create new task from repeatRule');
              console.log($scope.task);
              TaskModel.save($scope.task,
                function (task) {
                  $localStorage.sit2b.listsAndTasks[task.tlist_id].tasks[task.parent_id].repeatRules.exception[task.repeat_exception_code] = 1;
                  LocalData
                    .prepareTask(task)
                    .putTaskToLocalList(task);
                  $localStorage.sit2b.listsAndTasks[task.tlist_id].tasks[task.parent_id].childs[task.id] = task;
                  console.log('Задача записана ', task, $localStorage.sit2b.listsAndTasks);
                }, function (result) {
                  console.log('Задача НЕ записана ', result.data);
                  alert(result.data.status + ': ' + result.data.message);
                });
            }
          }
        } else {
          $scope.fTask.title.$setDirty();
          console.log('Нельзя записывать');
        }
      };


      /**
       * Show confirm modal window.
       * @param question
       */
      $scope.confirmAreYouSure = function (q) {
        var modal = $modal.open({
          size: 'sm',
          backdrop: 'static',
          templateUrl: "src/dashboard/partials/confirm.html",
          resolve: {
            question: function () {
              return q;
            }
          },
          controller: ['$scope', 'question', function ($scope, question) {
            $scope.question = question;

            $scope.ok = function () {
              $scope.$close(true);
            };

            $scope.cancel = function () {
              $scope.$dismiss(false);
            };
          }]
        });

        return modal.result;
      };

      /**
       * Div-Scroll
       * info: https://github.com/noraesae/perfect-scrollbar
       */
      $scope.perfectScrollbarInit = function() {
        var scrollConteiner = $("#task-form-wrapper .body_wrapper");
        var contentHeight = $("#task-form-wrapper .body_content").height();
        var maxHeight = $("#task-form-wrapper").parent().height() - 60;

        $(scrollConteiner).css('max-height', maxHeight);

        if (contentHeight > maxHeight && !$(scrollConteiner).hasClass('ps-container')) {
          $(scrollConteiner).perfectScrollbar({
            wheelSpeed: 20,
            suppressScrollX: true,
            minScrollbarLength: 20
          });
        }
        if ($(scrollConteiner).hasClass('ps-container')) {
          $(scrollConteiner).perfectScrollbar('update');
        }
      }

      setTimeout(function () {
        $scope.perfectScrollbarInit();
        $(window).resize($scope.perfectScrollbarInit);
        $("#task-form-wrapper .panel-heading").click(function () {
          setTimeout($scope.perfectScrollbarInit, 260);
        });
      }, 1);


      /**
       * Form log data rendering
       */
      $scope.form.lastLogDate = null;

      $scope.form.getLogDate = function (log) {
        if (!moment(log.created).isSame($scope.form.lastLogDate, 'day')) {
          $scope.form.lastLogDate = log.created;
          log.showDate = true;
        }
        return moment(log.created).format('DD MMMM YYYY');
      };

      $scope.form.getLogTime = function (log_created) {
        return moment(log_created).format('HH:mm');
      };

      $scope.form.getLogText = function (log_text) {
        var usernmae = $scope.$root.user.firstname + ' ' + $scope.$root.user.lastname;
        return log_text.replace(usernmae, '<span>Я</span>');
      };

      this.testFromOut = function () {
        console.log('testFromOut');
      }


      /**
       * TODO: method from TaskListsCtrl
       * Toggle 'done' state and save task changes.
       * @param task
       */
      $scope.toggleDoneState = function (task) {
        //$scope.$state.reload();
        TaskModel.update({id: task.id}, task,
          function (task) {
            $localStorage.sit2b.listsAndTasks[task.tlist_id].tasks[task.parent_id].childs[task.id].done = Number(task.done);
            LocalData
              .prepareTask(task)
              .putTaskToLocalList(task);
            //$scope.reloadState();
            console.log('Задача записана ', task);
          }, function (task) {
            console.log('Задача НЕ записана ', task.data);
            alert(result.data.status + ': ' + result.data.message);
          });
      };

    }]);
