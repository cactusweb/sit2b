angular.module('FRZ.dashboard')
  .controller('CommentCtrl', ['$scope', '$injector', 'comments', '$localStorage', 'LocalData', 'appConfig', '$http', function ($scope, $injector, comments, $localStorage, LocalData, appConfig, $http) {
    $scope.comments = comments;
    $scope.apiHost = appConfig.apiHost;

    $scope.getCommentDate = function(created){
      return moment(created).format('DD MMMM YYYY, HH:mm');
    };

    $scope.getCommentUsername = function(user){
      var username = user.firstname+' '+user.lastname;

      if($scope.$root.user.id === user.id){
        username = 'Я';
      }

      return username;
    };

    //console.log(comments);

    /**
     * Div-Scroll
     * info: https://github.com/noraesae/perfect-scrollbar
     */
    $scope.perfectScrollbarInit = function() {
      var scrollConteiner = $("#comments .popup_content");
      var contentHeight = $(scrollConteiner).height();
      var maxHeight = $(scrollConteiner).parent().parent().height() - 30;

      $(scrollConteiner).css('max-height', maxHeight);

      if(contentHeight > maxHeight && !$(scrollConteiner).hasClass('ps-container')){
        $(scrollConteiner).perfectScrollbar({
          wheelSpeed: 20,
          suppressScrollX: true,
          minScrollbarLength: 20
        });
      }
      if($(scrollConteiner).hasClass('ps-container')){
        $(scrollConteiner).perfectScrollbar('update');
      }
    };

    $scope.$on('onRepeatLast', function(scope, element, attrs){
        $http.post(appConfig.apiUrl+'/comment/mark-as-read', { data: $scope.comments.all[0] }).
          success(function(data, status, headers, config) {
            LocalData.getAllComments();
          }).
          error(function(data, status, headers, config) {
            alert(result.data.status + ': ' + result.data.message);
            console.log('error');
            console.log(result.data);
          });

        $localStorage.sit2b.comments.new.length = 0;

        $scope.perfectScrollbarInit();
        $(window).resize($scope.perfectScrollbarInit);
    });
    /**
     * Close modal.
     */
    $scope.close = function () {
      $scope.$close(true);
    };

  }]);
