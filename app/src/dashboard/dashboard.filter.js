/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 12.02.15
 */

angular.module('FRZ.dashboard')
/**
 * This filter needed for using in 'ng-repear'
 * while passing collection which is an object.
 * In this case orderBy will work correctly.
 *
 * Sample:
 * <div ng-repeat="(key, element) in someList | toArray | orderBy:'id'">
 */
  .filter("toArray", function () {
    return function (obj) {
      var result = [];
      angular.forEach(obj, function (val, key) {
        result.push(val);
      });
      return result;
    };
  })

/**
 * Select all tasks which belong to specified user.
 *
 * Usage: belongsToExecutor:executor_id (int)
 */
  .filter('belongsToExecutor', function () {
    return function (taskList, id) {
      var tempList = [];

      id = Number(id);

      if (id === 0) {
        return taskList;
      } else {
        angular.forEach(taskList, function (task) {
          if (task.executor_id == id) {
            tempList.push(task);
          }
        });

        return tempList;
      }
    }
  })

/**
 * Select only completed task (done == 1).
 *
 * Usage: showCompletedTask:show (bool)
 */
  .filter('showCompletedTask', function () {
    return function (taskList, show) {
      var tempList = [];

      show = show === undefined ? 1 : show;

      angular.forEach(taskList, function (task) {
        if (task.done == 1 && show == 0) {
          return;
        }

        tempList.push(task);
      });

      return tempList;
    }
  })

/**
 * Hide repeated tasks.
 *
 * Usage: hideChilds:show (bool)
 */
  .filter('hideChilds', function () {
    return function (taskList, show) {
      var tempList = [];

      show = show === undefined ? 1 : show;

      angular.forEach(taskList, function (task) {
        if (task.parent_id) {
          return;
        }

        tempList.push(task);
      });

      return tempList;
    }
  })

/**
 * Select tasks which have matching of search query.
 *
 * Usage: doQuickSearch:query (string)
 */
  .filter('doQuickSearch', function () {
    return function (taskList, query) {
      if (query !== '') {
        var tempList = [];

        angular.forEach(taskList, function (task) {
          if (task.title.toLowerCase().indexOf(query.toLowerCase()) > -1) {
            //task.title = task.title.replace(query, '<span class="search-result">$&</span>');
            tempList.push(task);
          }
        });
        return tempList;
      }

      return taskList;
    }
  })

  .filter('typeaheadMember', function () {
    return function (list, query) {
      if (query !== '') {
        var tempList = []
          fullMemberName = '';

        angular.forEach(list, function (member) {
          fullMemberName = member.firstname + ' ' + member.lastname + ' (' + member.email + ')';
          if (fullMemberName.toLowerCase().indexOf(query.toLowerCase()) > -1) {
            tempList.push(member);
          }
        });
        if (tempList.length) {
          $('.dropdown-menu').dropdown('toggle');
        }
        return tempList;
      } else {
        if ($('#list-members').hasClass('open')) {
          $('#list-members').removeClass('open');
        }
      }

      return list;
    }
  });
