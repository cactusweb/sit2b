angular.module('FRZ.dashboard')
  .controller('LogCtrl', ['$scope', '$injector', 'logs', '$localStorage', 'LocalData', 'appConfig', '$http', function ($scope, $injector, logs, $localStorage, LocalData, appConfig, $http) {
    $scope.logs = logs;
    $scope.sortOrder = '-id';

    $scope.getLogDate = function(created){
      return moment(created).format('DD MMMM YYYY, HH:mm');
    };

    $scope.getLogText = function(log_text){
      var usernmae = $scope.$root.user.firstname + ' ' + $scope.$root.user.lastname;
      return log_text.replace(usernmae, 'Я');
    };

    /**
     * Div-Scroll
     * info: https://github.com/noraesae/perfect-scrollbar
     */
    $scope.perfectScrollbarInit = function() {
      var scrollConteiner = $("#notifications .popup_content");
      var contentHeight = $(scrollConteiner).height();
      var maxHeight = $(scrollConteiner).parent().parent().height() - 30;

      $(scrollConteiner).css('max-height', maxHeight);

      if(contentHeight > maxHeight && !$(scrollConteiner).hasClass('ps-container')){
        $(scrollConteiner).perfectScrollbar({
          wheelSpeed: 20,
          suppressScrollX: true,
          minScrollbarLength: 20
        });
      }
      if($(scrollConteiner).hasClass('ps-container')){
        $(scrollConteiner).perfectScrollbar('update');
      }
    };

    $scope.$on('onRepeatLast', function(scope, element, attrs){
      $http.post(appConfig.apiUrl+'/log/mark-as-read', { data: $scope.logs.all[0] }).
        success(function(data, status, headers, config) {
          LocalData.getAllLogs();
        }).
        error(function(data, status, headers, config) {
          alert(result.data.status + ': ' + result.data.message);
          console.log('error');
          console.log(result.data);
        });

      $localStorage.sit2b.logs.new.length = 0;

      $scope.perfectScrollbarInit();
      $(window).resize($scope.perfectScrollbarInit);
    });

    /**
     * Close modal.
     */
    $scope.close = function () {
      $scope.$close(true);
    };

  }]);
