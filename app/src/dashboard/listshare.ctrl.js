/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 23.02.15
 */

angular.module('FRZ.dashboard')
  .controller('ListShareCtrl', ['$scope', '$injector', 'listProps', function ($scope, $injector, listProps) {

    var $sessionStorage = $injector.get('$sessionStorage'),
      $localStorage = $injector.get('$localStorage'),
      LocalData = $injector.get('LocalData'),
      appConfig = $injector.get('appConfig'),
      ownerId = $localStorage.sit2b.listsAndTasks[listProps.id].user_id;

    $scope.apiHost = appConfig.apiHost;

    $scope.listProps = listProps;

    if (ownerId != $sessionStorage.user.id) {
      $scope.listOwner = LocalData.getMember(ownerId);
      if (!$scope.listOwner) {
        var UserModel = $injector.get('UserModel');

        $scope.listOwner = new UserModel.get({id: ownerId});
      }
    } else {
      $scope.listOwner = $sessionStorage.user;
    }

    $scope.form = {
      members: $localStorage.sit2b.listsAndTasks[listProps.id].members,
      allMembers: $localStorage.sit2b.allMembers,
      newMember: '',
      selectedUser: null,
      notify: 0
    };

    $scope.$watch('form.selectedUser', function (newVal, oldVal) {
      if (newVal) {
        $scope.form.newMember = newVal.firstname + ' ' + newVal.lastname + ' (' + newVal.email + ')';
      }
    });

    $scope.$watch('form.newMember', function (newVal, oldVal) {
      if (newVal && oldVal && newVal != oldVal) {
        $scope.form.selectedUser = null;
      }
    });

    /**
     *
     * @param newMember
     */
    $scope.form.addMember = function (newMember) {
      var LocalData = $injector.get('LocalData');

      if (!$scope.form.selectedUser) { // never used member
        var UserModel = $injector.get('UserModel');
        UserModel.findByEmail({email: newMember}, function (user) {
          $scope.form.selectedUser = user;
          LocalData.addListMember(listProps.id, $scope.form.selectedUser, true);
          $scope.form.newMember = '';
          $scope.form.selectedUser = null;
        }, function (data) {
          // @TODO not found
        });
      } else {
        LocalData.addListMember(listProps.id, $scope.form.selectedUser);
        $scope.form.newMember = '';
        $scope.form.selectedUser = null;
      }
    };


    /**
     * Div-Scroll
     * info: https://github.com/noraesae/perfect-scrollbar
     */
    $scope.form.memberlistScrollInit = function () {
      var perfectScrollbar = $("#list-members .perfectScrollbar");
      setTimeout(function () {
        if (!$(perfectScrollbar).hasClass('ps-container')) {
          $(perfectScrollbar).perfectScrollbar({
            wheelSpeed: 20,
            suppressScrollX: true,
            minScrollbarLength: 20
          });
        }
      }, 1);
    };

    /**
     *
     * @param id
     */
    $scope.form.deleteMember = function (id) {
      var LocalData = $injector.get('LocalData');

      LocalData.deleteListMember(listProps.id, id);
    };

    /**
     * Close modal.
     */
    $scope.form.close = function () {
      $scope.$close(true);
    };

    /**
     *
     * @param email
     * @returns {boolean}
     */
    $scope.form.validateEmail = function (email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    };

  }]);
