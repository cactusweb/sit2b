angular.module('FRZ.dashboard')
  .factory('ListModel', ['$resource', 'appConfig', function ($resource, appConfig) {
    return $resource(appConfig.apiUrl + '/tlist/:id/:action',
      {
        id: '@id',
        action: ''
      },
      {
        "executors": {
          method: 'GET',
          isArray: true,
          params: {action: 'executors'}
        },
        "members": {
          method: 'GET',
          isArray: true,
          params: {action: 'members'}
        },
        "allMembers": {
          method: 'GET',
          isArray: true,
          url: appConfig.apiUrl + '/tlist/all-members',
          params: {}
        },
        "addMember": {
          /**
           * params: {user_id: id}
           */
          method: 'POST',
          params: {action: 'member'}
        },
        "delMember": {
          method: 'DELETE',
          params: {action: 'member'}
        },

      });
  }]);