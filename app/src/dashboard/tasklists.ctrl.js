/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 20.01.15
 */

angular.module('FRZ.dashboard')
  .controller('TaskListsCtrl',
  ['$scope', 'TaskModel', 'newTaskModel', 'UserProfile', '$localStorage', 'LocalData', '$timeout', 'toastr', '$sessionStorage', '$injector',
    function ($scope, TaskModel, newTaskModel, UserProfile, $localStorage, LocalData, $timeout, toastr, $sessionStorage, $injector) {
      var StringUtils = $injector.get('StringUtils');

      $scope.zero = 0;
      $scope.listsAndTasks = $localStorage.sit2b.listsAndTasks;
      $scope.taskNotify = $localStorage.sit2b.taskNotify;

      $scope.$watch(function () {
        return Object.keys($scope.listsAndTasks).length;
      }, function () {
        $scope.listCount = Object.keys($scope.listsAndTasks).length;
      });

      $scope.prefs = UserProfile.getUserPreferences();

      $scope.sortPredicate = 'title';
      $scope.sortReverse = false;

      $scope.taskList = {};
      $scope.quickSearch = {};
      $scope.quickSearch.query = '';

      //$scope.showPrevButton = false;
      //$scope.showNextButton = false;

      /**
       * Close query input by Esc.
       * @param $event
       */
      $scope.quickSearch.closeByEsc = function ($event) {
        if ($event.keyCode == 27) {
          $('#top-title').show();
          $scope.quickSearch.query = '';
          $($event.target).val('').blur().parent().removeClass('active');
        }
      };

      /**
       * Close query input by clicking on the 'cross' icon.
       * @param $event
       */
      $scope.quickSearch.closeByCross = function ($event) {
        var icon = $($event.target);

        if (icon.prev().val() !== '')
          icon.prev().val('').focus();
        else {
          $('#top-title').show();
          $scope.quickSearch.query = '';
          icon.val('').blur().parent().removeClass('active');
        }
      };

      /**
       *
       * @param task
       * @returns {boolean|*}
       */
      $scope.inThePast = function (task) {
        return !task.done && moment(task.end_date).isBefore();
      };

      /**
       * Show date-time summary on the task card.
       * @param task
       * @returns {string}
       */
      $scope.showDateSummary = function (task) {
        var retval = '';

        retval += moment(task.start_date).format('H:mm');
        if (task.end_date) {
          retval += '-' + moment(task.end_date).format('H:mm');
        }
        retval += ', ';

        if (moment(task.start_date).isSame(moment(), 'day')) {
          retval += 'сегодня';
        } else {
          retval += moment(task.start_date).format('DD.MM.YYYY');
        }
        return retval;
      }

      /**
       * Task counter.
       * @param list
       * @returns {*}
       */
      $scope.taskList.countAll = function (list) {
        return Object.keys(list).length;
      };

      /**
       * Counter of completed tasks.
       * @param list
       * @returns {number}
       */
      $scope.taskList.countDone = function (list) {
        var i = 0;

        for (var task in list) {
          if (list[task].done) i++;
        }

        return i;
      };

      /**
       * Returns "true" if task has s "to do" list in description.
       * @param task
       * @returns {*}
       */
      $scope.hasToDoList = function (task) {
        return angular.isArray(task.description);
      };

      /**
       * Toggle 'done' state and save task changes.
       * @param task
       */
      $scope.toggleDoneState = function (task) {
        //$scope.$state.reload();
        TaskModel.update({id: task.id}, task,
          function (task) {
            //$scope.reloadState();
            console.log('Задача записана ', task);
          }, function (task) {
            console.log('Задача НЕ записана ', task.data);
            alert(result.data.status + ': ' + result.data.message);
          });
      };

      /**
       * Delete task by clicking the cross on the task card/
       * @param task
       */
      $scope.setTaskDeleted = function (task) {
        LocalData.removeTaskFromLocalList(task);
        TaskModel.delete({id: task.id}, task,
          function (result) {
            toastr.info('"' + task.title + '"', 'Задача удалена');

            $timeout(function () {
              $scope.perfectScrollbar();
            });

          }, function (task) {
            console.log('Задача НЕ записана ', task.data);
            toastr.error('Ошибка удаления задачи');
            alert(result.data.status + ': ' + result.data.message);
          });
      };

      /**
       * Closure for inversion text of placeholder between empty and start value.
       * @returns {Function}
       */
      $scope.invertPlaceholder = (function invert() {
        var p = '';

        return function (e) {
          if (e.target.placeholder) p = e.target.placeholder;

          switch (e.type) {
            case 'focus':
              e.target.placeholder = '';
              break;
            case 'blur':
              if (e.keyCode != 13) e.target.value = '';
              e.target.placeholder = p;
              break;
          }
        };
      })();


      /**
       * Fast create a new task via input on top of the list.
       * @param e
       * @param tlist_id
       */
      $scope.createNewTask = function (e, tlist_id) {
        var title = StringUtils.trim(e.target.value);

        if (e.keyCode == 13 && title) {
          var listProps = UserProfile.getListProperties(tlist_id),
            task = newTaskModel.create($scope.$root.user.id, false);

          task.title = title;
          task.tlist_id = tlist_id;
          task.duration = listProps.def_duration;
          e.target.value = '';

          TaskModel.save(task,
            function (task) {
              LocalData
                .prepareTask(task)
                .putTaskToLocalList(task);

              $timeout(function () {
                $scope.perfectScrollbar();
              }, 10);

              console.log('Задача записана ', task);
            }, function (response) {
              console.log('Задача НЕ записана ', response.data);
            });
        }
      };


      /**
       * Reload current state views.
       */
      $scope.reloadState = function () {
        $timeout(function () {
          $scope.$state.go('.', {}, {reload: true});
        }, 10);
      };

      /**
       * Div-Scroll
       * info: https://github.com/noraesae/perfect-scrollbar
       */
      $scope.perfectScrollbar = function () {
        var  lists_wrapper = $("#lists_wrapper");
        var  lists_container = $("#lists_container");
        var  scrolled_left =lists_wrapper.scrollLeft();
        var  size_diff = parseInt(lists_container.width() - lists_wrapper.width());
        if(lists_wrapper.width() < lists_container.width()){
          if(scrolled_left > 0){
            $timeout(function(){
              $scope.showPrevButton = true;
            });
          }else{
              $timeout(function(){
                $scope.showPrevButton = false;
              });
          }

          if(scrolled_left === size_diff){
            $timeout(function(){
              $scope.showNextButton = false;
            });
          }else{
              $timeout(function(){
                $scope.showNextButton = true;
              });
          }
        }else{
          $scope.showPrevButton = false;
          $scope.showNextButton = false;
        }

        //if (!$("#lists_wrapper").hasClass('ps-container')) {
        //  $("#lists_wrapper").perfectScrollbar({
        //    wheelSpeed: 20,
        //    //wheelPropagation: true,
        //    suppressScrollY: true,
        //    minScrollbarLength: 20
        //  });
        //} else {
        //  $("#lists_wrapper").perfectScrollbar('update');
        //}

        $(".list-tasks").each(function () {
          var listHeight = $(this).height();
          var listBodyHeight = $(this).find('.list-tasks-body').outerHeight();

          if (!$(this).hasClass('ps-container') && listBodyHeight > listHeight) {
            $(this).perfectScrollbar({
              wheelSpeed: 20,
              suppressScrollX: true,
              minScrollbarLength: 20
            });
          } else if ($(this).hasClass('ps-container')) {
            $(this).perfectScrollbar('update');
          }
        });
      };

      setTimeout(function () {
        $scope.perfectScrollbar();
        $(window).resize(function(){
          $scope.perfectScrollbar();
        });
        $("#lists_wrapper").scroll(function(){
          $scope.perfectScrollbar();
        });
      }, 1);

      $scope.$watch(function () {
        return $scope.$parent.showLists;
      }, function (n, o) {
        if (n !== o) {
          //console.log('watch: showLists');
          $sessionStorage.user.preferences.showLists = $scope.$parent.showLists;
          UserProfile.saveToServer();
          if (n) {
            $timeout(function () {
              $scope.perfectScrollbar();
            });
          }
        }
      });

      $scope.$watch(function () {
        return $scope.$parent.showCalendar;
      }, function (n, o) {
        if (n !== o) {
          $timeout(function () {
            $scope.perfectScrollbar();
          });
        }
      });

      $scope.NextList = function () {
        var container = $("#lists_wrapper");
        var scrolledLeft = $(container).scrollLeft();
        $(container).scrollLeft(scrolledLeft += 260);
      };

      $scope.PrevList = function () {
        var container = $("#lists_wrapper");
        var scrolledLeft = $(container).scrollLeft();
        $(container).scrollLeft(scrolledLeft -= 260);
      };

    }]);