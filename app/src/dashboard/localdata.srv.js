/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 22.01.15
 */

angular.module('FRZ')
  .service('LocalData',
  ['$injector', '$localStorage', '$q',
    function ($injector, $localStorage, $q) {

      var self = this;

      /**
       *
       * @param forceRewrite
       */
      this.initLocalData = function (forceRewrite) {
        // @TODO 'true' is for developing only
        if (forceRewrite === undefined) forceRewrite = true;
        if ($localStorage.sit2b && !forceRewrite) return;

        $localStorage.sit2b = {
          inboxListId: null,
          listsAndTasks: {},
          taskNotify: {}, // для красных точек
          allMembers: [],
          comments: {},
          logs: {}
        };
      };

      /**
       *
       * @param forceRewrite
       * @returns {Promise}
       */
      this.fetchAllData = function (forceRewrite) {
        // @TODO 'true' is for developing only
        if (forceRewrite === undefined) forceRewrite = true;

        return $q.all([
          this.updateLocalLists(forceRewrite),
          this.getAllMembers(),
          this.getAllListsProperties(),
          this.getAllComments(),
          this.getAllLogs(),
          this.getTrash()
        ]);
      };

      /**
       * Get from server information about list and task,
       * then transform the result object to convenient format
       * and write down to the local storage.
       */
      this.updateLocalLists = function (forceRewrite) {
        var ListModel = $injector.get('ListModel'),
          cache = $injector.get('superCache'),
          UserProfile = $injector.get('UserProfile'),
          self = this, // fucking crutch?
          deferred = $q.defer();

        // @TODO 'true' is for developing only
        if (forceRewrite === undefined) forceRewrite = true;
        if ($localStorage.sit2b.listsAndTasks && !forceRewrite) return null;

        ListModel.query(function (response) {
          var locLists = {},
            tempList;

          $localStorage.sit2b.listsAndTasks = locLists;

          angular.forEach(response, function (list) {
            tempList = {};
            self.prepareList(list);
            if (Number(list.private)) $localStorage.sit2b.inboxListId = list.id;

            angular.forEach(list.tasks, function (task) {
              self.prepareTask(task);
              tempList[task.id] = task;
            });

            list.tasks = tempList;

            if (!list.private) {
              ListModel.members({id: list.id}, function (members) {
                list.members = members;
              }, function (data) {
                list.members = [];
              });
            } else {
              list.members = [];
            }

            locLists[list.id] = list;
          });

          $localStorage.sit2b.listsAndTasks = locLists;
          deferred.resolve($localStorage.sit2b.listsAndTasks);
          //console.log($localStorage.sit2b);
        }, function (data) {// error block
          deferred.reject(data)
        });

        return deferred.promise;
      };

      /**
       * Find a task by ID in all the lists.
       * Return 'undefined' if not found.
       * @param id
       * @returns {*}
       */
      this.findTaskById = function (id) {
        var tlist_id, result;

        angular.forEach($localStorage.sit2b.listsAndTasks, function (list) {
          angular.forEach(list.tasks, function (task) {
            if (task.id == id) {
              tlist_id = list.id;
              return;
            }
          });
          if (tlist_id) return;
        });

        if (tlist_id) result = $localStorage.sit2b.listsAndTasks[tlist_id].tasks[id];
        return result;
      };

      /**
       *
       * @param id
       */
      this.doesListExists = function (id) {
        return $localStorage.sit2b.listsAndTasks.hasOwnProperty(id);
      };

      /**
       * Find a list id to which the task belongs to.
       * @param id
       * @returns {*}
       */
      this.findTaskListBelongsTo = function (id) {
        var tlist_id = null;

        angular.forEach($localStorage.sit2b.listsAndTasks, function (list) {
          angular.forEach(list.tasks, function (task) {
            if (task.id == id) {
              tlist_id = list.id;
              return;
            }
          });
          if (tlist_id) return;
        });

        return tlist_id;
      };

      /**
       * Convert some data types ;)
       * @param task
       * @returns {*}
       */
      this.prepareTask = function (task) {
        task.created = moment(task.created).toDate();
        task.deleted = Number(task.deleted);
        task.done = Number(task.done);
        task.duration = Number(task.duration);
        task.end_date = task.end_date ? moment(task.end_date).toDate() : '';
        task.entire_day = Number(task.entire_day);
        task.executor_id = Number(task.executor_id);
        task.id = Number(task.id);
        task.in_trash = Number(task.in_trash);
        task.parent_id = Number(task.parent_id);
        task.start_date = task.start_date ? moment(task.start_date).toDate() : '';
        task.tlist_id = Number(task.tlist_id);
        task.user_id = Number(task.user_id);
        task.updated = task.updated ? moment(task.updated).toDate() : task.updated;

        //console.log(task);
        task.description = JSON.parse(task.description);

        if(task.repeatRules){
          task.repeatRules.repeat_days = JSON.parse(task.repeatRules.repeat_days);
          task.repeatRules.exception = JSON.parse(task.repeatRules.exception);
        }

        var childs = {};

        if(task.childs){
          task.childs.forEach(function(child){
            self.prepareTask(child);
            childs[child.id] = child;
          });
        }

        task.childs = childs;

        return this;
      };

      /**
       * Convert some data types ;)
       * @param list
       */
      this.prepareList = function (list) {
        list.created = moment(list.created).toDate();
        list.deleted = Number(list.deleted);
        list.id = Number(list.id);
        list.in_trash = Number(list.in_trash);
        list.private = Number(list.private);
        list.updated = list.updated ? moment(list.updated).toDate() : list.updated;
        list.user_id = Number(list.user_id);
        list.members = [];
        return this;
      };

      /**
       * Put the task to appropriated local list.
       * @param task
       * @returns {*}
       */
      this.putTaskToLocalList = function (task) {
        $localStorage.sit2b.listsAndTasks[task.tlist_id].tasks[task.id] = task;

        return this;
      };

      /**
       * Remove the task from local list.
       * @param task
       * @returns {*}
       */
      this.removeTaskFromLocalList = function (task) {
        var tlist_id = this.findTaskListBelongsTo(task.id);
        console.log('Зачада из списка: ', tlist_id);
        if (tlist_id) delete $localStorage.sit2b.listsAndTasks[tlist_id].tasks[task.id];

        return this;
      };

      /**
       * Add task to the local list.
       * @param task
       * @returns {*}
       */
      this.addTaskToLocalList = function (task) {
        var tlist_id = task.tlist_id;
        if($localStorage.sit2b.listsAndTasks[tlist_id]){
          $localStorage.sit2b.listsAndTasks[tlist_id].tasks[task.id] = task;
        }else{
          console.log('addTaskToLocalList');
          console.log(task);
          console.log($localStorage.sit2b.listsAndTasks[tlist_id]);
        }

        return this;
      };

      /**
       *
       */
      this.getAllListsProperties = function () {
        var UserProfile = $injector.get('UserProfile'),
          deferred = $q.defer();

        angular.forEach($localStorage.sit2b.listsAndTasks, function (list) {
          list.properties = UserProfile.getListProperties(list.id);
        });

        deferred.resolve(true);

        return deferred.promise;
      };

      /**
       *
       * @param tlist_id
       * @returns {*}
       */
      this.getListExecutors = function (tlist_id) {
        var ListModel = $injector.get('ListModel'),
          deferred = $q.defer();

        if (tlist_id) {
          ListModel.executors({id: tlist_id}, function (executors) {
            deferred.resolve(executors);
          }, function (data) {
            deferred.reject(data)
          });
        }

        return deferred.promise;
      };

      /**
       *
       * @param tlist_id
       * @returns {*}
       */
      this.getListMembers = function (tlist_id) {
        var ListModel = $injector.get('ListModel'),
          deferred = $q.defer();

        if (tlist_id) {
          ListModel.members({id: tlist_id}, function (members) {
            deferred.resolve(members);
          }, function (data) {
            console.log(data);
            deferred.reject(data)
          });
        }

        return deferred.promise;
      };

      /**
       *
       * @param tlist_id
       * @param user_id
       */
      this.addListMember = function (tlist_id, user, neverUsed) {
        var ListModel = $injector.get('ListModel');

        if (!this.isListMember(tlist_id, user.id)) {
          ListModel.addMember({id: tlist_id, user_id: user.id});
          7
          $localStorage.sit2b.listsAndTasks[tlist_id].members.push(user);

          if (neverUsed) {
            $localStorage.sit2b.allMembers.push(user);
          }
        }
      };

      /**
       *
       * @param tlist_id
       * @param user_id
       */
      this.deleteListMember = function (tlist_id, user_id) {
        var ListModel = $injector.get('ListModel');

        ListModel.delMember({id: tlist_id, user_id: user_id}, function (data) {
          console.log(data);
          angular.forEach($localStorage.sit2b.listsAndTasks[tlist_id].members, function (member, key) {
            if (member.id == user_id) {
              delete $localStorage.sit2b.listsAndTasks[tlist_id].members[key];
              $localStorage.sit2b.listsAndTasks[tlist_id].members.length--;
            }
          });
        }, function (data) {
          console.log(data);
        });
      };

      /**
       *
       * @param tlist_id
       * @param user_id
       * @returns {boolean}
       */
      this.isListMember = function (tlist_id, user_id) {
        var result = false;

        angular.forEach($localStorage.sit2b.listsAndTasks[tlist_id].members, function (member, key) {
          if (member.id == user_id) {
            result = true;
          }
        });

        return result;
      };

      /**
       *
       * @returns {*}
       */
      this.getAllMembers = function () {
        var ListModel = $injector.get('ListModel'),
          deferred = $q.defer();

        ListModel.allMembers(function (data) {
          $localStorage.sit2b.allMembers = data;
          deferred.resolve(data);
        }, function (data) {
          console.log(data);
          deferred.reject(data);
        });

        return deferred.promise;
      };

      /**
       *
       * @param id
       * @returns {*}
       */
      this.getMember = function (id) {
        var result = null;

        angular.forEach($localStorage.sit2b.allMembers, function (member, key) {
          if (member.id == id) {
            result = member;
          }
        });

        return result;
      };

      /**
       *
       * @param list
       * @returns {*}
       */
      this.putLocalList = function (list) {
        this.prepareList(list);
        $localStorage.sit2b.listsAndTasks[list.id] = list;
        return this;
      };

      /**
       *
       * @param tlist_id
       * @returns {*}
       */
      this.removeLocalList = function (tlist_id) {
        delete $localStorage.sit2b.listsAndTasks[tlist_id];

        return this;
      };


      /**
       *
       */
      this.getAllComments = function () {
        var CommentModel = $injector.get('CommentModel'),
          deferred = $q.defer();

        CommentModel.get(function (data) {
          var newComments = [];

          angular.forEach(data, function (comment) {
            if (!comment.log.viewedLog) {
              newComments.push(comment);
            }
          });

          $localStorage.sit2b.comments = {
            'all': data,
            'new': newComments
          };

          deferred.resolve($localStorage.sit2b.comments);
        }, function (data) {
          console.log(data);
          deferred.reject(data);
        });

        return deferred.promise;
      };

      /**
       * add new comment to local task
       */
      this.addComment = function (task, comment) {
        $localStorage.sit2b.listsAndTasks[task.tlist_id].tasks[task.id].comments.push(comment);
      };

      /**
       *
       * @param logItem
       * @returns {*}
       */
      this.prepareLogItem = function (logItem) {
        logItem.comment_id = Number(logItem.comment_id);
        logItem.id = Number(logItem.id);
        logItem.list_id = Number(logItem.list_id);
        logItem.reminder_id = Number(logItem.reminder_id);
        logItem.task_id = Number(logItem.task_id);
        logItem.created = moment(logItem.created).toDate()

        return this;
      };

      /**
       *
       */
      this.getAllLogs = function () {
        var LogModel = $injector.get('LogModel'),
          deferred = $q.defer();

        LogModel.get(function (data) {
          var newLogs = [];
          self.dropTaskNotifications();

          angular.forEach(data, function (log) {
            self.prepareLogItem(log);
            if (!log.viewedLog) {
              newLogs.push(log);
              self.updateTaskNotifications(log);
            }
          });

          $localStorage.sit2b.logs = {
            'all': data,
            'new': newLogs
          };

          var $filter = $injector.get('$filter');
          console.log($filter('orderBy')($localStorage.sit2b.logs.all, '-id'))

          deferred.resolve($localStorage.sit2b.logs);
        }, function (data) {
          console.log(data);
          deferred.reject(data);
        });

        return deferred.promise;
      };

      /**
       *
       * @param id
       */
      this.findLogItemById = function (id, type) {
        var result;
        if (type != 'all' && type != 'new') type = 'all';

        angular.forEach($localStorage.sit2b.logs[type], function (logItem) {
            if (logItem.id == id) {
              result = logItem;
              return;
            }
        });

        return result;
      };

      /**
       *
       * @param logItem
       */
      this.addLogItem = function (logItem) {
        if (!this.findLogItemById(logItem.id)) {
          $localStorage.sit2b.logs.all.push(logItem);
        }
      };

      this.addLogItemAsUnread = function (logItem) {
        if (!this.findLogItemById(logItem.id, 'new')) {
          $localStorage.sit2b.logs.new.push(logItem);
        }
      };

      /**
       *
       * @param logItem
       */
      this.updateTaskNotifications = function (logItem) {
        if (logItem.task_id) {
          if (!$localStorage.sit2b.taskNotify.hasOwnProperty(logItem.task_id)) {
            $localStorage.sit2b.taskNotify[logItem.task_id] = {};
          }
          $localStorage.sit2b.taskNotify[logItem.task_id][logItem.type] = true;

          console.log($localStorage.sit2b.taskNotify);
        }
      };

      this.dropTaskNotifications = function () {
        //$localStorage.sit2b.taskNotify = {};

        angular.forEach($localStorage.sit2b.taskNotify, function (task) {
          for (var prop in task) {
            task[prop] = false;
          }
        });
        console.log($localStorage.sit2b.taskNotify);
      };

      /**
       *
       */
      this.getTrash = function () {
        var self = this;
        var TaskModel = $injector.get('TaskModel'),
          deferred = $q.defer();

        TaskModel.get({trash: true},function (data) {
          if(angular.isArray(data)){
            data.forEach(function(task){
              self.prepareTask(task);
            });
          }
          $localStorage.sit2b.trash = data;
          deferred.resolve($localStorage.sit2b.trash);
        }, function (data) {
          console.log(data);
          deferred.reject(data);
        });

        return deferred.promise;
      };


    }]);