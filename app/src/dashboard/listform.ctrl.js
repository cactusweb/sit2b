/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 06.02.15
 */

angular.module('FRZ.dashboard')
  .controller('ListFormCtrl', [
    '$scope', '$modal', '$injector', 'listProps', 'executors', 'members', 'UserProfile', 'toastr',
    function ($scope, $modal, $injector, listProps, executors, members, UserProfile, toastr) {
      var $localStorage = $injector.get('$localStorage');
      var LocalData = $injector.get('LocalData');

      $scope.props = listProps;
      $scope.list = $localStorage.sit2b.listsAndTasks[$scope.props.id];

      $scope.form = {};
      $scope.form.executors = [{
        value: 0,
        label: 'все исполнители',
        position: 0
      }];

      $scope.members = members;

      $scope.$watch('props.own_title', function (newVal, oldVal) {
        //if (newVal == '') {
        //  $scope.props.own_title = $scope.props.title;
        //}
      });

      executors.$promise.then(function (data) {
        angular.forEach(data, function (val, key) {
          $scope.form.executors.push({
            value: Number(val.id),
            label: val.firstname + ' ' + val.lastname + ' (' + val.email + ')',
            position: 0
          });
        });
      });

      $scope.form.ordering = [
        {
          value: 'created',
          label: '&darr; По дате создания задачи',
          position: 0
        },
        {
          value: '-created',
          label: '&uarr; По дате создания задачи',
          position: 1
        },
        {
          value: 'start_date',
          label: '&darr; По дате в календре',
          position: 2
        },
        {
          value: '-start_date',
          label: '&uarr; По дате в календре',
          position: 3
        },
        {
          value: 'priority',
          label: '&darr; По приоритету задачи',
          position: 4
        },
        {
          value: '-priority',
          label: '&uarr; По приоритету задачи',
          position: 5
        },
        {
          value: 'updated',
          label: '&darr; По дате изменения задачи',
          position: 6
        },
        {
          value: '-updated',
          label: '&uarr; По дате изменения задачи',
          position: 7
        }
      ];

      $scope.form.durations = [
        {
          value: 15,
          label: '15 минут',
          position: 0
        },
        {
          value: 30,
          label: '30 минут',
          position: 0
        },
        {
          value: 60,
          label: '60 минут',
          position: 0
        },
        {
          value: 90,
          label: '90 минут',
          position: 0
        },
        {
          value: 120,
          label: '120 минут',
          position: 0
        },
      ];

      $scope.form.colors = [
        'fdbebe', 'ffccb3', 'ffdd99', 'ffffa6', 'e6ffb3', 'ccffb3', 'bcfad1', 'bcf6fa', 'bce5fa', 'c3d1fa', 'fac3f1',
        'e8c3fa', 'd1c3fa', 'e6cfcf', 'e6decf', 'cfe6d7', 'dee6cf', 'cfe4e6', 'cfd5e6', 'e7dce6', 'decfe6', 'f2f2f2',
        'e6e6e6', 'cccccc'
      ];

      /**
       * Creating new list.
       */
      $scope.form.createNewList = function () {
        var ListModel = $injector.get('ListModel'),
          LocalData = $injector.get('LocalData');

        ListModel.save({
          title: $scope.props.title
        }, function (newList) {
          $scope.props.own_title = $scope.props.title;
          $scope.props.id = newList.id;
          LocalData.putLocalList(newList);
          UserProfile.setListProperties(newList.id, $scope.props);
          $scope.$close(true);
        }, function (result) {
          alert(result.data.status + ': ' + result.data.message);
        });
      };

      /**
       * Deleting list by id.
       */
      $scope.form.deleteList = function () {
        var ConfirmDialog = $injector.get('ConfirmDialog'),
          ListModel = $injector.get('ListModel'),
          LocalData = $injector.get('LocalData'),
          parent = $('div.popup.list_settings'),
          sure = ConfirmDialog.open('Вы хотите удалить список "' + $scope.props.own_title + '" и все задачи в нём?', parent);

        sure.then(function (res) {
          ListModel.delete({
            id: $scope.props.id
          }, function () {
            UserProfile.deleteListProperties($scope.props.id);
            LocalData.removeLocalList($scope.props.id);
            $scope.$close(true);
            toastr.info('"' + $scope.props.own_title + '"', 'Список удален');
          }, function (result) {
            alert(result.data.status + ': ' + result.data.message);
            toastr.error('Ошибка удаления списка');
          });
        });
      };

      /**
       *
       * @param $event
       */
      $scope.form.setBlurOnEnter = function ($event) {
        if ($event.keyCode == 13) {
          $($event.target).blur();
        }
      };

      /**
       * Restore default settings.
       * @param id
       */
      $scope.form.revert = function (id) {
        UserProfile.revertToDefaultProperties(id);
        $scope.props = UserProfile.getListProperties(id);
        console.log($scope.props);
        toastr.info('Восстановлены настройки списка по умолчанию');
      };

      /**
       * Close modal.
       */
      $scope.form.close = function () {
        $scope.$close('closed');
      };

      /**
       * Auto saving for edited properties.
       */
      if ($scope.props.id) {
        $scope.$watch('props', function (newVal, oldVal) {
          UserProfile.saveToServer();
        }, true);
      }

    }]);