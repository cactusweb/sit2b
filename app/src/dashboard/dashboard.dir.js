/**
 * frzDashboard
 *
 * Description
 */
angular.module('FRZ.dashboard')

  .directive('frzCheckbox', function () {
    /**
     * This directive serves the checkbox which styled by Twitter Bootstrap.
     * If state values of ngModel is not a boolean, you need to specify it
     * via attributes 'frz-true' and 'frz-false' (only integers are assumed).
     *
     * Sample:
     * <div data-toggle="buttons" class="checkbox strong"
     *     frz-checkbox frz-true="1" frz-false="0" ng-model="obj.cbValue">
     */
    return {
      restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
      require: 'ngModel',
      link: function ($scope, element, attrs, ngModel) {
        var label = element.find('label')[0];

        function getTrueValue() {
          return getCheckboxValue(attrs.frzTrue, true);
        }

        function getFalseValue() {
          return getCheckboxValue(attrs.frzFalse, false);
        }

        function getCheckboxValue(attributeValue, defaultValue) {
          var val = $scope.$eval(attributeValue);
          return angular.isDefined(val) ? parseInt(val) : defaultValue;
        }

        $scope.$watch(function () {
          return ngModel.$modelValue;
        }, function () {
          //console.log('model changed: '+attrs.ngModel);
          $(element).find('input').prop('checked', angular.equals(ngModel.$viewValue, getTrueValue()));
          angular.element(label).toggleClass('active', angular.equals(ngModel.$viewValue, getTrueValue()));
        }, true);

        $(element).find('input').bind('change', function () {
          //console.log('input changed');
          if ($(this).is(':checked')) {
            ngModel.$setViewValue(getTrueValue());
          } else {
            ngModel.$setViewValue(getFalseValue());
          }
        });
      }
    };
  })

  .directive('frzTaskDescriptionType', function () {
    return {
      restricted: 'A',
      require: 'ngModel',
      link: function ($scope, element, attrs, ngModel) {

        function showDescription() {
          $(element).find('[data-descType="text"]').toggleClass('active');
          $(element).parent().find("div.task_description").toggleClass('active');
        }

        function showList() {
          $(element).find('[data-descType="list"]').toggleClass('active');
          $(element).parent().find("div.task_description_list").toggleClass('active');
        }

        $scope.$watch(ngModel.$viewValue, function () {
          if (angular.isArray(ngModel.$modelValue)) {
            showList();
          } else {
            showDescription();
          }
        });

        // descr transformation
        $(element).find('a.toggle').bind('click', function () {
          if (!$(this).hasClass('active')) {
            $(element).find('a.toggle').toggleClass('active');
            var descType = $(this).attr("data-descType");
            $(element).parent().find("div.toggle_data").toggleClass('active');
            if (descType == 'text') {
              var outputText = '';
              var lineCounter = 1;
              $scope.form.showDescriptionAsList = false;
              $.each(ngModel.$modelValue, function (itemkey, itemVal) {
                outputText += itemVal["subject"];
                if (lineCounter != ngModel.$modelValue.length) {
                  outputText += '\n';
                }
                lineCounter++;
              });

              var textHeight = ngModel.$modelValue.length * 16 + 22;
              $(element).parent().find('textarea').css({
                height: textHeight,
                overflow: 'hidden'
              });

              ngModel.$setViewValue(outputText);
            } else {
              var outputPreList = ngModel.$modelValue.split("\n");
              var outputList = [];
              $scope.form.showDescriptionAsList = true;
              $.each(outputPreList, function (index, value) {
                if (value != '') {
                  outputList.push({
                    subject: value,
                    done: false
                  });
                }
              });
              ngModel.$setViewValue(outputList);
            }
          }
          return false;
        });

      }
    }
  })
  .directive('frzTextareaAutoheight', function () {
    return {
      restricted: 'A',
      require: 'ngModel',
      link: function ($scope, element, attrs, ngModel) {

        var containsText = function (value) {
          return (value.replace(/\s/g, '').length > 0);
        };

        var diff = parseInt($(element).css('paddingBottom')) +
          parseInt($(element).css('paddingTop'));

        if (containsText($(element).val())) {
          $(element).height($(element).scrollHeight - diff);
        }

        // keyup is required for IE to properly reset height when deleting text
        $(element).on('input keyup', function (event) {
          var $window = $(window);
          var currentScrollPosition = $window.scrollTop();

          $(this)
            .height(0)
            .height(this.scrollHeight - diff);

          $window.scrollTop(currentScrollPosition);
          $scope.perfectScrollbarInit();
        });

      }
    }
  })
  .directive('frzTaskDescriptionList', function () {
    return {
      restricted: 'A',
      require: 'ngModel',
      link: function ($scope, element, attrs, ngModel) {

        $(element).on("click", "a.delete", function () {
          var key = $(this).attr('frz-item-key');
          ngModel.$modelValue.splice(key, 1);
        });

        $(element).find("input.frzTaskDescriptionListAdd").bind("keydown keypress", function (event) {
          if (event.which === 13) {
            var newItem = $.trim($(this).val());
            if(newItem === ''){
              event.preventDefault();
              return;
            }
            ngModel.$modelValue.push({
              subject: newItem,
              done: false
            });
            ngModel.$setViewValue(ngModel.$modelValue);

            $(this).val("");

            event.preventDefault();
          }
        });

        $(element).on("change", "input[type=checkbox]", function () {
          if ($(this).is(':checked')) {
            ngModel.$modelValue[$(this).attr('frz-item-key')]["done"] = 1;
          } else {
            ngModel.$modelValue[$(this).attr('frz-item-key')]["done"] = 0;
          }

          console.log(ngModel.$modelValue[$(this).attr('frz-item-key')]);
        });

      }
    }
  })
  .directive('frzTaskReminder', function () {
    return {
      restricted: 'A',
      templateUrl: 'src/dashboard/partials/taskreminder.dir.html',
      require: 'ngModel',
      link: function ($scope, element, attrs, ngModel) {
        $(element).on("click", "a.delete", function () {
          var reminderKey = $(this).attr('data-key');
          ngModel.$modelValue.splice(reminderKey, 1);
          ngModel.$setViewValue(ngModel.$modelValue);
        });
      }
    }
  })
/**
 * Directive for bootstrap dropdown list (select)
 *
 *  example:
 * <dropdown
 *    ng-model="reminder.type"
 *    options="[
 *        {
 *            value: 'system',
 *            label: 'Внутри системы',
 *            position: 0
 *        },
 *        {
 *            value: 'email',
 *            label: 'На E-mail',
 *            position: 1
 *        }
 *    ]"
 *  >
 * </dropdown>
 */
  .directive('dropdown', function () {
    return {
      restricted: 'E',
      require: 'ngModel',
      templateUrl: 'src/dashboard/partials/dropdown.dir.html',
      replace: true,
      scope: {
        options: "="
      },
      link: function ($scope, element, attrs, ngModel) {
        $scope.optionsObj = {};

        $scope.$watch('options', function (options) {
          $scope.updateOptionsList(options);
          $scope.updateTitle();
        }, true);

        $scope.$watch(function () {
          return ngModel.$modelValue;
        }, function (modelValue) {
          $scope.updateTitle(modelValue);
        });

        $scope.updateOptionsList = function (options) {
          $scope.optionsObj = {};
          $.each(options, function (key, option) {
            $scope.optionsObj[option.value] = option.label;
          });
        };

        $scope.updateTitle = function (modelValue) {
          if (modelValue) {
            $scope.current_value = modelValue;
            $scope.current_label = $scope.optionsObj[modelValue];
          } else {
            if ($.isEmptyObject($scope.optionsObj)) {
              $scope.current_value = '';
              $scope.current_label = '';
            } else {
              $.each($scope.optionsObj, function (key, val) {
                $scope.current_value = key;
                $scope.current_label = val;
                return false;
              });
            }
          }
        };

        var dropdownValue = $(element).find('.dropdown_value');
        $(element).find('ul.dropdown-menu').on('click', 'li', function () {
          var optionVal = $(this).attr("data-value");
          var optionLabel = $(this).html();
          $(dropdownValue)
            .attr("data-value", optionVal)
            .html(optionLabel);
          ngModel.$setViewValue(optionVal);
        });
      }
    }
  })
/**
 * Task duration object (attribute 'duration') must have such structure
 *   $scope.form.duration = {
 *     hours:   { val: 1, viewVal: 1, text: ' час.' },
 *     minutes: { val: 1, viewVal: 1, text: ' мин.' }
 *   }
 */
  .directive('frzTaskDuration', function () {
    return {
      restricted: 'E',
      template: ' \
        <div class="clear"> \
          <input type="text" name="hours" ng-model="duration.hours.viewVal" \
            ng-init="duration.hours.viewVal = duration.hours.val + duration.hours.text" \
            ng-blur="addText($event)" ng-focus="removeText($event)" ng-keyup="removeText($event)"/> \
          <input type="text" name="minutes" ng-model="duration.minutes.viewVal" \
            ng-init="duration.minutes.viewVal = duration.minutes.val + duration.minutes.text" \
            ng-blur="addText($event)" ng-focus="removeText($event)" ng-keyup="removeText($event)" /> \
        </div>',
      scope: {
        duration: '='
      },
      replace: true,
      link: function ($scope, element, attrs, ngModel) {
        // item.onblur
        $scope.addText = function (event) {
          var name = event.target.name;
          switch (name) {
            case 'hours':
              $scope.duration.hours.val = parseInt($scope.duration.hours.viewVal);
              $scope.duration.hours.viewVal =
                $scope.duration.hours.viewVal + $scope.duration.hours.text;
              break;
            case 'minutes':
              $scope.duration.minutes.val = parseInt($scope.duration.minutes.viewVal);
              $scope.duration.minutes.viewVal =
                $scope.duration.minutes.viewVal + $scope.duration.minutes.text;
              break;
          }
        };

        // item.onfocus
        $scope.removeText = function (event) {
          var name = event.target.name,
            val;
          switch (name) {
            case 'hours':
              val = parseInt($scope.duration.hours.viewVal);
              $scope.duration.hours.viewVal = isNaN(val) ? 0 : val;

              break;
            case 'minutes':
              val = parseInt($scope.duration.minutes.viewVal);
              $scope.duration.minutes.viewVal = isNaN(val) ? 0 : val;
              break;
          }
        };
      }
    }
  })

/**
 *
 */
  .directive('frzTaskDurationNew', ['$timeout', function ($timeout) {
    return {
      restricted: 'E',
      templateUrl: 'src/dashboard/partials/duration.dir.html',
      require: 'ngModel',
      replace: true,
      link: function ($scope, element, attrs, ngModel) {
        $scope.duration = {};

        $timeout(function () {
          $scope.duration.hours = Math.floor(ngModel.$modelValue / 60);
          $scope.duration.minutes = ngModel.$modelValue % 60;
        }).then(function () {
        });

        $scope.$watch('duration', function (n, o) {
          var total = 0;
          if (n.hours != o.hours || n.minutes != o.minutes) {

            n.hours = parseInt(n.hours);
            n.minutes = parseInt(n.minutes);

            if (!isNaN(n.hours)) total += n.hours * 60;
            if (!isNaN(n.minutes)) total += n.minutes;

            ngModel.$setViewValue(total);
            ngModel.$commitViewValue();

            // this is a crutch!!!
            if($scope.task.start_date instanceof Date){
              var end_date = new Date($scope.task.start_date.getTime() + (total * 60000));
              $scope.task.end_date = end_date;
            }
            //$scope.task.end_date = moment(end_date).format('DD.MM.YYYY');
          }
        }, true);

        $scope.parseDuration = function () {
          $scope.duration.hours = Math.floor(ngModel.$modelValue / 60);
          $scope.duration.minutes = ngModel.$modelValue % 60;
        };

        ngModel.$render = function () {
          $scope.parseDuration();
        };
      }
    }
  }])

  .directive('onLastRepeat', function () {
    return function (scope, element, attrs) {
      if (scope.$last) setTimeout(function () {
        scope.$emit('onRepeatLast', element, attrs);
      }, 1);
    };
  })
;

