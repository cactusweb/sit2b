/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 16.01.15
 */

angular.module('FRZ.dashboard')
  .factory('TaskModel', ['$resource', 'appConfig', function ($resource, appConfig) {
    return $resource(appConfig.apiUrl + '/task/:id',
      {id: '@id'}, {
        'update': {method: 'PUT'},
        'get': {isArray: true},
        'restore': {
          method: 'POST',
          responseType: 'JSON',
          url: appConfig.apiUrl + '/task/:id/restore',
          params: {}
          //isArray: true
        }
      }
    );
  }])

  .factory('newTaskModel', [function () {
    return {
      create: function (id, setDate) {
        var now = moment.tz(new Date(), 'Europe/Kiev'),
          duration = 15,
          start_date = null,
          end_date = null;

        setDate = setDate !== undefined ? setDate : true;

        if (setDate) {
          start_date = now.toDate();
          end_date = now.add(duration, 'minutes').toDate();
        }

        return {
          id: null,
          parent_id: null,
          user_id: id,
          executor_id: id,
          executor: null, // virtual field
          title: "",
          location: "",
          description: "",
          priority: "d",
          deleted: 0,
          start_date: start_date,
          end_date: end_date,
          duration: duration,
          created: null,
          updated: null,
          entire_day: 0,
          done: 0,
          tlist_id: null,
          comments: [],
          files: [],
          logs: [],
          reminders: [],
          repeatRules: null,
          parentRules: null,
          parent_rules_id: null
        }
      }
    }
  }]);
