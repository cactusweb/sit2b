
angular.module('FRZ.dashboard')
  .controller('CalendarCtrl',
  ['$scope', '$compile', '$localStorage', 'UserProfile', '$state', 'TaskModel', '$timeout', '$sessionStorage', 'LocalData',
    function ($scope, $compile, $localStorage, UserProfile, $state, TaskModel, $timeout, $sessionStorage, LocalData) {

      $scope.preferences = $sessionStorage.user.preferences;
      $scope.listsExclude = {};
      $scope.tasks = [];

      $scope.getCalendarHeight = function(){
        return $('.work-bar').height()-200;
      };

      $scope.setCalendarHeight = function(){
        if($scope.sit2bCalendar){
          $scope.sit2bCalendar.fullCalendar('option', 'height', $scope.getCalendarHeight());
        }
      };

      $scope.toggleListsExclude = function(list){
        if(!$scope.listsExclude[list.id]){
          $scope.listsExclude[list.id] = list;
        }else{
          delete $scope.listsExclude[list.id];
        }
      };

      $scope.checkListsExclude = function(list){
        return !!$scope.listsExclude[list.id];
      };

      $scope.fetchCalendarTasks = function(){
        angular.forEach($localStorage.sit2b.listsAndTasks, function(list){
            angular.forEach(list.tasks, function(task){

              if(!$scope.listsExclude[task.tlist_id] && !task.parent_id){
                var allDay = false;
                if(task.entire_day == 1){
                  allDay = true;
                }



                if(task.repeatRules){

                  //repeatRule = {
                  //  end_date: null,
                  //  id: "2",
                  //  interval: null,
                  //  limit: null,
                  //  period: "day",
                  //  repeat_days: "0",
                  //  start_date: "2015-04-09 12:00:00"
                  //}

                  task.repeatRules.interval = Number(task.repeatRules.interval);
                  if(!task.repeatRules.interval){
                    task.repeatRules.interval = 1;
                  }

                  switch(task.repeatRules.period){
                    case 'day':
                      var step = 1;
                      var period = 'days';
                      break;
                    case 'week':
                      var step = 7;
                      var period = 'weeks';
                      break;
                    case 'month':
                      var step = 30;
                      var period = 'months';
                      break;
                    case 'year':
                      var step = 360;
                      var period = 'years';
                      break;
                  }

                  var incr = 0;
                  var counter = 0;

                  for(var i = 0; i<360; i+=step){

                    if(period != 'weeks' && task.repeatRules.exception && task.repeatRules.exception[incr+'_'+period]){
                      incr += task.repeatRules.interval;
                      continue;
                    }

                    if(i === 0 || moment(task.start_date).add(incr, period).isAfter(task.repeatRules.start_date) || period === 'weeks'){

                      if(task.repeatRules.limit && task.repeatRules.limit < counter){
                        break;
                      }
                      if(task.repeatRules.end_date && moment(task.start_date).add(incr, period).isAfter(task.repeatRules.end_date)){
                        break;
                      }

                      if(period === 'weeks'){

                        var start_date = moment(task.start_date).day(0);
                        var end_date = moment(task.end_date).day(0);

                        if(incr != 0){
                          start_date = moment(task.start_date).add(incr, period);
                          end_date = moment(task.end_date).add(incr, period);
                        }

                        for(var x = 0; x<7; x++){
                          if(task.repeatRules.repeat_days[moment(start_date).add(x, 'days').day()] === 1){

                            var exceptionKey = moment(start_date).add(x, 'days').diff(moment(task.start_date), 'days');
                            if(task.repeatRules.exception && task.repeatRules.exception[exceptionKey+'_days']){
                              continue;
                            }

                            $scope.tasks.push({
                              id: task.id,
                              parent_id: task.parent_id,
                              list_id: task.tlist_id,
                              title: task.title,
                              start: moment(start_date).add(x, 'days'),
                              end: moment(end_date).add(x, 'days'),
                              allDay: allDay,
                              color: $scope.preferences.lists[task.tlist_id].color,
                              incr: exceptionKey,
                              period: 'days',
                              done: task.done,
                              url: $state.href('dashboard.start.task-inst', {id: task.id})+'?incr='+exceptionKey+'&period=days'
                            });

                            counter++;
                          }
                        }

                      }else{
                        $scope.tasks.push({
                          id: task.id,
                          parent_id: task.parent_id,
                          list_id: task.tlist_id,
                          title: task.title,
                          start: moment(task.start_date).add(incr, period),
                          end: moment(task.end_date).add(incr, period),
                          allDay: allDay,
                          color: $scope.preferences.lists[task.tlist_id].color,
                          incr: incr,
                          period: period,
                          done: task.done,
                          url: $state.href('dashboard.start.task-inst', {id: task.id})+'?incr='+incr+'&period='+period
                        });
                      }

                      counter++;
                    }

                    incr += task.repeatRules.interval;
                  }

                  if(task.childs){

                    angular.forEach(task.childs, function(child){
                      var childAllDay = false;
                      if(child.entire_day == 1){
                        childAllDay = true;
                      }
                      $scope.tasks.push({
                        id: child.id,
                        parent_id: child.parent_id,
                        list_id: child.tlist_id,
                        title: child.title,
                        start: moment(child.start_date),
                        end: moment(child.end_date),
                        allDay: childAllDay,
                        color: $scope.preferences.lists[child.tlist_id].color,
                        done: child.done,
                        url: $state.href('dashboard.start.task', {id: child.id})
                      });
                    });
                  }

                }else{
                  $scope.tasks.push({
                    id: task.id,
                    parent_id: task.parent_id,
                    list_id: task.tlist_id,
                    title: task.title,
                    start: moment(task.start_date),
                    end: moment(task.end_date),
                    allDay: allDay,
                    color: $scope.preferences.lists[task.tlist_id].color,
                    done: task.done,
                    url: $state.href('dashboard.start.task', {id: task.id})
                  });
                }
              }
            });
          });
      };

      $scope.getCalendarTasks = function(){
        //console.log('getCalendarTasks');
        $scope.fetchCalendarTasks();
      };

      $scope.updateCalendarTasks = function(){
        //console.log('updateCalendarTasks');
        $timeout(function(){
          $scope.tasks.length = 0;
          $scope.fetchCalendarTasks();
          if($scope.tasks.length){
            $scope.sit2bCalendar.fullCalendar('destroy');
          }
        });
      };

      /* watches start*/
      $scope.$watch('listsExclude', function(n, o){
        if(n !== o){
          //console.log('watch: listsExclude');
          $scope.updateCalendarTasks();
        }
      }, true);

      $scope.$watch(function(){
        return $localStorage.sit2b.listsAndTasks;
      },function(n, o){
        if(n !== o){
          //console.log('watch: listsAndTasks');
          $scope.updateCalendarTasks();
          $scope.perfectScrollbar();
        }
      }, true);

      $scope.$watch(function(){
        return $scope.$parent.showCalendar;
      },function(n, o){
        if(n !== o){
          //console.log('watch: showCalendar');
          $sessionStorage.user.preferences.showCalendar = $scope.$parent.showCalendar;
          UserProfile.saveToServer();
          if(n && $scope.sit2bCalendar) {
            $timeout(function () {
              $scope.updateCalendarTasks();
              $scope.setTimeline();
            });
          }
        }
      });

      $scope.$watch(function(){
        return $scope.$parent.showLists;
      },function(n, o){
        if(n !== o && n){
          //$scope.sit2bCalendar.fullCalendar('render');
          //console.log('watch: showLists');
          $timeout(function(){
            $scope.setTimeline();
          }, 10);
        }
      });
      /* watches end */

      /**
       * Set red timeline on the calendar
       */
      $scope.setTimeline = function (){
        //console.log('setTimeline');

        if($(".calendar .timeline").length == 0){
          $(".fc-time-grid-container").prepend("<div style='width:100%; max-width: 100%; overflow: visible;  position: absolute; z-index: 223;'><span class='timeline-marker'></span><hr class='timeline'/></div>")
        }
        var timeline = $(".calendar .timeline");
        var timeline_marker = $(".calendar .timeline-marker");

        if($(".fc-today").length <= 0){
          timeline.hide();
          return;
        }
        else{
          timeline.show();
        }

        var now = moment();
        var day = parseInt(now.format("e"));
        var width =  $(".fc-today").outerWidth();
        var height =  42;
        var left = (day*width) + jQuery(".fc-axis").outerWidth();
        var top = (now.hours()*60 + now.minutes())/60;

        top = height*top;

        if($('.fc-view').hasClass('fc-agendaDay-view')){
          left = $(".fc-axis.fc-time").outerWidth();
        }

        timeline
          .css('width',width+"px")
          .css('left',left+"px")
          .css('top',top+"px");

        timeline_marker.css('top',top+"px");
      };

      /**
       * Toggle 'done' state and save task changes.
       * @param task_id
       * @param list_id
       */
      $scope.toggleDoneState = function (task_id, list_id, incr, period) {
        var task = $localStorage.sit2b.listsAndTasks[list_id].tasks[task_id];
        //console.log(incr, period);
        if(incr != 'undefined' && period != 'undefined'){
          var taskNew = angular.copy(task);
          if(task.done){
            taskNew.done = 0
          }else{
            taskNew.done = 1;
          }
          taskNew.parent_id = task.id;
          taskNew.incr = incr;
          taskNew.period = period;
          taskNew.id = null;
          taskNew.start_date =  moment(task.start_date).add(incr, period).toDate();
          taskNew.end_date =  moment(task.end_date).add(incr, period).toDate();
          taskNew.repeatRules = null;
          taskNew.comments = [];
          taskNew.files = [];
          taskNew.logs = [];
          taskNew.reminders = [];

          TaskModel.save(taskNew,
            function (task) {
              $localStorage.sit2b.listsAndTasks[task.tlist_id].tasks[task.parent_id].repeatRules.exception[task.repeat_exception_code] = 1;
              LocalData
                .prepareTask(task)
                .putTaskToLocalList(task);
              $localStorage.sit2b.listsAndTasks[task.tlist_id].tasks[task.parent_id].childs[task.id] = task;
              console.log('Задача записана ', task, $localStorage.sit2b.listsAndTasks);
            }, function (result) {
              console.log('Задача НЕ записана ', result.data);
              alert(result.data.status + ': ' + result.data.message);
            });
        }else{
          if(task.done){
            task.done = 0
          }else{
            task.done = 1;
          }
          TaskModel.update({id: task.id}, task,
            function (task) {
              //success
            }, function (task) {
              if(task.done){
                task.done = 1
              }else{
                task.done = 0;
              }
              console.log('Задача НЕ записана ', task.data);
              alert(result.data.status + ': ' + result.data.message);
            });
        }
      };

      /**
       * set calendar's grid height (fc-time-grid-container class)
       */
      $scope.calendarGridHeight = function(){
        //console.log('calendarGridHeight');
        var gridHeight = $('#calendar').height();
        gridHeight -= 200;// calendar's top (calendar_top class)
        gridHeight -= 50;// calendar's toolbar (fc-toolbar class)
        gridHeight -= 31;// calendar's head (fc-head class)
        gridHeight -= 4;// separation line (fc-divider, fc-widget-header classes)

        if($('.fc-time-grid-container').length){
          gridHeight -= $('.fc-day-grid').height();// fullday tasks
          $(".fc-time-grid-container").height(gridHeight);
        }

        //if view = month
        if($('.fc-day-grid-container').length){
          $('.fc-day-grid .fc-row.fc-week').height($('.fc-day-grid .fc-row.fc-week .fc-day').width());
          var dayGridHeight = $('.fc-day-grid').height();
          gridHeight += 4;

          if(dayGridHeight > gridHeight){
            $(".fc-day-grid-container").addClass('fc-scroller');
            var srollbarWidth = $(".fc-day-grid-container").innerWidth() - $('.fc-day-grid').innerWidth(); //srollbar width
            $('.fc-row.fc-widget-header').css({
              borderRightWidth: '1px',
              marginRight: srollbarWidth
            });
          }else if($(".fc-day-grid-container").hasClass('fc-scroller')){
            $(".fc-day-grid-container").removeClass('fc-scroller');
            $('.fc-row.fc-widget-header').css({
              borderRightWidth: 0,
              marginRight: 0
            });
          }
          $(".fc-day-grid-container").height(gridHeight);
        }
      };

      $scope.getCalendarTasks();
      $scope.eventSources = [$scope.tasks];

      /**
       * CONFIGs Fullcalendar & Datepicker
       *
       */

      /* config datepicker object */
      $scope.datepickerOptions = {
        dateFormat: "yy-mm-dd",
        autoSize: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        onSelect: function(selectedDate, uiCalendar){},
        beforeShowDay: function(curDate){
          return [true, ''];
        }
      };

      /* config calendar object */
      $scope.uiConfig = {
        calendar:{
          lang: 'ru',
          height: $scope.getCalendarHeight(),
          editable: false,
          header:{
            left: 'prev,next',
            center: 'title',
            right: 'agendaDay agendaWeek month'
          },
          defaultView: 'agendaWeek',
          dateFormat:"yy-dd-mm",
          axisFormat: 'HH:mm',
          timeFormat: 'HH:mm',
          monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
          monthNamesShort: ['Янв.','Фев.','Март','Апр.','Май','Июнь','Июль','Авг.','Сент.','Окт.','Ноя.','Дек.'],
          dayNames: ["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],
          dayNamesShort: ["ВС","ПН","ВТ","СР","ЧТ","ПТ","СБ"],
          buttonText: {
            prev: "<",
            next: ">",
            prevYear: "prev year",
            nextYear: "next year",
            today: "Сегодня",
            month: "Месяц",
            week: "Неделя",
            day: "День",
            list:"Повестка дня"
          },
          allDayText: 'День',
          eventRender: function(event, element) {
            element.on('click','button', function(){
              $(this).parent().toggleClass('done');
              var taskId = $(this).attr('data-taskId');
              var taskListId = $(this).attr('data-taskListId');
              var incr = $(this).attr('data-incr');
              var period = $(this).attr('data-period');
              $scope.toggleDoneState(taskId, taskListId, incr, period);
              //event.stopPropagation();
              return false;
            });
          },
          viewRender: function (view, element) {
            //console.log('viewRender');

            $scope.datepickerOptions.autoSize = !$scope.datepickerOptions.autoSize;//init datepicker's directive update.
            $scope.datepickerOptions.beforeShowDay = function(curDate){
              if( (moment(curDate).diff(view.start, 'days') >= 0) && (moment(curDate).diff(view.end, 'days') < 0) ){
                return [true, 'highlight'];
              }
              return [true, ''];
            };
            $scope.datepickerOptions.onSelect = function(selectedDate, uiCalendar){
              var goToDate = moment(new Date(selectedDate));
              $scope.sit2bCalendar.fullCalendar( 'gotoDate', goToDate );
            };

            //$timeout(function(){
              $scope.setCalendarHeight();
              $scope.calendarGridHeight();
              $scope.setTimeline();
            //});
          },
          windowResize: function(view, element) {
            $scope.setTimeline();
            //$scope.perfectScrollbar();
          }
        }
      };


      /**
       * Div-Scroll
       * info: https://github.com/noraesae/perfect-scrollbar
       */
      $scope.perfectScrollbar = function(){
        var scrollEl = $('.calendar_lists_wrapper .list-tasks-body');
        var listHeight = $(scrollEl).height();
        var listBodyHeight = $(scrollEl).find('ul').outerHeight();

        if(!$(scrollEl).hasClass('ps-container') && listBodyHeight > listHeight){
          $(scrollEl).perfectScrollbar({
            wheelSpeed: 20,
            suppressScrollX: true,
            minScrollbarLength: 20
          });
        }else if($(scrollEl).hasClass('ps-container')){
          $(scrollEl).perfectScrollbar('update');
        }
      };
      $timeout(function(){
        $scope.perfectScrollbar();
      });

    }]
);