angular.module('FRZ.dashboard')
  .controller('TrashCtrl', ['$scope', '$injector', 'tasks', '$localStorage', 'LocalData', 'UserProfile', 'appConfig', '$http', 'toastr', 'TaskModel', '$timeout',
      function ($scope, $injector, tasks, $localStorage, LocalData, UserProfile, appConfig, $http, toastr, TaskModel, $timeout)
  {
    $scope.tasks = tasks;
    $scope.preferences = UserProfile.getUserPreferences();

    /**
     *
     */
    $scope.getListColor = function (list_id) {
      if($scope.preferences.lists[list_id]){
        return $scope.preferences.lists[list_id].color;
      }else{
        return 'fdbebe';
      }
    };

    /**
     *
     * @param val
     * @returns {number}
     */
    $scope.toNumber = function (val) {
      return Number(val);
    };
    /**
     *
     * @param task
     * @returns {boolean|*}
     */
    $scope.inThePast = function (task) {
      return !task.done && moment(task.end_date).isBefore();
    };

    /**
     * Show date-time summary on the task card.
     * @param task
     * @returns {string}
     */
    $scope.showDateSummary = function(task) {
      var retval = '';

      retval += moment(task.start_date).format('H:mm');
      if (task.end_date) {
        retval += '-' + moment(task.end_date).format('H:mm');
      }
      retval += ', ';

      if (moment(task.start_date).isSame(moment(), 'day')) {
        retval += 'сегодня';
      } else {
        retval += moment(task.start_date).format('DD.MM.YYYY');
      }
      return retval;
    }

    /**
     * Restore task
     * @param task
     * @param index
     */
    $scope.restoreTask = function (task, index) {
      TaskModel.restore({id: task.id}, task,
        function (result) {
          console.log('restoreTask');
          console.log(task);
          console.log(result);
          if(parseInt(result.task.tlist_id) !== parseInt(task.tlist_id)){
            task.tlist_id = result.task.tlist_id;
            task.executor_id = result.task.executor_id;
            task.executor = {};
            toastr.info('"' + task.title + '"', 'Задача восстановлена в список "Входящие", так как списка "'+result.list.title+'" не существует.');
          }else{
            toastr.info('"' + task.title + '"', 'Задача восстановлена');
          }
          $scope.tasks.splice(index, 1);
          LocalData.addTaskToLocalList(task);

          $timeout(function(){
            $scope.perfectScrollbarInit();
          });

        }, function (task) {
          console.log('Задача НЕ восстановлена ', task.data);
          toastr.error('Ошибка восстановления задачи');
          alert(result.data.status + ': ' + result.data.message);
        });
    };

    /**
     * Div-Scroll
     * info: https://github.com/noraesae/perfect-scrollbar
     */
    $scope.perfectScrollbarInit = function() {
      var scrollConteiner = $("#trash .popup_content");
      var contentHeight = $(scrollConteiner).height();
      var maxHeight = $(scrollConteiner).parent().parent().height() - 30;

      $(scrollConteiner).css('max-height', maxHeight);

      if(contentHeight > maxHeight && !$(scrollConteiner).hasClass('ps-container')){
        $(scrollConteiner).perfectScrollbar({
          wheelSpeed: 20,
          suppressScrollX: true,
          minScrollbarLength: 20
        });
      }
      if($(scrollConteiner).hasClass('ps-container')){
        $(scrollConteiner).perfectScrollbar('update');
      }
    };

    $scope.$on('onRepeatLast', function(scope, element, attrs){
      $scope.perfectScrollbarInit();
        $(window).resize($scope.perfectScrollbarInit);
    });
    /**
     * Close modal.
     */
    $scope.close = function () {
      $scope.$close(true);
    };

  }]);
