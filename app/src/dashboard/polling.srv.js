/**
 * @file   File description
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 22.04.15
 */

angular.module('FRZ')
  .service('Polling',
  ['$injector', '$q',
    function ($injector, $q) {
      var self = this;
      var $localStorage = $injector.get('$localStorage'),
        $timeout = $injector.get('$timeout'),
        LocalData = $injector.get('LocalData'),
        TaskModel = $injector.get('TaskModel'),
        ListModel = $injector.get('ListModel'),
        LogModel = $injector.get('LogModel'),
        toastr = $injector.get('toastr');

      var INTERVAL_TO_RETRIEVE_POLLING = 600000; // is ms, default must be 600000 (10 min)
      var MAX_BAD_REQUESTS_COUNT = 5;

      this.lastLogItemId = 0;
      this.badRequestsCount = 0;
      this.badRequestTime = null;

      /**
       * Add log to localstorage.
       * @param logItem
       * @returns {boolean}
       */
      var processLogItem = function (logItem) {
        LocalData.prepareLogItem(logItem).addLogItem(logItem);
        if (!logItem.viewedLog) LocalData.addLogItemAsUnread(logItem);
        return logItem.viewedLog ? false : true;
      };

      /**
       * Get task from server and insert into local storage.
       * @param task_id
       * @param msg
       */
      var retrieveTask = function (logItem) {
        TaskModel.get({id: logItem.task_id}).$promise.then(function (collection) {
          if (collection.length) {
            toastr.info(logItem.text);
            LocalData.prepareTask(collection[0]);
            LocalData.addTaskToLocalList(collection[0]);
            LocalData.updateTaskNotifications(logItem)
          }
        }, function (response) {
          console.log(response);
        });
      };

      /**
       * Remove task from local storage if exists.
       * @param task_id
       * @param msg
       */
      var removeTask = function (task_id, msg) {
        var task = LocalData.findTaskById(task_id);
        if (task) {
          toastr.info(msg);
          LocalData.removeTaskFromLocalList(task);
        }
      };

      /**
       * Move task across available lists.
       * @param task
       * @param msg
       */
      var moveAcrossLists = function (task, msg) {
        toastr.info(msg);
        LocalData.removeTaskFromLocalList(task);
        if (LocalData.doesListExists(task.tlist_id)) {
          TaskModel.get({id: task.id}).$promise.then(function (collection) {
            if (collection.length) {
              LocalData.prepareTask(collection[0]);
              LocalData.addTaskToLocalList(collection[0]);
            }
          }, function (response) {
            console.log(response);
          });
        }
      };

      /**
       * Get, store and show shared list.
       * @param list_id
       */
      var addSharedList = function (list_id, msg) {
        toastr.info(msg);
        ListModel.get({id: list_id}).$promise.then(function (list) {
          LocalData.prepareList(list);
          angular.forEach(list.tasks, function (task) {
            LocalData.prepareTask(task);
          });
          $timeout(function () {
            $localStorage.sit2b.listsAndTasks[list.id] = list;
          }, 1000);
        }, function (response) {
          console.log(response);
        });
      };

      /**
       * Remove shared list.
       * @param list_id
       * @param msg
       */
      var removeSharedList = function (list_id, msg) {
        toastr.info(msg);
        LocalData.removeLocalList(list_id);
      };


      /**
       * Get fresh log from server
       */
      this.makeRequest = function () {
        var timeDiff = self.badRequestTime ? moment().diff(self.badRequestTime) : 0;
        console.log('badRequestCount: ', self.badRequestsCount);
        console.log('badRequestTime: ', self.badRequestTime);
        console.log('timeDiff: ', timeDiff);

        if (self.badRequestsCount >= MAX_BAD_REQUESTS_COUNT && timeDiff <= INTERVAL_TO_RETRIEVE_POLLING) {
          toastr.error('Проверьте сетевое подключение', 'Ошибка!');
          return;
        }

        if (self.lastLogItemId == 0 && $localStorage.sit2b.logs.new.length) {
          self.lastLogItemId = $localStorage.sit2b.logs.new[$localStorage.sit2b.logs.new.length - 1].id;
        }

        LogModel.get({last: self.lastLogItemId}).$promise.then(function (data) {
          self.badRequestsCount = 0;
          self.badRequestTime = null;

          if (data.length) {
            self.lastLogItemId = data[data.length - 1].id;
            angular.forEach(data, function (logItem) {
              if (!processLogItem(logItem)) return;

              switch (logItem.type) {
                case 'create':
                case 'edit':
                case 'done':
                case 'repeat':
                case 'repeat_out':
                case 'executor_id':
                case 'files':
                case 'priority':
                case 'date':
                case 'location':
                case 'description':
                case 'duration':
                case 'comments':
                case 'reminders':
                  if (logItem.task_id) {
                    retrieveTask(logItem);
                  }
                  break;
                case 'delete':
                  if (logItem.task_id) {
                    removeTask(logItem.task_id, logItem.text);
                  }
                  break;
                case 'share':
                  if (logItem.list_id) {
                    addSharedList(logItem.list_id, logItem.text);
                  }
                  break;
                case 'unshare':
                  if (logItem.list_id) {
                    removeSharedList(logItem.list_id, logItem.text);
                  }
                  break;
              /**
               * If task has been moved into another list -
               * check if this list is available for current user
               * and insert into it this task.
               */
                case 'list_id':
                  console.log(logItem.task);
                  moveAcrossLists(logItem.task, logItem.text);
                  break;
              }
            });
          }
        }, function (data) {
          self.badRequestsCount++;
          self.badRequestTime = moment();
          console.error('NO CARRIER!', data);
        });
      }
    }
  ]);


