/**
 *  Module FRZ.dashd
 *
 * Description
 */
angular.module('FRZ.dashboard')
  .controller('TopNavCtrl', ['$scope', '$localStorage', function ($scope, $localStorage) {

    $scope.$watch(function(){
      return $localStorage.sit2b.comments.new.length
    }, function(n, o){
      $scope.newComments = n;
    });

    $scope.$watch(function(){
      return $localStorage.sit2b.logs.new.length
    }, function(n, o){
      $scope.newLogs = n;
    });

    $scope.showLists =  $scope.$parent.showLists;
    $scope.showCalendar = $scope.$parent.showCalendar;

    $scope.$watch('showCalendar', function(){
      $scope.$parent.showCalendar = $scope.showCalendar;
    });
    $scope.$watch('showLists', function(){
      $scope.$parent.showLists = $scope.showLists;
    });

  }]);