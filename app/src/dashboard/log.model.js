angular.module('FRZ.dashboard')

  .factory('LogModel', ['$resource', 'appConfig', function ($resource, appConfig) {
    return $resource(appConfig.apiUrl + '/log/:id',
      {id: '@id'}, {
        'update': {method: 'PUT'},
        'get':{isArray: true}
      }
    );
  }]);
