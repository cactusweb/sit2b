/**
 * @file   Maon application file.
 *
 * @author Victor Burre, victor.burre@gmail.com
 *
 * 20.11.14
 */

"use strict";
var __DEBUG__ = true;

angular.module('FRZ', [
  'FRZ.auth',
  'FRZ.dashboard',
  'ui.router',
  'ui.utils',
  'ngStorage',
  'ngAnimate',
  'toastr'
])
  .service('appConfig', ['$location', function ($location) {
    switch ($location.host()) {
      case 'sit2b.com':
        this.apiHost = 'http://api.sit2b.com';
        this.apiUrl = 'http://api.sit2b.com/v1';
        this.frontUrl = 'http://sit2b.com';
        this.googleOAuthClientId = '744059574569-6q7vobmamrc70qku16e8g79c32gmtppg.apps.googleusercontent.com';
        break;
      case 'sit2b.rollinggames.net':
        this.apiHost = 'http://sit2b-api.rollinggames.net';
        this.apiUrl = 'http://sit2b-api.rollinggames.net/v1';
        this.frontUrl = 'http://sit2b.rollinggames.net';
        this.googleOAuthClientId = '744059574569-6q7vobmamrc70qku16e8g79c32gmtppg.apps.googleusercontent.com';
        break;
    }
  }])

  .run([
    '$rootScope', '$state', '$stateParams', '$sessionStorage', 'AUTH_EVENTS', 'AuthService', 'SessionService',
    function ($rootScope, $state, $stateParams, $sessionStorage, AUTH_EVENTS, AuthService, SessionService) {
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;

      $rootScope.$on('$stateNotFound',
        function (event, unfoundState, fromState, fromParams) {
          console.error('STATE NOT FOUND');
          console.info(unfoundState.to); // "lazy.state"
          console.log(unfoundState.toParams); // {a:1, b:2}
          console.log(unfoundState.options); // {inherit:false} + default options
        });

      $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {
          //console.log(toState);
          var authorizedRoles = toState.data.authorizedRoles;

          if (AuthService.allUsersAllowed(authorizedRoles)) {
            $rootScope.$broadcast(AUTH_EVENTS.loginFree);
          } else {
            if (!AuthService.isAuthorized(authorizedRoles)) {
              event.preventDefault();

              // Check session storage for user data (if user pressed F5)
              if ($sessionStorage.user !== undefined && $sessionStorage.user.access_token !== undefined) {
                console.log('Жали F5');
                SessionService.create($sessionStorage.user.access_token, $sessionStorage.user.id, $sessionStorage.user.group);
                SessionService.update($sessionStorage.user.access_token);
                $rootScope.$broadcast(AUTH_EVENTS.sessionRecovered, $sessionStorage.user);
                $rootScope.$state.go(toState, toParams);
              }

              if (AuthService.isAuthenticated()) {
                // user is not allowed
                $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
              } else {
                // user is not logged in
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
                $sessionStorage.originRedirect = {
                  toState: toState.name,
                  toParams: toParams
                }
              }
            }
          }
        }
      )
      ;

      //moment.tz('Europe/Kiev');
      moment.locale('ru');
    }
  ])

  .config([
    '$stateProvider', '$urlRouterProvider', 'USER_ROLES',
    function ($stateProvider, $urlRouterProvider, USER_ROLES) {

      $urlRouterProvider
        .when('', '/')
        //.otherwise('/404');
        .otherwise(function ($injector, $location) {
          var $scope = $injector.get('$rootScope');
          var toastr = $injector.get('toastr');
          console.log($location.url());
          console.log($scope.$state.get());
          var states = $scope.$state.get();

          for (var i = 0; i < states.length; i++) {
            if ($location.url() == states[i].url) {
              $scope.$state.go(states[i]);
              return;
            } else {
              toastr.error('Page not found', '404');
              //$scope.$state.go('not-found');
              return;
            }
          }
        });

      $stateProvider
        .state('home', {
          url: '/',
          template: '<ui-view><button ui-sref="auth.login">Войти в систему</button></ui-view>',
          data: {
            authorizedRoles: [USER_ROLES.all]
          },
          controller: ['$scope', function ($scope) {
            //$scope.$state.go('dashboard.start');
          }]
        })

        .state('not-found', {
          url: '/404',
          template: '<ui-view><h1>404 Страница не найдена</h1><br><button ui-sref="{{backurl}}">Вернуться</button></ui-view>',
          data: {
            authorizedRoles: [USER_ROLES.all]
          },
          params: {
            backTo: 'dashboard.start'
          },
          controller: ['$scope', '$state', '$stateParams', function ($scope, $state, $stateParams) {
            $scope.backurl = $stateParams.backTo;
          }]
        });
    }
  ])

  .config(function (toastrConfig) {
    angular.extend(toastrConfig, {
      allowHtml: false,
      closeButton: false,
      closeHtml: '<button>&times;</button>',
      containerId: 'toast-container',
      extendedTimeOut: 1000,
      iconClasses: {
        error: 'toast-error',
        info: 'toast-info',
        success: 'toast-success',
        warning: 'toast-warning'
      },
      maxOpened: 0,
      messageClass: 'toast-message',
      newestOnTop: true,
      onHidden: null,
      onShown: null,
      positionClass: 'toast-bottom-right',
      preventDuplicates: false,
      tapToDismiss: true,
      target: 'body',
      timeOut: 5000,
      titleClass: 'toast-title',
      toastClass: 'toast'
    });
  })

/**
 * Interceptor for server side authentication.
 *
 * @see http://www.webdeveasy.com/interceptors-in-angularjs-and-useful-examples/
 */
  .
  factory('AuthorizationInterceptor', ['$q', '$injector', function ($q, $injector) {
    var sessionInterceptor = {
      request: function (config) {
        var SessionService = $injector.get('SessionService');

        if (SessionService.id) {
          config.headers.Authorization = SessionService.id;
        }
        return config;
      }
    };
    return sessionInterceptor;
  }])

/**
 * Interceptor for session recovering.
 *
 * @see http://www.webdeveasy.com/interceptors-in-angularjs-and-useful-examples/
 */
  .factory('sessionRecoverer', ['$q', '$injector', function ($q, $injector) {
    var sessionRecoverer = {
      responseError: function (response) {
        console.log(response);
        // Session has expired
        if (response.status == 418) {
          console.log('INTERCEPTOR: 418 Session expired');
          var $http = $injector.get('$http');
          var AuthService = $injector.get('AuthService');
          var deferred = $q.defer();

          AuthService.sessionRecovery().then(deferred.resolve, deferred.reject);

          // When the session recovered, make the same backend call again and chain the request
          return deferred.promise.then(function () {
            return $http(response.config);
          });
        } else if (response.status == 401) {
          var SessionService = $injector.get('SessionService');
          var $sessionStorage = $injector.get('$sessionStorage');
          var $http = $injector.get('$http');
          var AuthService = $injector.get('AuthService');
          var deferred = $q.defer();

          if ($sessionStorage.user !== undefined && $sessionStorage.user.access_token !== undefined) {
            console.log('INTERCEPTOR: 401 Session from sessionStorage');
            // @TODO разобраться не дублируют ли эти строки функциональность
            SessionService.update($sessionStorage.user.access_token);
            AuthService.sessionRecovery().then(deferred.resolve, deferred.reject);
            // @TODO не ясно тут это делать или нет
            $rootScope.$broadcast(AUTH_EVENTS.sessionRecovered, $sessionStorage.user);
            ////$rootScope.$state.go(toState, toParams);
          }
          return deferred.promise.then(function () {
            return $http(response.config);
          });
        }
        return $q.reject(response);
      }
    };
    return sessionRecoverer;
  }])

  .factory('AuthInterceptor', ['$rootScope', '$q', 'AUTH_EVENTS', function ($rootScope, $q, AUTH_EVENTS) {
    return {
      responseError: function (response) {
        $rootScope.$broadcast({
          401: AUTH_EVENTS.notAuthenticated,
          403: AUTH_EVENTS.notAuthorized,
          418: AUTH_EVENTS.sessionTimeout,
          440: AUTH_EVENTS.sessionTimeout
        }[response.status], response);
        return $q.reject(response);
      }
    };
  }])

/**
 * Main system cache.
 */
  .factory('superCache', ['$cacheFactory', function ($cacheFactory) {
    return $cacheFactory('super-cache');
  }])

  .config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
    $httpProvider.interceptors.push('AuthorizationInterceptor');
    $httpProvider.interceptors.push('sessionRecoverer');
  }])


/**
 * Decorator for Template Factory.
 */
  .config(['$provide', function ($provide) {
    // Set a suffix outside the decorator function
    var cacheBuster = Date.now().toString();

    function templateFactoryDecorator($delegate) {
      var fromUrl = angular.bind($delegate, $delegate.fromUrl);
      $delegate.fromUrl = function (url, params) {
        if (url !== null && angular.isDefined(url) && angular.isString(url)) {
          url += (url.indexOf("?") === -1 ? "?" : "&");
          url += "v=" + cacheBuster;
        }

        return fromUrl(url, params);
      };

      return $delegate;
    }

    $provide.decorator('$templateFactory', ['$delegate', templateFactoryDecorator]);
  }]);
